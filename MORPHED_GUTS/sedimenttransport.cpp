#include "sedimenttransport.h"

namespace MORPH{

SedimentTransport::SedimentTransport(QString xmlPath):Surfaces(xmlPath)
{
}

SedimentTransport::SedimentTransport(XMLReadWrite &XmlObj):Surfaces(XmlObj)
{
    pCumRetreat = NULL;
    pBankID = NULL;
    pShearDS = NULL;
    pDepthDS = NULL;
    pFlowDirDS = NULL;
    pDepoTemp = NULL;
    pCohesion = NULL;

    bedErode = 0.0, bedErodeTotal = 0.0, bedDepo = 0.0, bedDepoTotal = 0.0, bankErode = 0.0, bankErodeTotal = 0.0, bankDepo = 0.0, bankDepoTotal = 0.0, exported = 0.0, exportedTotal = 0.0, imported = 0.0, importedTotal = 0.0, importDepo = 0.0;

    loadDrivers();
    HydroData = new Hydraulics(XmlObj);
    qDebug()<<"Hydraulics class loaded";
    GS = new GrainSize(XmlObj);
    qDebug()<<"Grainsize class loaded";
    Veg = new Vegetation(XmlObj);
    qDebug()<<"Vegetation class loaded";
    PlBed = new PathLengthDistribution(XmlObj);
    PlImport = new PathLengthDistribution(XmlObj);
    qsTempDepo = qsTempDir + "/depo_temp.tif";
    qsBankID = qsTempDir + "/bankID.tif";
    qsCumRetreat = qsTempDir + "/cumRetreat.tif";
    qsSlopeBankMax = qsTempDir + "/slopeBankMax.tif";
    qsCohesion = qsTempDir + "/cohesion.tif";
    qsBedE = qsTempDir + "/bede.tif";
    qsBedD = qsTempDir + "/bedd.tif";
    qsBank = qsTempDir + "/bank.tif";
    inGS = 0.0;

    char **papszOptions = NULL;

    pDepoTemp = pDriverTIFF->Create(qsTempDepo.toStdString().c_str(),nCols, nRows, 1, GDT_Float32, papszOptions);
    pDepoTemp->SetGeoTransform(transform);
    pDepoTemp->GetRasterBand(1)->Fill(0.0);

    pBankID = pDriverTIFF->Create(qsBankID.toStdString().c_str(),nCols, nRows, 1, GDT_Float32, papszOptions);
    pBankID->SetGeoTransform(transform);
    pBankID->GetRasterBand(1)->Fill(0.0);

    pCumRetreat = pDriverTIFF->Create(qsCumRetreat.toStdString().c_str(),nCols, nRows, 1, GDT_Float32, papszOptions);
    pCumRetreat->SetGeoTransform(transform);
    pCumRetreat->GetRasterBand(1)->Fill(0.0);

    depth = (float*) CPLMalloc(sizeof(float)*1);
    fdirValue = (float*) CPLMalloc(sizeof(float)*1);
    setData();
    Veg->createInitialCohesion(pDetrend);
    qDebug()<<"Sed transport initialized";
}

SedimentTransport::~SedimentTransport()
{
    qDebug()<<"Sediment destructor";
    if (pCumRetreat != NULL)
    {
        GDALClose(pCumRetreat);
    }
    if (pBankID != NULL)
    {
        GDALClose(pBankID);
    }
    if (pShearDS != NULL)
    {
        GDALClose(pShearDS);
    }
    if (pDepthDS != NULL)
    {
        GDALClose(pDepthDS);
    }
    if (pFlowDirDS != NULL)
    {
        GDALClose(pFlowDirDS);
    }
    if (pDepoTemp != NULL)
    {
        GDALClose(pDepoTemp);
    }
    if (pCohesion != NULL)
    {
        qDebug()<<"close cohesion";
        GDALClose(pCohesion);
    }
    CPLFree(depth);
    CPLFree(fdirValue);
    depth = NULL;
    fdirValue = NULL;
    delete HydroData;
    delete GS;
    delete PlBed;
    delete PlImport;
    delete Veg;
}

void SedimentTransport::bankSloughing()
{
    qDebug()<<"setting slough vars";
    bool stop = false;
    bool slough = false;
    int count = 0, iterCount = 0, oneCount = 0;
    double amount, slopeThresh;
    QVector<int> lowCell;
    GDALRasterBand *pCohesionBand;
    pCohesionBand = Veg->getCohesionDS()->GetRasterBand(1);

    float *cVal = (float*) CPLMalloc(sizeof(float)*1);
    float *sVal = (float*) CPLMalloc(sizeof(float)*1);
    float *dVal = (float*) CPLMalloc(sizeof(float)*1);
    float *eVal = (float*) CPLMalloc(sizeof(float)*1);
    float *gsThick = (float*) CPLMalloc(sizeof(float)*1);
    float *gsSize = (float*) CPLMalloc(sizeof(float)*1);
    float *gsBand = (float*) CPLMalloc(sizeof(float)*1);
    qDebug()<<"bank slough calc slope";
    calculateSlopeThirdOrderFinite(pNewSurface);

    while (!stop && iterCount < 400)
    {
        qDebug()<<"Starting bank slough loop "<<iterCount;
        for (int i=0; i<nRows; i++)
        {
            for (int j=0; j<nCols; j++)
            {
                pCohesionBand->RasterIO(GF_Read,j,i,1,1,cVal,1,1,GDT_Float32,0,0);
                pSlope->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,sVal,1,1,GDT_Float32,0,0);

                if (*cVal != fNoDataValue && *sVal != fNoDataValue)
                {
                    //if slope exceeds threshold for a given cohesion value set "slough" to "true";
                    //qDebug()<<"vals "<<*cVal<<" "<<*sVal;
                    if (*cVal > 2.5)
                    {
                        if (*sVal > MAXFAIL_SLOPE3)
                        {
                            slough = true;
                            slopeThresh = MAXFAIL_SLOPE3;
                        }
                    }
                    else if (*cVal > 1.5)
                    {
                        if (*sVal > MAXFAIL_SLOPE2)
                        {
                            slough = true;
                            slopeThresh = MAXFAIL_SLOPE2;
                        }
                    }
                    else if (*cVal > 0.0)
                    {
                        if (*sVal > MAXFAIL_SLOPE1)
                        {
                            slough = true;
                            slopeThresh = MAXFAIL_SLOPE1;
                        }
                    }
                    else
                    {
                        qDebug()<<"Invalid cohesion value "<<*cVal;
                        slough = false;
                    }

                    //if slope threshold exceeded adjust cell elevations and recalculate slope
                    if (slough)
                    {
                        //qDebug()<<"slouging happening "<<*sVal<<" "<<slopeThresh;
                        oneCount = 0;
                        count++;
                        while (*sVal > slopeThresh && oneCount < 50)
                        {
                            lowCell = findLowestNeighbor(i, j, amount);

                            //only make adjustments if material is actually moved
                            if (amount > MAXFAIL_MIN && lowCell[0] > 0 && lowCell[1] > 0)
                            {
                                if (amount < 100.0)
                                {
                                    //qDebug()<<"Changing values with slough "<<amount<<" "<<i<<" "<<j;
                                    //count++;
                                    pDepth->RasterIO(GF_Read,lowCell[1],lowCell[0],1,1,dVal,1,1,GDT_Float32,0,0);
                                    GS->pActive->GetRasterBand(2)->RasterIO(GF_Read,j,i,1,1,gsThick,1,1,GDT_Float32,0,0);
                                    bankErode += amount;
                                    bankErodeTotal += amount;

                                    //check depth of slough to for grain size averaging
                                    if (amount > *gsThick)
                                    {
                                        if (amount < 100.0)
                                        {
                                            *gsSize = GS->erodeMultipleLayers(i, j, amount);
                                        }
                                        else
                                        {
                                            qDebug()<<"AMOUNT ERODE ERROR: "<<amount<<" row "<<i<<" col "<<j<<" slope "<<*sVal;
                                        }
                                        //qDebug()<<"slough eroding multiple layers: erode depth "<<amount<<" layer thickness "<<*gsThick;
                                    }
                                    else
                                    {
                                        *gsThick -= amount;
                                        GS->pActive->GetRasterBand(2)->RasterIO(GF_Write,j,i,1,1,gsThick,1,1,GDT_Float32,0,0);
                                        GS->pActive->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,gsBand,1,1,GDT_Float32,0,0);
                                        GS->pMain->GetRasterBand(*gsBand)->RasterIO(GF_Read,j,i,1,1,gsSize,1,1,GDT_Float32,0,0);
                                        //qDebug()<<"slough NOT eroding multiple layers: erode depth "<<amount<<" layer thickness "<<*gsThick;
                                    }

                                    //if deposit cell is wet distribute according to path-length distribution
                                    if (*dVal > 0.0)
                                    {
                                        //idCandidateCells(lowCell[0], lowCell[1], amount, *gsSize);
                                        //depositToCandidates();
                                        runBedTransportSingleThread_5Paths(lowCell[0], lowCell[1], amount, *gsSize);
                                    }
                                    else
                                    {
                                        GS->addDeposition(lowCell[0], lowCell[1], *gsSize, amount);
                                        bankDepo += amount;
                                    }
                                    //recalculate all slope values in 3x3
                                    int row = i, col = j;
                                    for (int k=row-1; k<row+2; k++)
                                    {
                                        for (int l=col-1; l<col+2; l++)
                                        {
                                            recalcSlope3x3TOF(pNewSurface,k,l);
                                        }
                                    }
                                }
                                else
                                {
                                    qDebug()<<"AMOUNT DEPO ERROR: "<<amount<<" row "<<i<<" col "<<j<<" slope "<<*sVal;
                                }
                            }
                            else
                            {
                                oneCount = 50;
                                slough = false;
                                //qDebug()<<"something wrong "<<amount<<" "<<lowCell[0]<<" "<<lowCell[1];
                            }
                            pSlope->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,sVal,1,1,GDT_Float32,0,0);
                            //qDebug()<<"new slope "<<*sVal<<" slope thresh "<<slopeThresh;
                            oneCount++;
                        }
                        slough = false;
                    }
                }
            }
        }
        if (count == 0)
        {
            stop = true;
        }
        else
        {
            qDebug()<<"Bank slough iteration done "<<count<<" cells changed";
            stop = false;
            count = 0;
            iterCount++;
        }
        depositToDEM(bankDepo);
    }
    qDebug()<<"copying ds bound";
    copyDSBoundary();
    qDebug()<<"ds bound copy done";

    pBank = pDriverTIFF->CreateCopy(qsBank.toStdString().c_str(),pNewSurface,FALSE,NULL,NULL,NULL);
    QString qsDodPath = qsOutputs + "/GTIFF/BankChange" + QString::number(nCurrentIteration+1) + ".tif";
    writeTempDoDs(qsDodPath,pBedD,pNewSurface);
    GDALClose(pBedD);
    pBedD = NULL;

    bankDepoTotal += bankDepo;
    qDebug()<<"Main bank slough loop finished";
    qDebug()<<"exported "<<exported<<" imported "<<imported;
    importSediment();
    depositToDEM(importDepo);
    qsDodPath = qsOutputs + "/GTIFF/ImportDeposition" + QString::number(nCurrentIteration+1) + ".tif";
    writeTempDoDs(qsDodPath,pBank,pNewSurface);
    GDALClose(pBank);
    pBank=NULL;
    qDebug()<<"temp dod written";

    importedTotal += imported;
    exportedTotal += exported;
    qDebug()<<"updating gs layers";
    GS->updateLayers(pOldSurface);
    qDebug()<<"import finished";
    //depositToDEM();
    //qDebug()<<"Deposit to dem finished";

    CPLFree(cVal);
    CPLFree(sVal);
    CPLFree(dVal);
    CPLFree(eVal);
    CPLFree(gsThick);
    CPLFree(gsSize);
    CPLFree(gsBand);
    cVal = NULL;
    sVal = NULL;
    dVal = NULL;
    eVal = NULL;
    gsThick = NULL;
    gsSize = NULL;
    gsBand = NULL;
    qDebug()<<"Slough memory freed";

    //HydroData->closeDatasets();

    braidData = HydroData->calculateBraidIndex(pDepth);
    qDebug()<<"braid index = "<<braidData[0]<<" confluences = "<<braidData[1]<<" diffluences = "<<braidData[2];
}

void SedimentTransport::calculateCumulativeRetreat()
{
    qDebug()<<"starting cum retreat";
    float *retreatVal = (float*) CPLMalloc(sizeof(float)*1);
    float *bankVal = (float*) CPLMalloc(sizeof(float)*1);
    float *slopeVal = (float*) CPLMalloc(sizeof(float)*1);

    double shear;
    qDebug()<<"cumretreat vars set";

    for (int i=0; i<nRows; i++)
    {
        for (int j=0; j<nCols; j++)
        {
            pBankID->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,bankVal,1,1,GDT_Float32,0,0);
            if (*bankVal > 0.0)
            {
                shear = fabs(findMaxShear3x3(i,j));
                pSlope->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,slopeVal,1,1,GDT_Float32,0,0);
                pCumRetreat->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,retreatVal,1,1,GDT_Float32,0,0);
                if (*slopeVal > 0.0)
                {
                    *retreatVal = *retreatVal + (shear * 0.1);
                }
                else
                {
                    *retreatVal = *retreatVal + (shear * 0.001);
                }
                pCumRetreat->GetRasterBand(1)->RasterIO(GF_Write,j,i,1,1,retreatVal,1,1,GDT_Float32,0,0);
            }
        }
    }
    qDebug()<<"cumretreat loop done";

    CPLFree(retreatVal);
    retreatVal = NULL;
    CPLFree(bankVal);
    bankVal = NULL;
    CPLFree(slopeVal);
    slopeVal = NULL;
}

void SedimentTransport::clearVectors()
{
    //QMutexLocker locker(&mutex);
    rowCand.clear();
    colCand.clear();
    gsVec.clear();
    volDep.clear();
    volErode.clear();
    propCell.clear();
    propPl.clear();
}

void SedimentTransport::closeSedDatasets()
{
    qDebug()<<"closing sed datasets";

    if (pCumRetreat != NULL)
    {
        qDebug()<<"retreat";
        GDALClose(pCumRetreat);
        pCumRetreat = NULL;
        qDebug()<<"retreat closed";
    }
    if (pBankID != NULL)
    {
        qDebug()<<"bankid";
        GDALClose(pBankID);
        pBankID = NULL;
        qDebug()<<"bankid closed";
    }
    if (pShearDS != NULL)
    {
        qDebug()<<"shear";
        GDALClose(pShearDS);
        pShearDS = NULL;
        qDebug()<<"shear closed";
    }
    if (pDepthDS != NULL)
    {
        qDebug()<<"depth";
        GDALClose(pDepthDS);
        pDepthDS = NULL;
        qDebug()<<"depth closed";
    }
    if (pFlowDirDS != NULL)
    {
        qDebug()<<"flow";
        GDALClose(pFlowDirDS);
        pFlowDirDS = NULL;
        qDebug()<<"flow closed";
    }
    if (pDepoTemp != NULL)
    {
        qDebug()<<"depo";
        GDALClose(pDepoTemp);
        pDepoTemp = NULL;
        qDebug()<<"depo closed";
    }
    if (pCohesion != NULL)
    {
        qDebug()<<"cohesion";
        GDALClose(pCohesion);
        pCohesion = NULL;
        qDebug()<<"cohesion closed";
    }
    qDebug()<<"sed done";
    GS->closeGsDatasets();
    qDebug()<<"gs done";
    Veg->closeVegDatasets();
    qDebug()<<"veg done";
    closeDatasets();
    qDebug()<<"dem done";
}

void SedimentTransport::createInitialCohesion()
{

}

void SedimentTransport::depositToCandidates()
{
    //QMutexLocker locker(&mutex);
    float* val = (float*) CPLMalloc(sizeof(float)*1);
    for (int i=0; i<rowCand.size(); i++)
    {
        //qDebug()<<"volume dep "<<volDep[i];
        //adjust grainsize active layer
        GS->addDeposition(rowCand[i], colCand[i], gsVec[i], volDep[i]);

        //add deposition to temp layer
        pDepoTemp->GetRasterBand(1)->RasterIO(GF_Read,colCand[i],rowCand[i],1,1,val,1,1,GDT_Float32,0,0);
        *val += volDep[i];
        pDepoTemp->GetRasterBand(1)->RasterIO(GF_Write,colCand[i],rowCand[i],1,1,val,1,1,GDT_Float32,0,0);

        //make sure deposition is not greater than water depth, if it is change depth to 0;
        GS->pDeposition->GetRasterBand(2)->RasterIO(GF_Read,colCand[i],rowCand[i],1,1,newElev,1,1,GDT_Float32,0,0);
        pDepth->RasterIO(GF_Read,colCand[i],rowCand[i],1,1,depth,1,1,GDT_Float32,0,0);

        if (*newElev > *depth)
        {
            *depth = 0.0;
            pDepth->RasterIO(GF_Write,colCand[i],rowCand[i],1,1,depth,1,1,GDT_Float32,0,0);
        }
    }
    clearVectors();
}

void SedimentTransport::depositToCandidatesImport()
{
    //QMutexLocker locker(&mutex);
    int count;
    double amtPos;
    QVector<int> posCount;
    QVector<double>depAmount;
    for (int i=0; i<PlImport->pathLength.size(); i++)
    {
        for (int j=0; j<plIn.size(); j++)
        {
            if (plIn[j] == i)
            {
                count++;
            }
        }
        posCount.append(count);
        amtPos = imported * PlImport->pathLength[i] / count;
        depAmount.append(amtPos);
        count = 0;
    }
    for (int i=0; i<plIn.size(); i++)
    {
        volDep.append(depAmount[plIn[i]]);
        gsVec.append(inGS);
    }
    depositToCandidates();
    plIn.clear();
    posCount.clear();
    depAmount.clear();
}

void SedimentTransport::depositToDEM(double &updateDepo)
{
    //QMutexLocker locker(&mutex);
    qDebug()<<"Depositing to DEM";
    for (int i=0; i<nRows; i++)
    {
        for (int j=0; j<nCols; j++)
        {
            pDepoTemp->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,newElev,1,1,GDT_Float32,0,0);
            if (*newElev > 0.0)
            {
                pNewSurface->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,oldElev,1,1,GDT_Float32,0,0);
                if (*oldElev > 0.0)
                {
                    if (*newElev > VEG_RESET)
                    {
                        Veg->makeBare(i,j);
                    }
                    updateDepo += *newElev;
                    *newElev += *oldElev;
                    pNewSurface->GetRasterBand(1)->RasterIO(GF_Write,j,i,1,1,newElev,1,1,GDT_Float32,0,0);
                }
                *newElev = 0.0;
                pDepoTemp->GetRasterBand(1)->RasterIO(GF_Write,j,i,1,1,newElev,1,1,GDT_Float32,0,0);
            }
        }
    }
    qDebug()<<"Deposit to DEM finished";
}

double SedimentTransport::erodeBed(double shearCrit, double shear, int row, int col)
{
    double sediment, bedload, USTAR, USTARC, bedvel, grainSize;
    float *actThick = (float*) CPLMalloc(sizeof(float)*1);
    float *grainSizeVal = (float*) CPLMalloc(sizeof(float)*1);
    float *actBand = (float*) CPLMalloc(sizeof(float)*1);

    bedload = pow (shear - shearCrit, 1.5);
    USTAR = sqrt(shear / RHO);
    USTARC = sqrt(shearCrit / RHO);
    bedvel = A * (USTAR - USTARC);
    sediment = (bedload) / (bedvel * RHO_S * (1 - POROSITY));

    GS->pActive->GetRasterBand(1)->RasterIO(GF_Read,col,row,1,1,actBand,1,1,GDT_Float32,0,0);
    GS->pMain->GetRasterBand(*actBand)->RasterIO(GF_Read,col,row,1,1,grainSizeVal,1,1,GDT_Float32,0,0);

    if (sediment > VEG_RESET)
    {
        Veg->makeBare(row, col);
    }

    //update new dem with erosion
    pNewSurface->GetRasterBand(1)->RasterIO(GF_Read,col,row,1,1,oldElev,1,1,GDT_Float32,0,0);
    *newElev = *oldElev - sediment;
    pNewSurface->GetRasterBand(1)->RasterIO(GF_Write,col,row,1,1,newElev,1,1,GDT_Float32,0,0);
    //update thickness of active stratigraphy layer
    GS->pActive->GetRasterBand(1)->RasterIO(GF_Read,col,row,1,1,actThick,1,1,GDT_Float32,0,0);
    //if erosion is deeper than active stratigraphy layer
    if (sediment > *actThick)
    {
        qDebug()<<"ERODING MULTIPLE LAYERS";
        grainSize = GS->erodeMultipleLayers(row, col, sediment);
    }
    else
    {
        *actThick -= sediment;
        GS->pActive->GetRasterBand(1)->RasterIO(GF_Write,col,row,1,1,actThick,1,1,GDT_Float32,0,0);
        grainSize = *grainSizeVal;
    }

    CPLFree(actThick);
    CPLFree(actBand);
    CPLFree(grainSizeVal);
    actThick = NULL;
    actBand = NULL;
    grainSizeVal = NULL;

    return sediment;
}

double SedimentTransport::erodeBed5Cells(double shearCrit, double shear, int row, int col)
{
    //if this works need to adjust it to respect edges and conserve grain size
    double sediment, bedload, USTAR, USTARC, bedvel, xCoord, yCoord, grainSize, tempsed;
    QVector<double> qvGs, qvErode, qvResult(4);
    QVector<int> qvRow, qvCol;
    QVector<double> qvScaling(3);
    qvScaling[0] = 0.3333, qvScaling[1] = 0.2222, qvScaling[2] = 0.1111;

    float *actThick = (float*) CPLMalloc(sizeof(float)*1);
    float *grainSizeVal = (float*) CPLMalloc(sizeof(float)*1);
    float *actBand = (float*) CPLMalloc(sizeof(float)*1);
    float *watDep = (float*) CPLMalloc(sizeof(float)*1);

    bedload = pow (shear - shearCrit, 1.5);
    USTAR = sqrt(shear / RHO);
    USTARC = sqrt(shearCrit / RHO);
    bedvel = A * (USTAR - USTARC);
    sediment = (bedload) / (bedvel * RHO_S * (1 - POROSITY));

    xCoord = transform[0] + (col * cellWidth);
    yCoord = transform[3] - (row * cellWidth);
    qvResult[0] = row;
    qvResult[1] = col;
    qvResult[2] = xCoord;
    qvResult[3] = yCoord;
    qvRow.append(qvResult[0]), qvCol.append(qvResult[1]), qvErode.append(sediment*qvScaling[0]), qvGs.append(*grainSizeVal);

    for (int i=1; i<6; i++)
    {
        qvResult = findNextCellCoords(qvResult[2], qvResult[3], qvResult[0], qvResult[1]);
        GS->pActive->GetRasterBand(1)->RasterIO(GF_Read,qvResult[1],qvResult[0],1,1,actBand,1,1,GDT_Float32,0,0);
        if (qvResult[0] < nRows-2 && qvResult[0] > 1 && qvResult[1] < nCols-2 && qvResult[1] > 1 && *actBand != fNoDataValue)
        {
            GS->pMain->GetRasterBand(*actBand)->RasterIO(GF_Read,qvResult[1],qvResult[0],1,1,grainSizeVal,1,1,GDT_Float32,0,0);
            qvRow.append(qvResult[0]), qvCol.append(qvResult[1]), qvErode.append(sediment*qvScaling[i]), qvGs.append(*grainSizeVal);
        }
    }
    qvResult[0] = row;
    qvResult[1] = col;
    qvResult[2] = xCoord;
    qvResult[3] = yCoord;
    for (int i=1; i<6; i++)
    {
        qvResult = findNextCellCoordsBackward(qvResult[2], qvResult[3], qvResult[0], qvResult[1]);
        GS->pActive->GetRasterBand(1)->RasterIO(GF_Read,qvResult[1],qvResult[0],1,1,actBand,1,1,GDT_Float32,0,0);
        if (qvResult[0] < nRows-2 && qvResult[0] > 1 && qvResult[1] < nCols-2 && qvResult[1] > 1 && *actBand != fNoDataValue)
        {
            GS->pMain->GetRasterBand(*actBand)->RasterIO(GF_Read,qvResult[1],qvResult[0],1,1,grainSizeVal,1,1,GDT_Float32,0,0);
            qvRow.append(qvResult[0]), qvCol.append(qvResult[1]), qvErode.append(sediment*qvScaling[i]), qvGs.append(*grainSizeVal);
        }
    }
    tempsed = sediment/(qvResult.size()*1.0);
    for (int i=0; i<qvRow.size(); i++)
    {
        if (tempsed > VEG_RESET)
        {
            Veg->makeBare(qvRow[i], qvCol[i]);
        }

        //update new dem with erosion
        pNewSurface->GetRasterBand(1)->RasterIO(GF_Read,qvCol[i],qvRow[i],1,1,oldElev,1,1,GDT_Float32,0,0);
        *newElev = *oldElev - tempsed;
        pNewSurface->GetRasterBand(1)->RasterIO(GF_Write,qvCol[i],qvRow[i],1,1,newElev,1,1,GDT_Float32,0,0);
        //update thickness of active stratigraphy layer
        GS->pActive->GetRasterBand(1)->RasterIO(GF_Read,qvCol[i],qvRow[i],1,1,actThick,1,1,GDT_Float32,0,0);
        //if erosion is deeper than active stratigraphy layer
        if (tempsed > *actThick)
        {
            qDebug()<<"ERODING MULTIPLE LAYERS";
            grainSize = GS->erodeMultipleLayers(qvRow[i], qvCol[i], tempsed);
        }
        else
        {
            *actThick -= tempsed;
            GS->pActive->GetRasterBand(1)->RasterIO(GF_Write,qvCol[i],qvRow[i],1,1,actThick,1,1,GDT_Float32,0,0);
            grainSize = *grainSizeVal;
        }
    }

    CPLFree(actThick);
    CPLFree(actBand);
    CPLFree(grainSizeVal);
    CPLFree(watDep);
    actThick = NULL;
    actBand = NULL;
    grainSizeVal = NULL;
    watDep = NULL;

    return sediment;
}

double SedimentTransport::findBankHeight(int row, int col)
{
    double min, height, cell;
    float* data = (float*) CPLMalloc(sizeof(float)*9);

    pOldSurface->GetRasterBand(1)->RasterIO(GF_Read,col-1,row-1,3,3,data,3,3,GDT_Float32,0,0);

    cell = data[4];

    for (int i=0; i<9; i++)
    {
        if (i == 0)
        {
            min = data[i];
        }
        else
        {
            if (data[i] < min)
            {
                min = data[i];
            }
        }
    }
    height = cell-min;
    if (height > 0.0)
    {
        return height;
    }
    else
    {
        return -1;
    }
}

void SedimentTransport::findImportCells()
{
    int wetCount;

    rowIn.clear();
    colIn.clear();

    if (nDirUSbound == 1)
    {
        for (int i=1; i<nCols-1; i++)
        {
            pDepth->RasterIO(GF_Read,i,2,1,1,depth,1,1,GDT_Float32,0,0);
            wetCount = 0;

            for (int j=1; j<4; j++)
            {
                for (int k=i-1; k<i+2; k++)
                {
                    pDepth->RasterIO(GF_Read,k,j,1,1,depth,1,1,GDT_Float32,0,0);

                    if (*depth > 0.000)
                    {
                        wetCount++;
                    }
                }
            }
            if (wetCount > 6)
            {
                rowIn.append(2);
                colIn.append(i);
                qDebug()<<"import cell at "<<"2"<<" "<<i;
            }
        }
    }
    else if (nDirUSbound == 2)
    {
        for (int i=1; i<nCols-1; i++)
        {
            pDepth->RasterIO(GF_Read,i,nRows-2,1,1,depth,1,1,GDT_Float32,0,0);
            wetCount = 0;

            for (int j=nRows-3; j<nRows; j++)
            {
                for (int k=i-1; k<i+2; k++)
                {
                    pDepth->RasterIO(GF_Read,k,j,1,1,depth,1,1,GDT_Float32,0,0);

                    if (*depth > 0.000)
                    {
                        wetCount++;
                    }
                }
            }
            if (wetCount > 6)
            {
                rowIn.append(nRows-2);
                colIn.append(i);
                qDebug()<<"import cell at "<<nRows-2<<" "<<i;
            }
        }
    }
    else if (nDirUSbound == 3)
    {

    }
    else if (nDirUSbound == 4)
    {

    }
    else
    {

    }
}

double SedimentTransport::findMaxShear3x3(int row, int col)
{
    double maxShear = 0.0;
    float *shearVal = (float*) CPLMalloc(sizeof(float)*1);
    float *depthVal = (float*) CPLMalloc(sizeof(float)*1);

    for (int i=row-1; i<row+2; i++)
    {
        for (int j=col-1; j<col+2; j++)
        {
            pDepth->RasterIO(GF_Read,j,i,1,1,depthVal,1,1,GDT_Float32,0,0);
            if (*depthVal > 0.0)
            {
                pShear->RasterIO(GF_Read,j,i,1,1,shearVal,1,1,GDT_Float32,0,0);
                if (fabs(*shearVal) > maxShear)
                {
                    maxShear = *shearVal;
                }
            }
        }
    }

    CPLFree(shearVal);
    CPLFree(depthVal);
    shearVal = NULL;
    depthVal = NULL;

    return maxShear;
}

QVector<int> SedimentTransport::findNextCell(int startRow, int startCol, int prevRow, int prevCol)
{
    //QMutexLocker locker(&mutex);
    int tempRow = startRow, tempCol = startCol;
    QVector<int> address(2);
    double activeDirection;
    pFlowDir->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
    activeDirection = *fdirValue;

    if (activeDirection>=67.5 && activeDirection<112.5)
    {
        tempCol++;
        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
        if(*fdirValue <= 0.0)
        {
            if(activeDirection > 0.0)
            {
                tempRow--;
                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempRow+=2;
                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow-=2;
                        tempCol--;
                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow+=2;
                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow-=2;
                                tempCol--;
                                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow+=2;
                                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempRow--;
                                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                tempRow++;
                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempRow-=2;
                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow+=2;
                        tempCol--;
                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow-=2;
                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow+=2;
                                tempCol--;
                                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow-=2;
                                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempRow++;
                                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        address[0] = tempRow;
        address[1] = tempCol;
    }
    else if ((activeDirection>0.0 && activeDirection<=22.5) || (activeDirection<=360.0 && activeDirection>=337.5))
    {
        tempRow--;
        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
        if(*fdirValue <= 0.0)
        {
            if(activeDirection > 90.0)
            {
                tempCol--;
                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol+=2;
                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempCol-=2;
                        tempRow++;
                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempCol+=2;
                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempCol-=2;
                                tempRow++;
                                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempCol+=2;
                                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempCol--;
                                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                tempCol++;
                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol-=2;
                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempCol+=2;
                        tempRow++;
                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempCol-=2;
                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempCol+=2;
                                tempRow++;
                                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempCol-=2;
                                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempCol++;
                                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        address[0] = tempRow;
        address[1] = tempCol;
    }
    else if (activeDirection>=247.5 && activeDirection<=292.5)
    {
        tempCol--;
        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
        if(*fdirValue <= 0.0)
        {
            if(activeDirection < 180.0)
            {
                tempRow--;
                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempRow+=2;
                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow-=2;
                        tempCol++;
                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow+=2;
                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow-=2;
                                tempCol++;
                                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow+=2;
                                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempRow--;
                                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                tempRow++;
                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempRow-=2;
                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow+=2;
                        tempCol++;
                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow-=2;
                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow+=2;
                                tempCol++;
                                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow-=2;
                                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempRow++;
                                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        address[0] = tempRow;
        address[1] = tempCol;
    }
    else if (activeDirection>=157.5 && activeDirection<=202.5)
    {
        tempRow++;
        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
        if(*fdirValue <= 0.0)
        {
            if(activeDirection < 270.0)
            {
                tempCol--;
                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol+=2;
                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempCol-=2;
                        tempRow--;
                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempCol+=2;
                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempCol-=2;
                                tempRow--;
                                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempCol+=2;
                                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempCol--;
                                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                tempCol++;
                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol-=2;
                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempCol+=2;
                        tempRow--;
                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempCol-=2;
                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempCol+=2;
                                tempRow--;
                                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempCol-=2;
                                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempCol++;
                                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        address[0] = tempRow;
        address[1] = tempCol;
    }
    else if (activeDirection>22.5 && activeDirection<67.5)
    {
        tempCol++;
        tempRow--;
        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
        if(*fdirValue <= 0.0)
        {
            if(activeDirection > 45.0)
            {
                tempCol--;
                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol++;
                    tempRow++;
                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow--;
                        tempCol-=2;
                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow+=2;
                            tempCol+=2;
                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow--;
                                tempCol-=2;
                                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow++;
                                    tempCol++;
                                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempCol--;
                                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                tempRow++;
                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol--;
                    tempRow--;
                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow+=2;
                        tempCol++;
                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow-=2;
                            tempCol-=2;
                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow+=2;
                                tempCol++;
                                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow--;
                                    tempCol--;
                                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempRow++;
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        address[0] = tempRow;
        address[1] = tempCol;
    }
    else if (activeDirection>292.5 && activeDirection<337.5)
    {
        tempCol--;
        tempRow--;
        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
        if(*fdirValue <= 0.0)
        {
            if(activeDirection < 135.0)
            {
                tempCol++;
                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol--;
                    tempRow++;
                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow--;
                        tempCol+=2;
                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow+=2;
                            tempCol-=2;
                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow--;
                                tempCol+=2;
                                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow++;
                                    tempCol--;
                                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempCol++;
                                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                tempRow++;
                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol++;
                    tempRow--;
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow+=2;
                        tempCol--;
                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow-=2;
                            tempCol+=2;
                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow+=2;
                                tempCol--;
                                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow--;
                                    tempCol++;
                                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempRow++;
                                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        address[0] = tempRow;
        address[1] = tempCol;
    }
    else if (activeDirection>202.5 && activeDirection<247.5)
    {
        tempCol--;
        tempRow++;
        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
        if(*fdirValue <= 0.0)
        {
            if(activeDirection < 225.0)
            {
                tempCol++;
                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol--;
                    tempRow--;
                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow++;
                        tempCol+=2;
                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow-=2;
                            tempCol-=2;
                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow++;
                                tempCol+=2;
                                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow--;
                                    tempCol--;
                                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempCol++;
                                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                tempRow--;
                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol++;
                    tempRow++;
                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow-=2;
                        tempCol--;
                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow+=2;
                            tempCol+=2;
                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow-=2;
                                tempCol--;
                                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow++;
                                    tempCol++;
                                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempRow--;
                                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        address[0] = tempRow;
        address[1] = tempCol;
    }
    else if (activeDirection>112.5 && activeDirection<157.5)
    {
        tempCol++;
        tempRow++;
        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
        if(*fdirValue <= 0.0)
        {
            if(activeDirection < 315.0)
            {
                tempCol--;
                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol++;
                    tempRow--;
                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow++;
                        tempCol-=2;
                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow-=2;
                            tempCol+=2;
                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow++;
                                tempCol-=2;
                                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow--;
                                    tempCol++;
                                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempCol--;
                                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                tempRow--;
                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol--;
                    tempRow++;
                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow-=2;
                        tempCol++;
                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow+=2;
                            tempCol-=2;
                            pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow-=2;
                                tempCol++;
                                pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow++;
                                    tempCol--;
                                    pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempRow--;
                                        pFlowDir->RasterIO(GF_Read,tempCol,tempRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
        address[0] = tempRow;
        address[1] = tempCol;
    }
    else if (activeDirection == 0)
    {
        //std::cout<<"Flow direction 0 at "<<startRow<<" "<<startCol<<std::endl;
    }
    else
    {
        std::cout<<"Not sure what flow direction is, some problem here"<<std::endl;
    }

    return address;
}

QVector<int> SedimentTransport::findNextCellAveFlow(int startRow, int startCol, int prevRow, int prevCol)
{
    //QMutexLocker locker(&mutex);
    int tempRow = startRow, tempCol = startCol;
    QVector<int> address(2);
    double activeDirection;
    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
    activeDirection = *fdirValue;

    if (activeDirection>=67.5 && activeDirection<112.5)
    {
        tempCol++;
        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
        if(*fdirValue <= 0.0)
        {
            if(activeDirection > 0.0)
            {
                tempRow--;
                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempRow+=2;
                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow-=2;
                        tempCol--;
                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow+=2;
                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow-=2;
                                tempCol--;
                                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow+=2;
                                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempRow--;
                                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                tempRow++;
                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempRow-=2;
                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow+=2;
                        tempCol--;
                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow-=2;
                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow+=2;
                                tempCol--;
                                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow-=2;
                                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempRow++;
                                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        address[0] = tempRow;
        address[1] = tempCol;
    }
    else if ((activeDirection>0.0 && activeDirection<=22.5) || (activeDirection<=360.0 && activeDirection>=337.5))
    {
        tempRow--;
        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
        if(*fdirValue <= 0.0)
        {
            if(activeDirection > 90.0)
            {
                tempCol--;
                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol+=2;
                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempCol-=2;
                        tempRow++;
                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempCol+=2;
                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempCol-=2;
                                tempRow++;
                                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempCol+=2;
                                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempCol--;
                                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                tempCol++;
                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol-=2;
                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempCol+=2;
                        tempRow++;
                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempCol-=2;
                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempCol+=2;
                                tempRow++;
                                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempCol-=2;
                                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempCol++;
                                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        address[0] = tempRow;
        address[1] = tempCol;
    }
    else if (activeDirection>=247.5 && activeDirection<=292.5)
    {
        tempCol--;
        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
        if(*fdirValue <= 0.0)
        {
            if(activeDirection < 180.0)
            {
                tempRow--;
                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempRow+=2;
                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow-=2;
                        tempCol++;
                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow+=2;
                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow-=2;
                                tempCol++;
                                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow+=2;
                                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempRow--;
                                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                tempRow++;
                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempRow-=2;
                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow+=2;
                        tempCol++;
                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow-=2;
                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow+=2;
                                tempCol++;
                                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow-=2;
                                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempRow++;
                                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        address[0] = tempRow;
        address[1] = tempCol;
    }
    else if (activeDirection>=157.5 && activeDirection<=202.5)
    {
        tempRow++;
        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
        if(*fdirValue <= 0.0)
        {
            if(activeDirection < 270.0)
            {
                tempCol--;
                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol+=2;
                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempCol-=2;
                        tempRow--;
                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempCol+=2;
                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempCol-=2;
                                tempRow--;
                                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempCol+=2;
                                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempCol--;
                                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                tempCol++;
                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol-=2;
                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempCol+=2;
                        tempRow--;
                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempCol-=2;
                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempCol+=2;
                                tempRow--;
                                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempCol-=2;
                                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempCol++;
                                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        address[0] = tempRow;
        address[1] = tempCol;
    }
    else if (activeDirection>22.5 && activeDirection<67.5)
    {
        tempCol++;
        tempRow--;
        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
        if(*fdirValue <= 0.0)
        {
            if(activeDirection > 45.0)
            {
                tempCol--;
                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol++;
                    tempRow++;
                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow--;
                        tempCol-=2;
                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow+=2;
                            tempCol+=2;
                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow--;
                                tempCol-=2;
                                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow++;
                                    tempCol++;
                                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempCol--;
                                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                tempRow++;
                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol--;
                    tempRow--;
                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow+=2;
                        tempCol++;
                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow-=2;
                            tempCol-=2;
                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow+=2;
                                tempCol++;
                                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow--;
                                    tempCol--;
                                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempRow++;
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        address[0] = tempRow;
        address[1] = tempCol;
    }
    else if (activeDirection>292.5 && activeDirection<337.5)
    {
        tempCol--;
        tempRow--;
        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
        if(*fdirValue <= 0.0)
        {
            if(activeDirection < 135.0)
            {
                tempCol++;
                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol--;
                    tempRow++;
                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow--;
                        tempCol+=2;
                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow+=2;
                            tempCol-=2;
                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow--;
                                tempCol+=2;
                                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow++;
                                    tempCol--;
                                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempCol++;
                                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                tempRow++;
                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol++;
                    tempRow--;
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow+=2;
                        tempCol--;
                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow-=2;
                            tempCol+=2;
                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow+=2;
                                tempCol--;
                                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow--;
                                    tempCol++;
                                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempRow++;
                                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        address[0] = tempRow;
        address[1] = tempCol;
    }
    else if (activeDirection>202.5 && activeDirection<247.5)
    {
        tempCol--;
        tempRow++;
        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
        if(*fdirValue <= 0.0)
        {
            if(activeDirection < 225.0)
            {
                tempCol++;
                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol--;
                    tempRow--;
                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow++;
                        tempCol+=2;
                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow-=2;
                            tempCol-=2;
                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow++;
                                tempCol+=2;
                                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow--;
                                    tempCol--;
                                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempCol++;
                                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                tempRow--;
                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol++;
                    tempRow++;
                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow-=2;
                        tempCol--;
                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow+=2;
                            tempCol+=2;
                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow-=2;
                                tempCol--;
                                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow++;
                                    tempCol++;
                                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempRow--;
                                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        address[0] = tempRow;
        address[1] = tempCol;
    }
    else if (activeDirection>112.5 && activeDirection<157.5)
    {
        tempCol++;
        tempRow++;
        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
        if(*fdirValue <= 0.0)
        {
            if(activeDirection < 315.0)
            {
                tempCol--;
                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol++;
                    tempRow--;
                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow++;
                        tempCol-=2;
                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow-=2;
                            tempCol+=2;
                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow++;
                                tempCol-=2;
                                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow--;
                                    tempCol++;
                                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempCol--;
                                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                tempRow--;
                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                {
                    tempCol--;
                    tempRow++;
                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                    {
                        tempRow-=2;
                        tempCol++;
                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                        {
                            tempRow+=2;
                            tempCol-=2;
                            pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                            if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                            {
                                tempRow-=2;
                                tempCol++;
                                pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                {
                                    tempRow++;
                                    tempCol--;
                                    pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                    if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                    {
                                        tempRow--;
                                        pFlowDir5x5->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
                                        if(*fdirValue <= 0.0 || (prevRow == tempRow && prevCol == tempCol))
                                        {
                                            tempRow = startRow;
                                            tempCol = startCol;
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
        address[0] = tempRow;
        address[1] = tempCol;
    }
    else if (activeDirection == 0)
    {
        //std::cout<<"Flow direction 0 at "<<startRow<<" "<<startCol<<std::endl;
    }
    else
    {
        qDebug()<<"Not sure what flow direction is, some problem here "<<*fdirValue;
    }

    return address;
}

QVector<double> SedimentTransport::findNextCellCoords(double startX, double startY, int prevRow, int prevCol)
{
    int tempRow, tempCol, startRow, startCol;
    QVector<double> address(4);
    double activeDirection, yAdd, xAdd, newX, newY, oldX, oldY;
    oldX = startX;
    oldY = startY;
    startRow = round((transform[3] - oldY)/cellWidth);
    startCol = round((oldX - transform[0])/cellWidth);
    pFlowDir->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
    activeDirection = *fdirValue;

    //convert angle from geographic degrees to arithmetic degrees
    if (activeDirection > 0 && activeDirection <= 90.0)
    {
        activeDirection = 90.0 - activeDirection;
    }
    else if (activeDirection > 90.0 && activeDirection <= 180.0)
    {
        activeDirection = 270 + (180.0 - activeDirection);
    }
    else if (activeDirection > 180.0 && activeDirection <= 270.0)
    {
        activeDirection = 180.0 + (270.0 - activeDirection);
    }
    else if (activeDirection > 270.0 && activeDirection <= 360.0)
    {
        activeDirection = 90.0 + (360.0 - activeDirection);
    }

    activeDirection *= (Hydraulics::PI/180);

    yAdd = cellWidth * sin(activeDirection);
    xAdd = cellWidth * cos(activeDirection);

    newX = oldX + xAdd;
    newY = oldY + yAdd;

    tempRow = round((transform[3] - newY)/cellWidth);
    tempCol = round((newX - transform[0])/cellWidth);

    pFlowDir->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);

    if (*fdirValue > 0.0)
    {
        address[0] = tempRow;
        address[1] = tempCol;
        address[2] = newX;
        address[3] = newY;
    }
    else
    {
        QVector<int> nvAddress;
        nvAddress = findNextCell(startRow, startCol, prevRow, prevCol);
        address[0] = nvAddress[0];
        address[1] = nvAddress[1];
        address[2] = transform[0] + (nvAddress[1]*cellWidth);
        address[3] = transform[3] - (nvAddress[0]*cellWidth);
    }

    return address;
}

QVector<double> SedimentTransport::findNextCellCoordsBackward(double startX, double startY, int prevRow, int prevCol)
{
    int tempRow, tempCol, startRow, startCol;
    QVector<double> address(4);
    double activeDirection, yAdd, xAdd, newX, newY, oldX, oldY;
    oldX = startX;
    oldY = startY;
    startRow = round((transform[3] - oldY)/cellWidth);
    startCol = round((oldX - transform[0])/cellWidth);
    pFlowDir->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);
    activeDirection = *fdirValue;

    if (activeDirection <= 180.0)
    {
        activeDirection += 180.0;
    }
    else if(activeDirection > 180.0 && activeDirection <= 360.0)
    {
        activeDirection -= 180.0;
    }
    else
    {
        qDebug()<<"backward cell id problem: direction out of range";
    }

    //convert angle from geographic degrees to arithmetic degrees
    if (activeDirection > 0 && activeDirection <= 90.0)
    {
        activeDirection = 90.0 - activeDirection;
    }
    else if (activeDirection > 90.0 && activeDirection <= 180.0)
    {
        activeDirection = 270 + (180.0 - activeDirection);
    }
    else if (activeDirection > 180.0 && activeDirection <= 270.0)
    {
        activeDirection = 180.0 + (270.0 - activeDirection);
    }
    else if (activeDirection > 270.0 && activeDirection <= 360.0)
    {
        activeDirection = 90.0 + (360.0 - activeDirection);
    }

    activeDirection *= (Hydraulics::PI/180);

    yAdd = cellWidth * sin(activeDirection);
    xAdd = cellWidth * cos(activeDirection);

    newX = oldX + xAdd;
    newY = oldY + yAdd;

    tempRow = round((transform[3] - newY)/cellWidth);
    tempCol = round((newX - transform[0])/cellWidth);

    pFlowDir->RasterIO(GF_Read,startCol,startRow,1,1,fdirValue,1,1,GDT_Float32,0,0);

    if (*fdirValue > 0.0)
    {
        address[0] = tempRow;
        address[1] = tempCol;
        address[2] = newX;
        address[3] = newY;
    }
    else
    {
        QVector<int> nvAddress;
        nvAddress = findNextCell(startRow, startCol, prevRow, prevCol);
        address[0] = nvAddress[0];
        address[1] = nvAddress[1];
        address[2] = transform[0] + (nvAddress[1]*cellWidth);
        address[3] = transform[3] - (nvAddress[0]*cellWidth);
    }

    return address;
}

void SedimentTransport::idBanks()
{
    qDebug()<<"id banks";
    int xsize = nCols;
    int indexCell = xsize;
    float* rows3 = (float*) CPLMalloc(sizeof(float)*(xsize*3));
    float* bankVal = (float*) CPLMalloc(sizeof(float)*1);
    qDebug()<<"bank vars declared";
    for (int i=0; i<nRows-2; i++)
    {
        pDepth->RasterIO(GF_Read,0,i,xsize,3,rows3,xsize,3,GDT_Float32,0,0);
        for (int j=1; j<nCols-1; j++)
        {
            indexCell = xsize+j;
            if (rows3[indexCell] == 0.0 && (rows3[indexCell-xsize]>0.0 || rows3[indexCell+xsize]>0.0 || rows3[indexCell-1]>0.0 || rows3[indexCell+1]>0.0))
            {
                *bankVal = 1.0;
                pBankID->GetRasterBand(1)->RasterIO(GF_Write,j,(i+1),1,1,bankVal,1,1,GDT_Float32,0,0);
            }
        }
    }
    qDebug()<<"bank loop done";
    CPLFree(rows3);
    rows3 = NULL;
    CPLFree(bankVal);
    bankVal = NULL;
}

void SedimentTransport::idCandidateCells(int row, int col, double sedAmt, double grnSz)
{
    //QMutexLocker locker(&mutex);
    int prevRow, prevCol, prevRowHold, prevColHold;
    double sedRemain;
    QVector<double> cells;
    cells.resize(2);

    prevRow = 0, prevCol = 0;
    sedRemain = sedAmt;

    for (int p=0; p<PlBed->nLength; p++)
    {
        if (row>1 && row<nRows-2 && col>1 && col<nCols-2)
        {
            if (p == 0)
            {
                cells[0] = row;
                cells[1] = col;
                cells[2] = transform[0] + (col*cellWidth);
                cells[3] = transform[3] - (row*cellWidth);
            }
            prevRowHold = row, prevColHold = col;
            cells = findNextCellCoords(cells[2], cells[3], prevRow, prevCol);
            row = cells[0];
            col = cells[1];
            prevRow = prevRowHold, prevCol = prevColHold;

            if (row<2 || row>nRows-2 || col<2 || col>nCols-2)
            {
                inGS = ((exported*inGS)+(sedRemain*grnSz))/(sedRemain+exported);
                exported += sedRemain;
                break;
            }
            else
            {
                if (p < 3)
                {
                    setCandidateData3x3(row, col, sedAmt, grnSz, p);
                }
                else if (p >=3 )
                {
                    setCandidateData5x5(row, col, sedAmt, grnSz, p);
                }
                else
                {
                    qDebug()<<"no candidate distrib triggered";
                }
                sedRemain -= (sedAmt * PlBed->pathLength[p]);
            }
        }
        else
        {
            inGS = ((exported*inGS)+(sedRemain*grnSz))/(sedRemain+exported);
            exported += sedRemain;
            qDebug()<<"Index for erosion out of range "<<row<<" "<<col;
            break;
        }
    }
}

void SedimentTransport::idCandidateCellsImport(int row, int col, double sedAmt, double grnSz)
{
    //QMutexLocker locker(&mutex);
    int prevRow, prevCol, prevRowHold, prevColHold;
    double sedRemain;
    QVector<double> cells;
    cells.resize(2);

    prevRow = 0, prevCol = 0;
    sedRemain = sedAmt;

    for (int p=0; p<PlImport->nLength; p++)
    {
        if (row>0 && row<nRows-1 && col>0 && col<nCols-1)
        {
            if (p == 0)
            {
                cells[0] = row;
                cells[1] = col;
                cells[2] = transform[0] + (col*cellWidth);
                cells[3] = transform[3] - (row*cellWidth);
            }
            prevRowHold = row, prevColHold = col;
            cells = findNextCellCoords(cells[2], cells[3], prevRow, prevCol);
            row = cells[0];
            col = cells[1];
            prevRow = prevRowHold, prevCol = prevColHold;

//            if (prevRow == cells[0] && prevCol == cells[1])
//            {
//                qDebug()<<"ending import path length for repeat cell";
//                p = PlImport->nLength;
//                break;
//            }

            if (row<2 || row>nRows-2 || col<2 || col>nCols-2)
            {
                qDebug()<<"not importing at "<<row<<" "<<col;
                //inGS = ((exported*inGS)+(sedRemain*grnSz))/(sedRemain+exported);
                //exported += sedRemain;
                p = PlImport->nLength;
                break;
            }
            else
            {
                if (p < 3)//(PlImport->nLength/2.0))
                {
                    setCandidateData3x3Import(row, col, p);
                }
                else if (p >=3)//>= (PlImport->nLength/2.0))
                {
                    setCandidateData5x5Import(row, col, p);
                }
                else
                {
                    qDebug()<<"no candidate distrib triggered";
                }
                //sedRemain -= (sedAmt * PlImport->pathLength[p]);
            }
        }
        else
        {
            //inGS = ((exported*inGS)+(sedRemain*grnSz))/(sedRemain+exported);
            //exported += sedRemain;
            break;
        }
    }
}

void SedimentTransport::importSediment()
{
    double sedInCell;
    bool run = false;

    if (nImport == 1)
    {
        imported = import[nCurrentIteration];
        if (imported > 0.0)
        {
            run = true;
        }
    }
    else if (nImport == 2)
    {
        imported = exported*import[nCurrentIteration];
        if (imported > 0.0)
        {
            run = true;
        }
    }
    else
    {
        run = false;
        qDebug()<<"Not importing sediment";
    }

    if (run == true)
    {
        PlImport->setGaussian(PlBed->nLength,ceil(PlBed->nLength/4.0),ceil(PlBed->nLength/2.0));
        qDebug()<<"pl import length "<<PlImport->nLength<<" pl bed length "<<PlBed->nLength;
        findImportCells();

        sedInCell = exported/(rowIn.size()*1.0);
        qDebug()<<rowIn.size()<<" import cells";

        for (int i=0; i<rowIn.size(); i++)
        {
            idCandidateCellsImport(rowIn[i], colIn[i], sedInCell, inGS);
            //depositToCandidates();
        }
        depositToCandidatesImport();
    }
}

void SedimentTransport::runBankTransport()
{

}

void SedimentTransport::runBedTransport()
{
    double shieldsStress, shearCrit, sedimentAvailable, grainSize;
    float* grainSizeVal = (float*) CPLMalloc(sizeof(float)*1);
    float* shearVal = (float*) CPLMalloc(sizeof(float)*1);
    float* elevOld = (float*) CPLMalloc(sizeof(float)*1);
    float* elevNew = (float*) CPLMalloc(sizeof(float)*1);
    float* actBand = (float*) CPLMalloc(sizeof(float)*1);
    float* sVal = (float*) CPLMalloc(sizeof(float)*1);
    float* oldVal = (float*) CPLMalloc(sizeof(float)*1);

    calculateSlopeThirdOrderFinite(pOldSurface);

    int band;

    qDebug()<<"pl "<<PlBed->nLength;
    for (int i=2; i<nRows-2; i++)
    {
        for (int j=2; j<nCols-2; j++)
        {
            pInitialSurface->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,oldVal,1,1,GDT_Float32,0,0);
            if (*oldVal != fNoDataValue)
            {
                pShear->RasterIO(GF_Read,j,i,1,1,shearVal,1,1,GDT_Float32,0,0);

                if (*shearVal != fNoDataValue)
                {
                    GS->pActive->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,actBand,1,1,GDT_Float32,0,0);
                    band = *actBand;

                    GS->pMain->GetRasterBand(band)->RasterIO(GF_Read,j,i,1,1,grainSizeVal,1,1,GDT_Float32,0,0);
                    pSlope->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,sVal,1,1,GDT_Float32,0,0);

                    //calculate random shields stress between 0.030 and 0.070
                    shieldsStress = rand() % 30 + 40;
                    shieldsStress /= 1000;
                    //shields stress set to constant 0.05
                    shieldsStress = 0.05;

                    //recalculate shileds stress based on slope (angle)
                    shieldsStress = shieldsStress * sqrt(1.0 - pow(sin(*sVal),2));

                    //calculate critical shear stress for erosion to occur
                    shearCrit = shieldsStress * (RHO_S - RHO) * G * (*grainSizeVal);
                    shearCrit = 50;

                    //if shear is out of normal range set it to a max value
                    if (fabs(*shearVal) > shearCrit)
                    {
                        if (fabs(*shearVal) > 250.0)
                        {
                            *shearVal = 250.0;
                        }
                        //determine scour depth and erode sediment
                        sedimentAvailable = erodeBed5Cells(shearCrit, fabs(*shearVal), i , j);
                        bedErode += sedimentAvailable;
                        bedErodeTotal += sedimentAvailable;

                        //idCandidateCells(i,j,sedimentAvailable,grainSize);
                        //depositToCandidates();
                        runBedTransportSingleThread_5Paths(i,j,sedimentAvailable,grainSize);
                    }
                }
            }
        }
    }
    qDebug()<<"transport loop done";
    loadDrivers();
    qDebug()<<"drivers loaded";
    pBedE = pDriverTIFF->CreateCopy(qsBedE.toStdString().c_str(),pNewSurface,FALSE,NULL,NULL,NULL);
    QString qsDodPath = qsOutputs + "/GTIFF/BedErosion" + QString::number(nCurrentIteration+1) + ".tif";
    writeTempDoDs(qsDodPath,pOldSurface,pNewSurface);
    qDebug()<<"bed e dod written";

    depositToDEM(bedDepo);

    pBedD = pDriverTIFF->CreateCopy(qsBedD.toStdString().c_str(),pNewSurface,FALSE,NULL,NULL,NULL);
    qsDodPath = qsOutputs + "/GTIFF/BedDeposition" + QString::number(nCurrentIteration+1) + ".tif";
    writeTempDoDs(qsDodPath,pBedE,pNewSurface);
    qDebug()<<"bed d dod written";
    GDALClose(pBedE);
    pBedE = NULL;
    qDebug()<<"bed e dem closed";

    bedDepoTotal += bedDepo;
    qDebug()<<"bed transport functions finished";

    CPLFree(grainSizeVal);
    CPLFree(shearVal);
    CPLFree(elevOld);
    CPLFree(elevNew);
    CPLFree(actBand);
    CPLFree(sVal);
    CPLFree(oldVal);
    actBand = NULL;
    grainSizeVal = NULL;
    shearVal = NULL;
    elevOld = NULL;
    elevNew = NULL;
    sVal = NULL;
    oldVal = NULL;
}

void SedimentTransport::runBedTransportSingleThread_5Paths(int row, int col, double sedAvailable, double gs)
{
    double activeDirection, cellSed;
    int wetCells;
    QVector<int> rows, cols;
    float *fDirWindow = (float*) CPLMalloc(sizeof(float)*9);
    pFlowDir->RasterIO(GF_Read,col-1,row-1,3,3,fDirWindow,3,3,GDT_Float32,0,0);
    activeDirection = fDirWindow[12];
    //qDebug()<<"start direction "<<activeDirection;

    wetCells = 0;

    if (activeDirection>=67.5 && activeDirection<112.5)
    {
        for (int i=0; i<9; i++)
        {
            if (i==1 || i==2 || i==5 || i==7 || i==8 )
            {
                //qDebug()<<"depth at start cell: "<<fDirWindow[i];
                if (fDirWindow[i] > 0.0)
                {
                    wetCells++;
                    rows.append(row + ROW_OFFSET[i]);
                    cols.append(col + COL_OFFSET[i]);
                }
            }
        }

    }
    else if ((activeDirection>0.0 && activeDirection<=22.5) || (activeDirection<=360.0 && activeDirection>=337.5))
    {
        for (int i=0; i<9; i++)
        {
            if (i==1 || i==2 || i==5 || i==0 || i==3 )
            {
                if (fDirWindow[i] > 0.0)
                {
                    //qDebug()<<"depth at start cell: "<<fDirWindow[i];
                    wetCells++;
                    rows.append(row + ROW_OFFSET[i]);
                    cols.append(col + COL_OFFSET[i]);
                }
            }
        }
    }
    else if (activeDirection>=247.5 && activeDirection<=292.5)
    {
        for (int i=0; i<9; i++)
        {
            if (i==1 || i==0 || i==3 || i==7 || i==6 )
            {
                //qDebug()<<"depth at start cell: "<<fDirWindow[i];
                if (fDirWindow[i] > 0.0)
                {
                    wetCells++;
                    rows.append(row + ROW_OFFSET[i]);
                    cols.append(col + COL_OFFSET[i]);
                }
            }
        }
    }
    else if (activeDirection>=157.5 && activeDirection<=202.5)
    {
        for (int i=0; i<9; i++)
        {
            if (i==3 || i==6 || i==5 || i==7 || i==8 )
            {
                //qDebug()<<"depth at start cell: "<<fDirWindow[i];
                if (fDirWindow[i] > 0.0)
                {
                    wetCells++;
                    rows.append(row + ROW_OFFSET[i]);
                    cols.append(col + COL_OFFSET[i]);
                }
            }
        }
    }
    else if (activeDirection>22.5 && activeDirection<67.5)
    {
        for (int i=0; i<9; i++)
        {
            if (i==1 || i==2 || i==5 || i==0 || i==8 )
            {
                //qDebug()<<"depth at start cell: "<<fDirWindow[i];
                if (fDirWindow[i] > 0.0)
                {
                    wetCells++;
                    rows.append(row + ROW_OFFSET[i]);
                    cols.append(col + COL_OFFSET[i]);
                }
            }
        }
    }
    else if (activeDirection>292.5 && activeDirection<337.5)
    {
        for (int i=0; i<9; i++)
        {
            if (i==1 || i==2 || i==0 || i==3 || i==6 )
            {
                //qDebug()<<"depth at start cell: "<<fDirWindow[i];
                if (fDirWindow[i] > 0.0)
                {
                    wetCells++;
                    rows.append(row + ROW_OFFSET[i]);
                    cols.append(col + COL_OFFSET[i]);
                }
            }
        }
    }
    else if (activeDirection>202.5 && activeDirection<247.5)
    {
        for (int i=0; i<9; i++)
        {
            if (i==0 || i==3 || i==6 || i==7 || i==8 )
            {
                //qDebug()<<"depth at start cell: "<<fDirWindow[i];
                if (fDirWindow[i] > 0.0)
                {
                    wetCells++;
                    rows.append(row + ROW_OFFSET[i]);
                    cols.append(col + COL_OFFSET[i]);
                }
            }
        }
    }
    else if (activeDirection>112.5 && activeDirection<157.5)
    {

        for (int i=0; i<9; i++)
        {
            if (i==6 || i==2 || i==5 || i==7 || i==8 )
            {
                //qDebug()<<"depth at start cell: "<<fDirWindow[i];
                if (fDirWindow[i] > 0.0)
                {
                    wetCells++;
                    rows.append(row + ROW_OFFSET[i]);
                    cols.append(col + COL_OFFSET[i]);
                }
            }
        }
    }
    else
    {

    }

    cellSed = sedAvailable/wetCells;
    //qDebug()<<"wet cells "<<wetCells<<" sediment per cell "<<cellSed;

    for (int i=0; i<wetCells; i++)
    {
        //find candidate cells to deposit sediment
        idCandidateCells(rows[i], cols[i], cellSed, gs);

        //depsoit sediment and adjust grainsize
        depositToCandidates();
    }
    rows.clear();
    cols.clear();

    CPLFree(fDirWindow);
    fDirWindow = NULL;
}

void SedimentTransport::runBedTransportSingleThread_10Paths(int row, int col, double sedAvailable, double gs)
{
    double activeDirection, cellSed;
    int wetCells;
    QVector<int> rows, cols;
    float *fDirWindow = (float*) CPLMalloc(sizeof(float)*25);
    pFlowDir->RasterIO(GF_Read,col-2,row-2,5,5,fDirWindow,5,5,GDT_Float32,0,0);
    activeDirection = fDirWindow[12];

    wetCells = 0;

    if (activeDirection>=67.5 && activeDirection<112.5)
    {
        for (int i=0; i<25; i++)
        {
            if (i==3 || i==4 || i==8 || i==9 || i==13 || i==14 || i==18 || i==19 || i==23 || i==24)
            {
                if (fDirWindow[i] > 0.0)
                {
                    wetCells++;
                    rows.append(row + ROW_OFFSET5[i]);
                    cols.append(col + COL_OFFSET5[i]);
                }
            }
        }

    }
    else if ((activeDirection>0.0 && activeDirection<=22.5) || (activeDirection<=360.0 && activeDirection>=337.5))
    {
        for (int i=0; i<10; i++)
        {
            if (fDirWindow[i] > 0.0)
            {
                wetCells++;
                rows.append(row + ROW_OFFSET5[i]);
                cols.append(col + COL_OFFSET5[i]);
            }
        }
    }
    else if (activeDirection>=247.5 && activeDirection<=292.5)
    {
        for (int i=0; i<25; i++)
        {
            if (i==0 || i==1 || i==5 || i==6 || i==10 || i==11 || i==15 || i==16 || i==20 || i==21)
            {
                if (fDirWindow[i] > 0.0)
                {
                    wetCells++;
                    rows.append(row + ROW_OFFSET5[i]);
                    cols.append(col + COL_OFFSET5[i]);
                }
            }
        }
    }
    else if (activeDirection>=157.5 && activeDirection<=202.5)
    {
        for (int i=15; i<25; i++)
        {
            if (fDirWindow[i] > 0.0)
            {
                wetCells++;
                rows.append(row + ROW_OFFSET5[i]);
                cols.append(col + COL_OFFSET5[i]);
            }
        }
    }
    else if (activeDirection>22.5 && activeDirection<67.5)
    {
        for (int i=0; i<25; i++)
        {
            if (i==1 || i==2 || i==3 || i==4 || i==7|| i==8 || i==9 || i==13 || i==14 || i==19)
            {
                if (fDirWindow[i] > 0.0)
                {
                    wetCells++;
                    rows.append(row + ROW_OFFSET5[i]);
                    cols.append(col + COL_OFFSET5[i]);
                }
            }
        }
    }
    else if (activeDirection>292.5 && activeDirection<337.5)
    {
        for (int i=0; i<25; i++)
        {
            if (i==1 || i==2 || i==3 || i==0 || i==7|| i==6 || i==5|| i==10 || i==11 || i==15)
            {
                if (fDirWindow[i] > 0.0)
                {
                    wetCells++;
                    rows.append(row + ROW_OFFSET5[i]);
                    cols.append(col + COL_OFFSET5[i]);
                }
            }
        }
    }
    else if (activeDirection>202.5 && activeDirection<247.5)
    {
        for (int i=0; i<25; i++)
        {
            if (i==5 || i==10 || i==11 || i==15 || i==16 || i==17 || i==20 || i==21 || i==22 || i==23)
            {
                if (fDirWindow[i] > 0.0)
                {
                    wetCells++;
                    rows.append(row + ROW_OFFSET5[i]);
                    cols.append(col + COL_OFFSET5[i]);
                }
            }
        }
    }
    else if (activeDirection>112.5 && activeDirection<157.5)
    {

        for (int i=0; i<25; i++)
        {
            if (i==9 || i==13 || i==14 || i==17 || i==18 || i==19 || i==21 || i==22 || i==23 || i==24)
            {
                if (fDirWindow[i] > 0.0)
                {
                    wetCells++;
                    rows.append(row + ROW_OFFSET5[i]);
                    cols.append(col + COL_OFFSET5[i]);
                }
            }
        }
    }
    else
    {

    }

    cellSed = sedAvailable/wetCells;

    for (int i=0; i<wetCells; i++)
    {
        //find candidate cells to deposit sediment
        idCandidateCells(rows[i], cols[i], cellSed, gs);

        //depsoit sediment and adjust grainsize
        depositToCandidates();
    }
    rows.clear();
    cols.clear();

    CPLFree(fDirWindow);
    fDirWindow = NULL;
}

void SedimentTransport::setCandidateData3x3(int row, int col, double sedAmt, double grnSz, int pl)
{
    //QMutexLocker locker(&mutex);
    QVector<double> matrix;
    matrix.resize(2);
    double reassign;
    int wetCells, dryCells;

    matrix[0] = 0.2;
    matrix[1] = 0.1;
    wetCells = 0, dryCells = 0;

    //id wet and dry cells, center cell will always be wet
    for (int i=(row-1); i<(row+2); i++)
    {
        for (int j=(col-1); j<(col+2); j++)
        {
            pDepth->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
            if (*depth > 0.0)
            {
                wetCells++;
            }
            else
            {
                dryCells++;
            }
        }
    }

    //re-scale the matrix if there are dry cells
    if (dryCells > 0)
    {
        reassign = ((1.0)/(wetCells+1.0));
        matrix[0] = reassign;
        matrix[1] = reassign*2;
    }

    //add each wet cell and its data to candidate cell vectors
    for (int i=(row-1); i<(row+2); i++)
    {
        for (int j=(col-1); j<(col+2); j++)
        {
            pDepth->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
            if (*depth > 0.0)
            {
                rowCand.append(i);
                colCand.append(j);
                propPl.append(PlBed->pathLength[pl]);
                volErode.append(sedAmt);
                gsVec.append(grnSz);

                if(i == row && j == col)
                {
                    propCell.append(matrix[0]);
                    volDep.append(PlBed->pathLength[pl]*matrix[0]*sedAmt);
                    //qDebug()<<"Deposited "<<(PlBed->pathLength[pl]*matrix[0]*sedAmt)<<" sedAmt "<<sedAmt<<" scale val "<<matrix[0]<<" pl "<<PlBed->pathLength[pl];
                }
                else
                {
                    propCell.append(matrix[1]);
                    volDep.append(PlBed->pathLength[pl]*matrix[1]*sedAmt);
                    //qDebug()<<"Deposited "<<(PlBed->pathLength[pl]*matrix[1]*sedAmt)<<" sedAmt "<<sedAmt<<" scale val "<<matrix[1]<<" pl "<<PlBed->pathLength[pl];
                }
            }
        }
    }
    //qDebug()<<"3x3 wet "<<wetCells;
}

void SedimentTransport::setCandidateData3x3(int row, int col, double sedAmt, double grnSz, int pl, PathLengthDistribution *PlBed)
{
    //QMutexLocker locker(&mutex);
    QVector<double> matrix;
    matrix.resize(2);
    double reassign;
    int wetCells, dryCells;

    matrix[0] = 0.2;
    matrix[1] = 0.1;
    wetCells = 0, dryCells = 0;

    //id wet and dry cells, center cell will always be wet
    for (int i=(row-1); i<(row+2); i++)
    {
        for (int j=(col-1); j<(col+2); j++)
        {
            pDepth->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
            if (*depth > 0.0)
            {
                wetCells++;
            }
            else
            {
                dryCells++;
            }
        }
    }

    //re-scale the matrix if there are dry cells
    if (dryCells > 0)
    {
        reassign = ((1.0)/(wetCells+1.0));
        matrix[0] = reassign;
        matrix[1] = reassign*2;
    }

    //add each wet cell and its data to candidate cell vectors
    for (int i=(row-1); i<(row+2); i++)
    {
        for (int j=(col-1); j<(col+2); j++)
        {
            pDepth->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
            if (*depth > 0.0)
            {
                rowCand.append(i);
                colCand.append(j);
                propPl.append(PlBed->pathLength[pl]);
                volErode.append(sedAmt);
                gsVec.append(grnSz);

                if(i == row && j == col)
                {
                    propCell.append(matrix[0]);
                    volDep.append(PlBed->pathLength[pl]*matrix[0]*sedAmt);
                    //qDebug()<<"Deposited "<<(PlBed->pathLength[pl]*matrix[0]*sedAmt)<<" sedAmt "<<sedAmt<<" scale val "<<matrix[0]<<" pl "<<PlBed->pathLength[pl];
                }
                else
                {
                    propCell.append(matrix[1]);
                    volDep.append(PlBed->pathLength[pl]*matrix[1]*sedAmt);
                    //qDebug()<<"Deposited "<<(PlBed->pathLength[pl]*matrix[1]*sedAmt)<<" sedAmt "<<sedAmt<<" scale val "<<matrix[1]<<" pl "<<PlBed->pathLength[pl];
                }
            }
        }
    }
}

void SedimentTransport::setCandidateData3x3Import(int row, int col, int pl)
{
    //QMutexLocker locker(&mutex);
    for (int i=(row-1); i<(row+2); i++)
    {
        for (int j=(col-1); j<(col+2); j++)
        {
            pDepth->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
            if (*depth > 0.0)
            {
                rowCand.append(i);
                colCand.append(j);
                plIn.append(pl);
            }
        }
    }
}

void SedimentTransport::setCandidateData3x3Uniform(int row, int col, double sedAmt, double grnSz, int pl)
{
    int wetCells = 0, exportCells = 0;
    double cellAmt;
    for (int i=(row-1); i<(row+2); i++)
    {
        for (int j=(col-1); j<(col+2); j++)
        {
            if (i>1 && j>1 && i<nRows-2 && j<nCols-2)
            {
                pDepth->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
                if (*depth > 0.0)
                {
                    wetCells++;
                }
            }
            else
            {
                exportCells ++;
            }
        }
    }
    cellAmt = (sedAmt*PlBed->pathLength[pl])/(wetCells+exportCells);
    for (int i=(row-1); i<(row+2); i++)
    {
        for (int j=(col-1); j<(col+2); j++)
        {
            if (i>1 && j>1 && i<nRows-2 && j<nCols-2)
            {
                pDepth->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
                if (*depth > 0.0)
                {
                    rowCand.append(i);
                    colCand.append(j);
                    propPl.append(PlBed->pathLength[pl]);
                    volErode.append(sedAmt);
                    gsVec.append(grnSz);
                    volDep.append(cellAmt);
                }
            }
            else
            {
                exported += cellAmt;
            }
        }
    }
}

void SedimentTransport::setCandidateData5x5(int row, int col, double sedAmt, double grnSz, int pl)
{
    //QMutexLocker locker(&mutex);
    QVector<double> matrix;
    matrix.resize(3);
    double reassign;
    int wetCellsOut, wetCellsIn, wetCells, dryCellsOut, dryCellsIn;

    matrix[0] = 0.08571, matrix[1] = 0.054714, matrix[2] = 0.02857;
    wetCells = 1, wetCellsIn = 0, wetCellsOut = 0;
    dryCellsIn = 0, dryCellsOut = 0;

    //ID wet and dry cells
    for (int i=(row-2); i<(row+3); i++)
    {
        for (int j=(col-2); j<(col+3); j++)
        {
            pDepth->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
            if (*depth > 0.0)
            {
                if (i==(row-2) || i==(row+2) || j==(col-2) || j==(col+2))
                {
                    wetCellsOut++;
                    wetCells++;
                }
                else if (i==(row-1) || i==(row+1) || j==(col-1) || j==(col+1))
                {
                    wetCellsIn++;
                    wetCells++;
                }
            }
            else
            {
                if (i==(row-2) || i==(row+2) || j==(col-2) || j==(col+2))
                {
                    dryCellsOut++;
                }
                else if (i==(row-1) || i==(row+1) || j==(col-1) || j==(col+1))
                {
                    dryCellsIn++;
                }
            }
        }
    }

    //Re-scale matrix values if there are dry cells
    if ((dryCellsIn+dryCellsOut) > 0)
    {
        reassign = ((1.0)/(wetCellsOut + (wetCellsIn*2.0) + 3.0));
        matrix[2] = reassign;
        matrix[1] = reassign * 2;
        matrix[0] = reassign * 3;
    }

    //Add each wet cell and its data to candidate cell vectors
    for (int i=(row-2); i<(row+3); i++)
    {
        for (int j=(col-2); j<(col+3); j++)
        {
            pDepth->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
            if (*depth > 0.0)
            {
                rowCand.append(i);
                colCand.append(j);
                propPl.append(PlBed->pathLength[pl]);
                gsVec.append(grnSz);
                volErode.append(sedAmt);

                if (i==(row-2) || i==(row+2) || j==(col-2) || j==(col+2))
                {
                    propCell.append(matrix[2]);
                    volDep.append(PlBed->pathLength[pl]*matrix[2]*sedAmt);
                }
                else if (i==(row-1) || i==(row+1) || j==(col-1) || j==(col+1))
                {
                    propCell.append(matrix[1]);
                    volDep.append(PlBed->pathLength[pl]*matrix[1]*sedAmt);
                }
                else if (i==row && j==col)
                {
                    propCell.append(matrix[0]);
                    volDep.append(PlBed->pathLength[pl]*matrix[0]*sedAmt);
                }
            }
        }
    }
    //qDebug()<<"5x5 wet "<<wetCells;
}

void SedimentTransport::setCandidateData5x5(int row, int col, double sedAmt, double grnSz, int pl, PathLengthDistribution *PlBed)
{
    //QMutexLocker locker(&mutex);
    QVector<double> matrix;
    matrix.resize(3);
    double reassign;
    int wetCellsOut, wetCellsIn, wetCells, dryCellsOut, dryCellsIn;

    matrix[0] = 0.08571, matrix[1] = 0.054714, matrix[2] = 0.02857;
    wetCells = 1, wetCellsIn = 0, wetCellsOut = 0;
    dryCellsIn = 0, dryCellsOut = 0;

    //ID wet and dry cells
    for (int i=(row-2); i<(row+3); i++)
    {
        for (int j=(col-2); j<(col+3); j++)
        {
            pDepth->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
            if (*depth > 0.0)
            {
                if (i==(row-2) || i==(row+2) || j==(col-2) || j==(col+2))
                {
                    wetCellsOut++;
                    wetCells++;
                }
                else if (i==(row-1) || i==(row+1) || j==(col-1) || j==(col+1))
                {
                    wetCellsIn++;
                    wetCells++;
                }
            }
            else
            {
                if (i==(row-2) || i==(row+2) || j==(col-2) || j==(col+2))
                {
                    dryCellsOut++;
                }
                else if (i==(row-1) || i==(row+1) || j==(col-1) || j==(col+1))
                {
                    dryCellsIn++;
                }
            }
        }
    }

    //Re-scale matrix values if there are dry cells
    if ((dryCellsIn+dryCellsOut) > 0)
    {
        reassign = ((1.0)/(wetCellsOut + (wetCellsIn*2.0) + 3.0));
        matrix[2] = reassign;
        matrix[1] = reassign * 2;
        matrix[0] = reassign * 3;
    }

    //Add each wet cell and its data to candidate cell vectors
    for (int i=(row-2); i<(row+3); i++)
    {
        for (int j=(col-2); j<(col+3); j++)
        {
            pDepth->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
            if (*depth > 0.0)
            {
                rowCand.append(i);
                colCand.append(j);
                propPl.append(PlBed->pathLength[pl]);
                gsVec.append(grnSz);
                volErode.append(sedAmt);

                if (i==(row-2) || i==(row+2) || j==(col-2) || j==(col+2))
                {
                    propCell.append(matrix[2]);
                    volDep.append(PlBed->pathLength[pl]*matrix[2]*sedAmt);
                }
                else if (i==(row-1) || i==(row+1) || j==(col-1) || j==(col+1))
                {
                    propCell.append(matrix[1]);
                    volDep.append(PlBed->pathLength[pl]*matrix[1]*sedAmt);
                }
                else if (i==row && j==col)
                {
                    propCell.append(matrix[0]);
                    volDep.append(PlBed->pathLength[pl]*matrix[0]*sedAmt);
                }
            }
        }
    }
}

void SedimentTransport::setCandidateData5x5Import(int row, int col, int pl)
{
    //QMutexLocker locker(&mutex);
    for (int i=(row-2); i<(row+3); i++)
    {
        for (int j=(col-2); j<(col+3); j++)
        {
            pDepth->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
            if (*depth > 0.0)
            {
                rowCand.append(i);
                colCand.append(j);
                plIn.append(pl);
            }
        }
    }
}

void SedimentTransport::setCandidateData5x5Uniform(int row, int col, double sedAmt, double grnSz, int pl)
{
    int wetCells = 0, exportCells = 0;
    double cellAmt;
    for (int i=(row-2); i<(row+3); i++)
    {
        for (int j=(col-2); j<(col+3); j++)
        {
            if (i>1 && j>1 && i<nRows-2 && j<nCols-2)
            {
                pDepth->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
                if (*depth > 0.0)
                {
                    wetCells++;
                }
            }
            else
            {
                exportCells ++;
            }
        }
    }
    cellAmt = (sedAmt*PlBed->pathLength[pl])/(wetCells+exportCells);
    for (int i=(row-2); i<(row+3); i++)
    {
        for (int j=(col-2); j<(col+3); j++)
        {
            if (i>1 && j>1 && i<nRows-2 && j<nCols-2)
            {
                pDepth->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
                if (*depth > 0.0)
                {
                    rowCand.append(i);
                    colCand.append(j);
                    propPl.append(PlBed->pathLength[pl]);
                    volErode.append(sedAmt);
                    gsVec.append(grnSz);
                    volDep.append(cellAmt);
                }
            }
            else
            {
                exported += cellAmt;
            }
        }
    }
}

void SedimentTransport::setCandidateData7x7Uniform(int row, int col, double sedAmt, double grnSz, int pl)
{
    int wetCells = 0, exportCells = 0;
    double cellAmt;
    for (int i=(row-3); i<(row+4); i++)
    {
        for (int j=(col-3); j<(col+4); j++)
        {
            if (i>1 && j>1 && i<nRows-2 && j<nCols-2)
            {
                pDepth->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
                if (*depth > 0.0)
                {
                    wetCells++;
                }
            }
            else
            {
                exportCells ++;
            }
        }
    }
    cellAmt = (sedAmt*PlBed->pathLength[pl])/(wetCells+exportCells);
    for (int i=(row-3); i<(row+4); i++)
    {
        for (int j=(col-3); j<(col+4); j++)
        {
            if (i>1 && j>1 && i<nRows-2 && j<nCols-2)
            {
                pDepth->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
                if (*depth > 0.0)
                {
                    rowCand.append(i);
                    colCand.append(j);
                    propPl.append(PlBed->pathLength[pl]);
                    volErode.append(sedAmt);
                    gsVec.append(grnSz);
                    volDep.append(cellAmt);
                }
            }
            else
            {
                exported += cellAmt;
            }
        }
    }
}

void SedimentTransport::setupHydraulics()
{
    HydroData->setData();
    HydroData->calculateFlowDirection();
    qDebug()<<"calculating 5x5 flow";
    HydroData->calculateFlowDirection5x5();
    qDebug()<<"finished calculating 5x5 flow";
    //HydroData->thresholdWaterDepth();

    HydroData->closeDatasets();

    pFlowDirDS = (GDALDataset*) GDALOpen(HydroData->qsFlowDir.toStdString().c_str(),GA_ReadOnly);
    pShearDS = (GDALDataset*) GDALOpen(HydroData->qsShear.toStdString().c_str(), GA_ReadOnly);
    pDepthDS = (GDALDataset*) GDALOpen(HydroData->qsDepth.toStdString().c_str(), GA_ReadOnly);

    pFlowDir = pFlowDirDS->GetRasterBand(1);
    pFlowDir5x5 = pFlowDirDS->GetRasterBand(3);
    pShear = pShearDS->GetRasterBand(1);
    pDepth = pDepthDS->GetRasterBand(1);
}

void SedimentTransport::updateIteration()
{
    incrementCurrentIteration();
    HydroData->incrementCurrentIteration();
    Veg->incrementCurrentIteration();
    GS->incrementCurrentIteration();
    PlBed->incrementCurrentIteration();
    PlImport->incrementCurrentIteration();
    exported = 0.0, imported = 0.0, importDepo = 0.0, bedErode = 0.0, bedDepo = 0.0, bankErode = 0.0, bankDepo = 0.0;
    setData();
    //GS->openGSDatasets();
    //Veg->openVegDatasets();
}

void SedimentTransport::writeTempDoDs(QString qsDodPath, GDALDataset *pOld, GDALDataset *pNew)
{
    GDALDataset *pDodDS;

    pDodDS = pDriverTIFF->Create(qsDodPath.toStdString().c_str(),nCols,nRows, 1, GDT_Float32,NULL);
    calculateDoD(pOld,pNew,pDodDS);

    GDALClose(pDodDS);
    pDodDS = NULL;
}

}
