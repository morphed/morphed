#ifndef HYDRAULICS_H
#define HYDRAULICS_H

#include "morphedbaseclass.h"

namespace MORPH{

class Hydraulics : public MORPHEDBaseClass
{
    friend class SedimentTransport;

private:

protected:
    GDALDataset *pXvel, *pYvel, *pShear, *pDepth, *pFlowDir;
    QString qsXvel, qsYvel, qsShear, qsDepth, qsFlowDir;
public:
    static const double WD_THRESH = 0.05;
    static const double PI = 3.14159265;

    Hydraulics(QString xmlPath);
    Hydraulics(XMLReadWrite &XmlObj);
    ~Hydraulics();

    void setData();

    QVector<double> calculateBraidIndex(GDALRasterBand *pDepthBand);
    void calculateFlowDirection();
    void calculateFlowDirection5x5();
    void calculateFlowMagnitude();
    void closeDatasets();
    void thresholdWaterDepth();
};

}

#endif // HYDRAULICS_H
