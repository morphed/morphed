#include "symbology.h"
#include <cmath>

Symbology::Symbology()
{
}

void Symbology::createHillshadeAsPNG(GDALDataset *pOldDS, QString qsPngPath, QString qsTempDir)
{
    GDALAllRegister();
    GDALDataset *pSymbolDS, *pFinalDS;
    GDALDriver *pDriverTiff, *pDriverPng;
    pDriverTiff = GetGDALDriverManager()->GetDriverByName("GTiff");
    pDriverPng = GetGDALDriverManager()->GetDriverByName("PNG");
    QString qsTempTiffPath = qsTempDir + "/tempdem.tif";

    int rows, cols;
    double ndv;
    double transform[6];
    pOldDS->GetGeoTransform(transform);
    rows = pOldDS->GetRasterBand(1)->GetYSize();
    cols = pOldDS->GetRasterBand(1)->GetXSize();
    ndv = pOldDS->GetRasterBand(1)->GetNoDataValue();

    pSymbolDS = pDriverTiff->Create(qsTempTiffPath.toStdString().c_str(),cols,rows,1,GDT_Byte,NULL);

    double altDeg, zenDeg, zenRad, azimuth, azimuthRad, azimuthMath, dzdx, dzdy, aspectRad, slopeRad, zFactor, hlsdByte, min, max;
    altDeg = 45.0;
    azimuth = 315.0;
    zFactor = 1.0;
    zenDeg = 90.0 - altDeg;
    zenRad = zenDeg * PI / 180.0;
    azimuthMath = 360.0 - azimuth + 90.0;
    if (azimuthMath >= 360.0)
    {
        azimuthMath = azimuthMath - 360.0;
    }
    azimuthRad = azimuthMath * PI / 180.0;

    float *demVals = (float*) CPLMalloc(sizeof(float)*9);
    unsigned char *newRow = (unsigned char*) CPLMalloc(sizeof(int)*cols);

    for (int i=1; i<rows-1; i++)
    {
        newRow[0] = 0, newRow[cols-1] = 0;
        for (int j=1; j<cols-1; j++)
        {
            pOldDS->GetRasterBand(1)->RasterIO(GF_Read,j-1,i-1,3,3,demVals,3,3,GDT_Float32,0,0);

            if (demVals[0] == ndv || demVals[1] == ndv || demVals[2] == ndv || demVals[3] == ndv || demVals[4] == ndv || demVals[5] == ndv || demVals[6] == ndv || demVals[7] == ndv || demVals[8] == ndv)
            {
                newRow[j] = 0;
            }
            else
            {
                dzdx = ((demVals[2]+(2*demVals[5])+demVals[8]) - (demVals[0]+(2*demVals[3])+demVals[6])) / (8*transform[1]);
                dzdy = ((demVals[6]+(2*demVals[7])+demVals[8]) - (demVals[0]+(2*demVals[1])+demVals[2])) / (8*transform[1]);
                slopeRad = atan(zFactor * sqrt(pow(dzdx,2)+pow(dzdy,2)));

                if (dzdx != 0.0)
                {
                    aspectRad = atan2(dzdy, (dzdx*(-1.0)));
                    if (aspectRad < 0.0)
                    {
                        aspectRad = 2.0 * PI + aspectRad;
                    }
                }
                else
                {
                    if (dzdy > 0.0)
                    {
                        aspectRad = PI / 2.0;
                    }
                    else if (dzdy < 0.0)
                    {
                        aspectRad = 2.0*PI - PI/2.0;
                    }
                    else
                    {
                        aspectRad = aspectRad;
                    }
                }

                hlsdByte = round(254 * ((cos(zenRad) * cos(slopeRad)) + (sin(zenRad) * sin(slopeRad) * cos(azimuthRad - aspectRad)))) + 1.0;
                newRow[j] = hlsdByte;
            }
        }
        pSymbolDS->GetRasterBand(1)->RasterIO(GF_Write,0,i,cols,1,newRow,cols,1,GDT_Byte,0,0);
    }
    pSymbolDS->GetRasterBand(1)->SetColorInterpretation(GCI_PaletteIndex);
    pSymbolDS->GetRasterBand(1)->GetStatistics(FALSE, TRUE, &min, &max, NULL, NULL);
    GDALColorTable table(GPI_RGB);
    GDALColorEntry black, white, trans;
    black.c1 = 1, black.c2 = 1, black.c3 = 1, black.c4 = 100;
    white.c1 = 255, white.c2 = 255, white.c3 = 255, white.c4 = 100;
    trans.c1 = 255, trans.c2 = 255, trans.c3 = 255, trans.c4 = 0;
    table.CreateColorRamp(1,&black,255,&white);
    table.SetColorEntry(0,&trans);
    pSymbolDS->GetRasterBand(1)->SetColorTable(&table);
    qDebug()<<"copy to png";
    pFinalDS = pDriverPng->CreateCopy(qsPngPath.toStdString().c_str(),pSymbolDS,FALSE,NULL,NULL,NULL);
    pFinalDS->SetGeoTransform(transform);
    qDebug()<<"close datasets";
    GDALClose(pFinalDS);
    GDALClose(pSymbolDS);
    QFile::remove(qsTempTiffPath);
    qDebug()<<"freeing memory";
    CPLFree(demVals);
    CPLFree(newRow);
    demVals = NULL;
    newRow = NULL;
    qDebug()<<"finished";
}

//will not work if there are negative elevation values
void Symbology::symbolizeDEMasPNG(GDALDataset *pOldDS, QString qsPngPath, QString qsTempDir)
{
    GDALAllRegister();
    GDALDataset *pSymbolDS, *pFinalDS;
    GDALDriver *pDriverTiff, *pDriverPng;
    pDriverTiff = GetGDALDriverManager()->GetDriverByName("GTiff");
    pDriverPng = GetGDALDriverManager()->GetDriverByName("PNG");
    QString qsTempTiffPath = qsTempDir + "/tempdem.tif";

    int rows, cols;
    double ndv, stdev, min, max, mean, scaled, byte;
    double transform[6];
    pOldDS->GetGeoTransform(transform);
    rows = pOldDS->GetRasterBand(1)->GetYSize();
    cols = pOldDS->GetRasterBand(1)->GetXSize();
    ndv = pOldDS->GetRasterBand(1)->GetNoDataValue();

    pOldDS->GetRasterBand(1)->GetStatistics(FALSE,TRUE,&min,&max,&mean,&stdev);

    pSymbolDS = pDriverTiff->Create(qsTempTiffPath.toStdString().c_str(),cols,rows,1,GDT_Byte,NULL);
    pSymbolDS->GetRasterBand(1)->SetNoDataValue(ndv);

    float *oldRow = (float*) CPLMalloc(sizeof(float)*cols);
    unsigned char *newRow = (unsigned char*) CPLMalloc(sizeof(int)*cols);

    for (int i=0; i<rows; i++)
    {
        pOldDS->GetRasterBand(1)->RasterIO(GF_Read,0,i,cols,1,oldRow,cols,1,GDT_Float32,0,0);
        for (int j=0; j<cols; j++)
        {
            if (oldRow[j] == ndv)
            {
                newRow[j] = 0;
            }
            else
            {
                scaled = fabs(1 - (max-oldRow[j])/(max-min));
                byte = round(scaled*254)+1;
                newRow[j] = byte;
            }
        }
        pSymbolDS->GetRasterBand(1)->RasterIO(GF_Write,0,i,cols,1,newRow,cols,1,GDT_Byte,0,0);
    }
    pSymbolDS->GetRasterBand(1)->SetColorInterpretation(GCI_PaletteIndex);
    GDALColorTable table(GPI_RGB);
    GDALColorEntry black, white, trans;
    black.c1 = 1, black.c2 = 1, black.c3 = 1, black.c4 = 50;
    white.c1 = 255, white.c2 = 255, white.c3 = 255, white.c4 = 50;
    trans.c1 = 255, trans.c2 = 255, trans.c3 = 255, trans.c4 = 0;
    table.CreateColorRamp(1,&black,255,&white);
    table.SetColorEntry(0,&trans);
    pSymbolDS->GetRasterBand(1)->SetColorTable(&table);
    qDebug()<<"copy to png";
    pFinalDS = pDriverPng->CreateCopy(qsPngPath.toStdString().c_str(),pSymbolDS,FALSE,NULL,NULL,NULL);
    pFinalDS->SetGeoTransform(transform);
    qDebug()<<"close datasets";
    GDALClose(pFinalDS);
    GDALClose(pSymbolDS);
    QFile::remove(qsTempTiffPath);
    qDebug()<<"freeing memory";
    CPLFree(oldRow);
    CPLFree(newRow);
    oldRow = NULL;
    newRow = NULL;
}

void Symbology::symbolizeDoDasPNG(GDALDataset *pOldDS, QString qsPngPath, QString qsTempDir)
{
    GDALAllRegister();
    GDALDataset *pSymbolDS, *pFinalDS;
    GDALDriver *pDriverTiff, *pDriverPng;
    pDriverTiff = GetGDALDriverManager()->GetDriverByName("GTiff");
    pDriverPng = GetGDALDriverManager()->GetDriverByName("PNG");
    QString qsTempTiffPath = qsTempDir + "/tempdod.tif";

    int rows, cols;
    double ndv;
    double transform[6];
    pOldDS->GetGeoTransform(transform);
    rows = pOldDS->GetRasterBand(1)->GetYSize();
    cols = pOldDS->GetRasterBand(1)->GetXSize();
    ndv = pOldDS->GetRasterBand(1)->GetNoDataValue();

    pSymbolDS = pDriverTiff->Create(qsTempTiffPath.toStdString().c_str(),cols,rows,1,GDT_Byte,NULL);

    float *oldRow = (float*) CPLMalloc(sizeof(float)*cols);
    unsigned char *newRow = (unsigned char*) CPLMalloc(sizeof(int)*cols);

    for (int i=0; i<rows; i++)
    {
        pOldDS->GetRasterBand(1)->RasterIO(GF_Read,0,i,cols,1,oldRow,cols,1,GDT_Float32,0,0);

        for (int j=0; j<cols; j++)
        {
            if (oldRow[j] == ndv)
            {
                newRow[j] = 0;
            }
            else if (oldRow[j] <= (-3.0) && oldRow[j] > (-1000.0))
            {
                newRow[j] = 1;
            }
            else if (oldRow[j] <= (-2.5) && oldRow[j] > (-3.0))
            {
                newRow[j] = 2;
            }
            else if (oldRow[j] <= (-2.0) && oldRow[j] > (-2.5))
            {
                newRow[j] = 3;
            }
            else if (oldRow[j] <= (-1.5) && oldRow[j] > (-2.0))
            {
                newRow[j] = 4;
            }
            else if (oldRow[j] <= (-1.0) && oldRow[j] > (-1.5))
            {
                newRow[j] = 5;
            }
            else if (oldRow[j] <= (-0.75) && oldRow[j] > (-1.0))
            {
                newRow[j] = 6;
            }
            else if (oldRow[j] <= (-0.5) && oldRow[j] > (-0.75))
            {
                newRow[j] = 7;
            }
            else if (oldRow[j] <= (-0.25) && oldRow[j] > (-0.75))
            {
                newRow[j] = 8;
            }
            else if (oldRow[j] <= (-0.15) && oldRow[j] > (-0.25))
            {
                newRow[j] = 9;
            }
            else if (oldRow[j] <= (-0.1) && oldRow[j] > (-0.15))
            {
                newRow[j] = 10;
            }
            else if (oldRow[j] < (0.1) && oldRow[j] > (-0.1))
            {
                newRow[j] = 0;
            }
            else if (oldRow[j] >= 0.01 && oldRow[j] < 0.15)
            {
                newRow[j] = 11;
            }
            else if (oldRow[j] >= 0.15 && oldRow[j] < 0.25)
            {
                newRow[j] = 12;
            }
            else if (oldRow[j] >= 0.25 && oldRow[j] < 0.5)
            {
                newRow[j] = 13;
            }
            else if (oldRow[j] >= 0.5 && oldRow[j] < 0.75)
            {
                newRow[j] = 14;
            }
            else if (oldRow[j] >= 0.75 && oldRow[j] < 1.0)
            {
                newRow[j] = 15;
            }
            else if (oldRow[j] >= 1.0 && oldRow[j] < 1.5)
            {
                newRow[j] = 16;
            }
            else if (oldRow[j] >= 1.5 && oldRow[j] < 2.0)
            {
                newRow[j] = 17;
            }
            else if (oldRow[j] >= 2.0 && oldRow[j] < 2.5)
            {
                newRow[j] = 18;
            }
            else if (oldRow[j] >= 2.5 && oldRow[j] < 3.0)
            {
                newRow[j] = 19;
            }
            else if (oldRow[j] >= 3.0 && oldRow[j] < 1000.0)
            {
                newRow[j] = 20;
            }
            else
            {
                qDebug()<<"problem: dod value "<<oldRow[j];
            }
        }
        pSymbolDS->GetRasterBand(1)->RasterIO(GF_Write,0,i,cols,1,newRow,cols,1,GDT_Byte,0,0);
    }

    pSymbolDS->GetRasterBand(1)->SetColorInterpretation(GCI_PaletteIndex);
    GDALColorTable table;

    for (int i=0; i<21; i++)
    {
        if (i==0)
        {
            GDALColorEntry entry;
            entry.c1 = 255;
            entry.c2 = 255;
            entry.c3 = 255;
            entry.c4 = 0;
            table.SetColorEntry(i,&entry);
        }
        else if(i==1)
        {
            GDALColorEntry entry;
            entry.c1 = 230;
            entry.c2 = 0;
            entry.c3 = 0;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==2)
        {

            GDALColorEntry entry;entry.c1 = 255;
            entry.c1 = 230;
            entry.c2 = 23;
            entry.c3 = 23;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==3)
        {
            GDALColorEntry entry;
            entry.c1 = 232;
            entry.c2 = 46;
            entry.c3 = 46;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==4)
        {
            GDALColorEntry entry;
            entry.c1 = 235;
            entry.c2 = 70;
            entry.c3 = 70;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==5)
        {
            GDALColorEntry entry;
            entry.c1 = 237;
            entry.c2 = 95;
            entry.c3 = 95;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==6)
        {
            GDALColorEntry entry;
            entry.c1 = 240;
            entry.c2 = 120;
            entry.c3 = 120;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==7)
        {
            GDALColorEntry entry;
            entry.c1 = 242;
            entry.c2 = 145;
            entry.c3 = 145;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==8)
        {
            GDALColorEntry entry;
            entry.c1 = 245;
            entry.c2 = 171;
            entry.c3 = 171;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==9)
        {
            GDALColorEntry entry;
            entry.c1 = 247;
            entry.c2 = 198;
            entry.c3 = 198;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==10)
        {
            GDALColorEntry entry;
            entry.c1 = 250;
            entry.c2 = 225;
            entry.c3 = 225;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==11)
        {
            GDALColorEntry entry;
            entry.c1 = 211;
            entry.c2 = 221;
            entry.c3 = 232;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==12)
        {
            GDALColorEntry entry;
            entry.c1 = 182;
            entry.c2 = 201;
            entry.c3 = 224;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==13)
        {
            GDALColorEntry entry;
            entry.c1 = 154;
            entry.c2 = 182;
            entry.c3 = 217;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==14)
        {
            GDALColorEntry entry;
            entry.c1 = 128;
            entry.c2 = 164;
            entry.c3 = 209;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==15)
        {
            GDALColorEntry entry;
            entry.c1 = 104;
            entry.c2 = 149;
            entry.c3 = 204;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==16)
        {
            GDALColorEntry entry;
            entry.c1 = 81;
            entry.c2 = 133;
            entry.c3 = 196;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==17)
        {
            GDALColorEntry entry;
            entry.c1 = 58;
            entry.c2 = 117;
            entry.c3 = 189;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==18)
        {
            GDALColorEntry entry;
            entry.c1 = 38;
            entry.c2 = 102;
            entry.c3 = 181;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==19)
        {
            GDALColorEntry entry;
            entry.c1 = 19;
            entry.c2 = 89;
            entry.c3 = 173;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }
        else if(i==20)
        {
            GDALColorEntry entry;
            entry.c1 = 0;
            entry.c2 = 76;
            entry.c3 = 168;
            entry.c4 = 255;
            table.SetColorEntry(i,&entry);
        }

    }
    qDebug()<<"set color table";
    pSymbolDS->GetRasterBand(1)->SetColorTable(&table);
    qDebug()<<"copy to png";
    pFinalDS = pDriverPng->CreateCopy(qsPngPath.toStdString().c_str(),pSymbolDS,FALSE,NULL,NULL,NULL);
    pFinalDS->SetGeoTransform(transform);
    qDebug()<<"close datasets";
    GDALClose(pFinalDS);
    GDALClose(pSymbolDS);
    QFile::remove(qsTempTiffPath);
    qDebug()<<"freeing memory";
    CPLFree(oldRow);
    CPLFree(newRow);
    oldRow = NULL;
    newRow = NULL;
}

void Symbology::symbolizeWaterDepthasPNG(GDALDataset *pOldDS, QString qsPngPath, QString qsTempDir)
{
    GDALAllRegister();
    GDALDataset *pSymbolDS, *pFinalDS;
    GDALDriver *pDriverTiff, *pDriverPng;
    pDriverTiff = GetGDALDriverManager()->GetDriverByName("GTiff");
    pDriverPng = GetGDALDriverManager()->GetDriverByName("PNG");
    QString qsTempTiffPath = qsTempDir + "/tempdem.tif";

    int rows, cols;
    double ndv, stdev, min, max, mean, scaled, byte;
    double transform[6];
    pOldDS->GetGeoTransform(transform);
    rows = pOldDS->GetRasterBand(1)->GetYSize();
    cols = pOldDS->GetRasterBand(1)->GetXSize();
    ndv = pOldDS->GetRasterBand(1)->GetNoDataValue();

    pOldDS->GetRasterBand(1)->GetStatistics(FALSE,TRUE,&min,&max,&mean,&stdev);

    pSymbolDS = pDriverTiff->Create(qsTempTiffPath.toStdString().c_str(),cols,rows,1,GDT_Byte,NULL);
    pSymbolDS->GetRasterBand(1)->SetNoDataValue(ndv);

    float *oldRow = (float*) CPLMalloc(sizeof(float)*cols);
    unsigned char *newRow = (unsigned char*) CPLMalloc(sizeof(int)*cols);

    for (int i=0; i<rows; i++)
    {
        pOldDS->GetRasterBand(1)->RasterIO(GF_Read,0,i,cols,1,oldRow,cols,1,GDT_Float32,0,0);
        for (int j=0; j<cols; j++)
        {
            if (oldRow[j] == ndv || oldRow[j] <= 0.0)
            {
                newRow[j] = 0;
                //alpRow[j] = 0;
            }
            else
            {
                scaled = 1 - (max-oldRow[j])/max;
                byte = round(scaled*254)+1;
                newRow[j] = byte;
                //alpRow[j] = 50;
            }
        }
        pSymbolDS->GetRasterBand(1)->RasterIO(GF_Write,0,i,cols,1,newRow,cols,1,GDT_Byte,0,0);
    }
    pSymbolDS->GetRasterBand(1)->SetColorInterpretation(GCI_PaletteIndex);
    GDALColorTable table(GPI_RGB);
    GDALColorEntry blue, white, trans;
    blue.c1 = 10, blue.c2 = 10, blue.c3 = 240, blue.c4 = 50;
    white.c1 = 182, white.c2 = 237, white.c3 = 240, white.c4 = 50;
    trans.c1 = 255, trans.c2 = 255, trans.c3 = 255, trans.c4 = 0;
    table.CreateColorRamp(1,&white,255,&blue);
    table.SetColorEntry(0,&trans);
    pSymbolDS->GetRasterBand(1)->SetColorTable(&table);
    qDebug()<<"copy to png";
    pFinalDS = pDriverPng->CreateCopy(qsPngPath.toStdString().c_str(),pSymbolDS,FALSE,NULL,NULL,NULL);
    pFinalDS->SetGeoTransform(transform);
    qDebug()<<"close datasets";
    GDALClose(pFinalDS);
    GDALClose(pSymbolDS);
    QFile::remove(qsTempTiffPath);
    qDebug()<<"freeing memory";
    CPLFree(oldRow);
    CPLFree(newRow);
    oldRow = NULL;
    newRow = NULL;
}
