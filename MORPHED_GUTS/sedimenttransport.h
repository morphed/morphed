#ifndef SEDIMENTTRANSPORT_H
#define SEDIMENTTRANSPORT_H

#include <QMutex>
#include "vegetation.h"
#include "surfaces.h"
#include "hydraulics.h"
#include "grainsize.h"
#include "pathlengthdistribution.h"
//#include "depositionthread.h"

namespace MORPH{

class SedimentTransport : public Surfaces
{
private:
    GDALRasterBand *pFlowDir, *pDepth, *pShear, *pFlowDir5x5;
    Hydraulics *HydroData;
    PathLengthDistribution *PlBed, *PlImport;
    Vegetation *Veg;
    float *depth, *fdirValue;
    QVector<double> propPl, propCell, volErode, volDep, gsVec;
    QVector<int> rowCand, colCand, morphCode, rowIn, colIn, plIn;
    QString qsBedE, qsBedD, qsBank;
    GDALDataset *pBedE, *pBedD, *pBank;
    QMutex mutex;

public:
    GDALDataset *pBankID, *pCumRetreat, *pCohesion;
    GDALDataset *pFlowDirDS, *pDepthDS, *pShearDS, *pDepoTemp;
    GrainSize *GS;
    QString qsTempDepo, qsBankID, qsCumRetreat, qsSlopeBankMax, qsCohesion;
    QVector<double> braidData;
    double exported, exportedTotal, bedErode, bedErodeTotal, bedDepo, bedDepoTotal, bankErode, bankErodeTotal, bankDepo, bankDepoTotal, inGS, imported, importedTotal, importDepo;

    static const double POROSITY = 0.7, RHO_S = 2650.0, RHO = 1000.0, A = 9.0, G = 9.81;
    static const double MAXFAIL_SLOPE1 = 32.0, MAXFAIL_SLOPE2 = 32.0, MAXFAIL_SLOPE3 = 32.0, VEG_RESET = 0.10, MAXFAIL_MIN = 0.01;

    SedimentTransport(QString xmlPath);
    SedimentTransport(XMLReadWrite &XmlObj);
    ~SedimentTransport();

    void bankSloughing();
    void calculateCumulativeRetreat();
    void clearVectors();
    void closeSedDatasets();
    void createInitialCohesion();
    void depositToCandidates();
    void depositToCandidatesImport();
    void depositToDEM(double &updateDepo);
    double erodeBank();
    double erodeBed(double shearCrit, double shear, int row, int col);
    double erodeBed5Cells(double shearCrit, double shear, int row, int col);
    double erodeBed3x3(double shearCrit, double shear);
    double findBankHeight(int row, int col);
    void findImportCells();
    double findMaxShear3x3(int row, int col);
    QVector<int> findNextCell(int startRow, int startCol, int prevRow, int prevCol);
    QVector<int> findNextCellAveFlow(int startRow, int startCol, int prevRow, int prevCol);
    QVector<double> findNextCellCoords(double startX, double startY, int prevRow, int prevCol);
    QVector<double> findNextCellCoordsBackward(double startX, double startY, int prevRow, int prevCol);
    void idBanks();
    void idCandidateCells(int row, int col, double sedAmt, double grnSz);
    void idCandidateCellsImport(int row, int col, double sedAmt, double grnSz);
    void importSediment();
    void runBankTransport();
    void runBedTransport();
    void runBedTransportSingleThread_3Paths(int row, int col, double sedAvailable, double gs);
    void runBedTransportSingleThread_5Paths(int row, int col, double sedAvailable, double gs);
    void runBedTransportSingleThread_10Paths(int row, int col, double sedAvailable, double gs);
    void setCandidateData3x3(int row, int col, double sedAmt, double grnSz, int pl);
    void setCandidateData3x3(int row, int col, double sedAmt, double grnSz, int pl, PathLengthDistribution *PlBed);
    void setCandidateData3x3Import(int row, int col, int pl);
    void setCandidateData3x3Uniform(int row, int col, double sedAmt, double grnSz, int pl);
    void setCandidateData5x5(int row, int col, double sedAmt, double grnSz, int pl);
    void setCandidateData5x5(int row, int col, double sedAmt, double grnSz, int pl, PathLengthDistribution *PlBed);
    void setCandidateData5x5Import(int row, int col, int pl);
    void setCandidateData5x5Uniform(int row, int col, double sedAmt, double grnSz, int pl);
    void setCandidateData7x7Uniform(int row, int col, double sedAmt, double grnSz, int pl);
    void setupHydraulics();
    void updateIteration();
    void writeTempDoDs(QString qsDodPath, GDALDataset *pOld, GDALDataset *pNew);
};

}

#endif // SEDIMENTTRANSPORT_H
