#ifndef MORPHED_GUTS_H
#define MORPHED_GUTS_H

#include "morphed_guts_global.h"

class MORPHED_GUTSSHARED_EXPORT MORPHED_GUTS
{

public:
    MORPHED_GUTS();
    static int test();
};

#endif // MORPHED_GUTS_H
