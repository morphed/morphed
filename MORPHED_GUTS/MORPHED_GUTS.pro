#-------------------------------------------------
#
# Project created by QtCreator 2014-07-21T13:27:20
#
#-------------------------------------------------

QT       -= gui
QT       += xml

TARGET = MORPHED_GUTS
TEMPLATE = lib

DEFINES += MORPHED_GUTS_LIBRARY

SOURCES += morphed_guts.cpp \
    delft3dio.cpp \
    filemanager.cpp \
    hydraulics.cpp \
    morphedbaseclass.cpp \
    surfaces.cpp \
    xmlreadwrite.cpp \
    pathlengthdistribution.cpp \
    morphedmodel.cpp \
    sedimenttransport.cpp \
    grainsize.cpp \
    vegetation.cpp \
    symbology.cpp \
    depositionthread.cpp

HEADERS += morphed_guts.h\
        morphed_guts_global.h \
    delft3dio.h \
    filemanager.h \
    hydraulics.h \
    morphedbaseclass.h \
    surfaces.h \
    xmlreadwrite.h \
    pathlengthdistribution.h \
    morphedmodel.h \
    sedimenttransport.h \
    grainsize.h \
    vegetation.h \
    symbology.h \
    depositionthread.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

#unix|win32: LIBS += -L$$PWD/../../../../../MinGW/msys/1.0/local/lib/ -llibgdal

#INCLUDEPATH += $$PWD/../../../../../MinGW/msys/1.0/local/include
#DEPENDPATH += $$PWD/../../../../../MinGW/msys/1.0/local/include



unix|win32: LIBS += -L$$PWD/../../../../../MinGW/msys/1.0/local/lib/ -llibgdal

INCLUDEPATH += $$PWD/../../../../../MinGW/msys/1.0/local/include
DEPENDPATH += $$PWD/../../../../../MinGW/msys/1.0/local/include
