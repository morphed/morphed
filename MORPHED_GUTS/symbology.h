#ifndef SYMBOLOGY_H
#define SYMBOLOGY_H

#include "gdal_priv.h"
#include <QtCore>

class Symbology
{
public:
    static const double PI = 3.14159265;

    Symbology();

    static void createHillshadeAsPNG(GDALDataset *pOldDS, QString qsPngPath, QString qsTempDir);
    static void symbolizeDEMasPNG(GDALDataset *pOldDS, QString qsPngPath, QString qsTempDir);
    static void symbolizeDoDasPNG(GDALDataset *pOldDS, QString qsPngPath, QString qsTempDir);
    static void symbolizeWaterDepthasPNG(GDALDataset *pOldDS, QString qsPngPath, QString qsTempDir);
};

#endif // SYMBOLOGY_H
