#ifndef VEGETATION_H
#define VEGETATION_H

#include "morphedbaseclass.h"

namespace MORPH{


class Vegetation : public MORPHEDBaseClass
{
private:
    QString qsCohesion;
    GDALDataset *pCohesion;

protected:

public:
    Vegetation(XMLReadWrite &XmlObj);
    ~Vegetation();

    void closeVegDatasets();
    void createInitialCohesion(GDALDataset* pDetrendDS);
    GDALDataset* getCohesionDS();
    void makeBare(int row, int col);
    void openVegDatasets();
    void updateGrowth();
};

}

#endif // VEGETATION_H
