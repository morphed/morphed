#include "pathlengthdistribution.h"

namespace MORPH{

PathLengthDistribution::PathLengthDistribution(QString xmlPath):MORPHEDBaseClass(xmlPath)
{
}

PathLengthDistribution::PathLengthDistribution(XMLReadWrite &XmlObj):MORPHEDBaseClass(XmlObj)
{
    dLength = XmlInit.readNodeData("MorphedParameters", "DistributionLength").toDouble();
    sigA = XmlInit.readNodeData("MorphedParameters", "SigA").toDouble();
    muB = XmlInit.readNodeData("MorphedParameters", "MuB").toDouble();
    nType = XmlInit.readNodeData("MorphedParameters", "DistributionType").toInt();
    nLength = rint(dLength/cellWidth);

    bool goodDist = true;
    double factor;
    double sum = 0;

    //exponential
    if (nType == 1)
    {
        for (int i=0; i<nLength; i++)
        {
            pathLength.append((1.0/sqrt(2.0*Hydraulics::PI))*exp(((-1.0)*sigA)*muB*(i+1.0)));
            sum += pathLength[i];
        }
    }
    //gaussian
    else if (nType == 2)
    {
        for (int i=0; i<nLength; i++)
        {
            pathLength.append((1/(sigA*sqrt(2*Hydraulics::PI)))*exp((-0.5)*pow((((i+1)-muB)/sigA),2)));
            sum += pathLength[i];
        }
    }
    //custom
    else if (nType == 3)
    {

    }
    else
    {
        goodDist = false;
    }

    if(goodDist)
    {
        factor = 1/sum;
        for (int i=0; i<nLength; i++)
        {
            pathLength[i] *= factor;
        }
    }
}

void PathLengthDistribution::setExponential(int length, double a, double b)
{
    nLength = length;
    sigA = a;
    muB = b;

    double sum = 0;
    double factor;

    for (int i=0; i<nLength; i++)
    {
        pathLength.append((1.0/sqrt(2.0*Hydraulics::PI))*exp(((-1.0)*sigA)*muB*(i+1.0)));
        sum += pathLength[i];
    }

    factor = 1/sum;
    for (int i=0; i<nLength; i++)
    {
        pathLength[i] *= factor;
    }
}

void PathLengthDistribution::setGaussian(int length, double sigma, double mu)
{
    nLength = length;
    sigA = sigma;
    muB = mu;

    double sum = 0;
    double factor;

    for (int i=0; i<nLength; i++)
    {
        pathLength.append((1/(sigA*sqrt(2*Hydraulics::PI)))*exp((-0.5)*pow((((i+1)-muB)/sigA),2)));
        sum += pathLength[i];
    }

    factor = 1/sum;
    for (int i=0; i<nLength; i++)
    {
        pathLength[i] *= factor;
    }
}

int PathLengthDistribution::getLength(double distance, double cellSize)
{
    int nLength;
    nLength = rint(distance/cellSize);
    return nLength;
}

QVector<double> PathLengthDistribution::getDistribution(int type, double sigA, double muB, int length)
{
    QVector<double> pathLength;
    bool goodDist = true;
    double factor;
    double sum = 0;

    //exponential
    if (type == 1)
    {
        for (int i=0; i<length; i++)
        {
            pathLength.append((1/sqrt(2*Hydraulics::PI))*exp(((-1)*sigA))*muB*(i+1));
            sum += pathLength[i];
        }
    }
    //gaussian
    else if (type == 2)
    {
        for (int i=0; i<length; i++)
        {
            pathLength.append((1/(sigA*sqrt(2*Hydraulics::PI)))*exp((-0.5)*pow((((i+1)-muB)/sigA),2)));
            sum += pathLength[i];
        }
    }
    //custom
    else if (type == 3)
    {

    }
    else
    {
        goodDist = false;
    }

    if(goodDist)
    {
        factor = 1/sum;
        for (int i=0; i<length; i++)
        {
            pathLength[i] *= factor;
        }
    }
    return pathLength;
}

}
