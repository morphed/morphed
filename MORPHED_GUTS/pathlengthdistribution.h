#ifndef PATHLENGTHDISTRIBUTION_H
#define PATHLENGTHDISTRIBUTION_H

#include "morphedbaseclass.h"
#include "hydraulics.h"

namespace MORPH{

class PathLengthDistribution : public MORPHEDBaseClass
{
    friend class SedimentTransport;
private:
protected:
    int nLength, nType;
    double dLength, sigA, muB;
    QVector<double> pathLength;

public:
    PathLengthDistribution(QString xmlPath);
    PathLengthDistribution(XMLReadWrite &XmlObj);

    void setExponential(int length, double a, double b);
    void setGaussian(int length, double sigma, double mu);

    static int getLength(double distance, double cellSize);
    static QVector<double> getDistribution(int type, double sigA, double muB, int length);
};

}

#endif // PATHLENGTHDISTRIBUTION_H
