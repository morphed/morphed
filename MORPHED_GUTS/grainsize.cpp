#include "grainsize.h"

namespace MORPH{

GrainSize::GrainSize(XMLReadWrite &XmlObj):Surfaces(XmlObj)
{
    char **papszOptions = NULL;

    loadDrivers();
    pMainVRT = NULL;
    type = XmlInit.readNodeData("Inputs", "GrainSize", "Type").toInt();
    activeType = XmlInit.readNodeData("Inputs", "GrainSize", "ActiveType").toInt();
    activeSize = XmlInit.readNodeData("Inputs", "GrainSize", "ActiveSize").toDouble();
    qsActPath = XmlInit.readNodeData("Inputs", "GrainSize", "ActiveRasterPath");
    subSurf = XmlInit.readNodeData("Inputs", "GrainSize", "SubSurfLayer").toInt();
    subType = XmlInit.readNodeData("Inputs", "GrainSize", "SubSurfType").toInt();
    subSize = XmlInit.readNodeData("Inputs", "GrainSize", "SubSurfSize").toDouble();
    thickness = XmlInit.readNodeData("Inputs", "GrainSize", "LayerThickness").toDouble();
    qsSubPath = XmlInit.readNodeData("Inputs", "GrainSize", "SubSurfRasterPath");
    qsDepoPath = qsTempDir + "/gs_depo.tif";
    qsActTempPath = qsTempDir + "/gs_act.tif";
    qsMainPath = qsTempDir + "/gs_main.tif";
    qsMainTemp = qsTempDir + "/gs_main_tmp.tif";
    qsMainPathVRT = qsTempDir + "/gs_mainvrt.vrt";

    demMax = findMax(pInitialSurface);
    demMin = findMin(pInitialSurface);

    if (activeType == 3)
    {
        qDebug()<<qsActPath;
        pActiveOrig = (GDALDataset*) GDALOpen(qsActPath.toStdString().c_str(), GA_ReadOnly);
    }
    else
    {
        pActiveOrig = NULL;
    }
    if (subType == 3)
    {
        pSubOrig = (GDALDataset*) GDALOpen(qsSubPath.toStdString().c_str(), GA_ReadOnly);
    }
    else
    {
        qsSubPath = qsTempDir + "/gs_sub.tif";
        float *dataRowNew = (float*) CPLMalloc(sizeof(float)*nRows);
        float *dataRowOld = (float*) CPLMalloc(sizeof(float)*nRows);
        pSubOrig = pDriverTIFF->Create(qsSubPath.toStdString().c_str(),nCols, nRows, 1,GDT_Float32,papszOptions);
        pSubOrig->GetRasterBand(1)->SetNoDataValue(fNoDataValue);
        for (int i=0; i<nRows; i++)
        {
            pOldSurface->GetRasterBand(1)->RasterIO(GF_Read,0,i,nCols,1,dataRowOld,nRows,1,GDT_Float32,0,0);
            for (int j=0; j<nCols; j++)
            {
                if (dataRowOld[j] != fNoDataValue)
                {
                    dataRowNew[j] = subSize;
                }
                else
                {
                    dataRowNew[j] = fNoDataValue;
                }
            }
            pSubOrig->GetRasterBand(1)->RasterIO(GF_Write,0,i,nCols,1,dataRowNew,nRows,1,GDT_Float32,0,0);
        }
        CPLFree(dataRowNew);
        dataRowNew = NULL;
        CPLFree(dataRowOld);
        dataRowOld = NULL;
    }


    //set up grain size deposition raster
    pDeposition = pDriverTIFF->Create(qsDepoPath.toStdString().c_str(),nCols,nRows,2,GDT_Float32,papszOptions);
    pDeposition->SetGeoTransform(transform);
    pDeposition->GetRasterBand(1)->Fill(fNoDataValue);
    pDeposition->GetRasterBand(2)->Fill(fNoDataValue);
    pDeposition->GetRasterBand(1)->SetNoDataValue(fNoDataValue);
    pDeposition->GetRasterBand(2)->SetNoDataValue(fNoDataValue);

    volume = (float*) CPLMalloc(sizeof(float)*1);
    grainsize = (float*) CPLMalloc(sizeof(float)*1);
    elev = (float*) CPLMalloc(sizeof(float)*1);
    actVal = (float*) CPLMalloc(sizeof(float)*1);
    subVal = (float*) CPLMalloc(sizeof(float)*1);
    actBand = (float*) CPLMalloc(sizeof(float)*1);
    depth = (float*) CPLMalloc(sizeof(float)*1);

    //add 3 layers below min DEM elevation and above max DEM elevation
    minElev = demMin - (thickness*3);
    maxElev = demMax + (thickness*3);

    //determine number of bands
    bandCount = ceil((maxElev-minElev)/thickness);
    qDebug()<<"dem min max "<<demMin<<" "<<demMax<<" band count "<<bandCount;
    maxElev = minElev + (bandCount*thickness);

    //create raster with necessary band number to hold all layer data, and active layer raster to hold data for active layer
    pMain = pDriverTIFF->Create(qsMainPath.toStdString().c_str(), nCols, nRows, bandCount, GDT_Float32, papszOptions);
    pActive = pDriverTIFF->Create(qsActTempPath.toStdString().c_str(), nCols, nRows, 2, GDT_Float32, papszOptions);

    setupMain();
    qDebug()<<"min "<<minElev<<" max "<<maxElev<<" bands "<<bandCount<<" thickness "<<thickness<<" max vector "<<findMaxVector(bandMinElev,bandMinElev.size());
    qDebug()<<"main setup done";

    pMainTemp = pDriverTIFF->CreateCopy(qsMainTemp.toStdString().c_str(),pMain,FALSE,NULL,NULL,NULL);
}

GrainSize::~GrainSize()
{
    if (pDeposition != NULL)
    {
        GDALClose(pDeposition);
    }
    if (pActiveOrig != NULL)
    {
        GDALClose(pActiveOrig);
    }
    if (pSubOrig != NULL)
    {
        GDALClose(pSubOrig);
    }
    if (pMain != NULL)
    {
        GDALClose(pMain);
    }
    if (pMainTemp != NULL)
    {
        GDALClose(pMainTemp);
    }
    if (pMainVRT != NULL)
    {
        GDALClose(pMainVRT);
    }
    if (pActive != NULL)
    {
        GDALClose(pActive);
    }

    CPLFree(volume);
    volume = NULL;
    CPLFree(grainsize);
    grainsize = NULL;
    CPLFree(depth);
    depth = NULL;
    CPLFree(actVal);
    actVal = NULL;
    CPLFree(elev);
    elev = NULL;
    CPLFree(subVal);
    subVal = NULL;
    CPLFree(actBand);
    actBand = NULL;
}

void GrainSize::load()
{
    loadDrivers();
    type = XmlInit.readNodeData("Inputs", "GrainSize", "Type").toInt();
    activeType = XmlInit.readNodeData("Inputs", "GrainSize", "ActiveType").toInt();
    activeSize = XmlInit.readNodeData("Inputs", "GrainSize", "SubSurfSize").toDouble();
    qsActPath = XmlInit.readNodeData("Inputs", "GrainSize", "ActiveRasterPath");
    subSurf = XmlInit.readNodeData("Inputs", "GrainSize", "SubSurfLayer").toInt();
    subType = XmlInit.readNodeData("Inputs", "GrainSize", "SubSurfType").toInt();
    subSize = XmlInit.readNodeData("Inputs", "GrainSize", "SubSurfSize").toDouble();
    thickness = XmlInit.readNodeData("Inputs", "GrainSize", "LayerThickness").toDouble();
    qsSubPath = XmlInit.readNodeData("Inputs", "GrainSize", "SubSurfRasterPath").toInt();
    qsDepoPath = qsTempDir + "/gs_depo.tif";
    qsActTempPath = qsTempDir + "/gs_act.tif";
    qsMainPath = qsTempDir + "gs_main.tif";
    qsMainPathVRT = qsTempDir + "gs_main.vrt";

    demMax = findMax(pInitialSurface);
    demMin = findMin(pInitialSurface);

    qDebug()<<"ActiveType "<<activeType;
    if (activeType == 3)
    {
        qDebug()<<qsActPath;
        pActiveOrig = (GDALDataset*) GDALOpen(qsActPath.toStdString().c_str(), GA_ReadOnly);
        qDebug()<<"Active grainsize loaded";
    }
    if (subType == 3)
    {
        pSubOrig = (GDALDataset*) GDALOpen(qsSubPath.toStdString().c_str(), GA_ReadOnly);
    }

    //setupMain();
}

void GrainSize::addBandBottom()
{
    qDebug()<<"adding bottom band";

    double newBandMin;
    int newBand;

    if (pMain != NULL)
    {
        GDALClose(pMain);
        QFile::remove(qsMainPath);
    }

    pMainVRT = pDriverVRT->CreateCopy(qsMainPathVRT.toStdString().c_str(), pMainTemp, FALSE, NULL, NULL, NULL);

    minElev -= thickness;
    newBandMin = minElev;
    bandCount++;
    newBand = bandCount;
    bandNumber.append(bandCount);
    bandMinElev.append(newBandMin);

    pMainVRT->AddBand(GDT_Float32, NULL);
    pMain = pDriverTIFF->CreateCopy(qsMainPath.toStdString().c_str(), pMainVRT, FALSE, NULL, NULL, NULL);

    float *subRow = (float*) CPLMalloc(sizeof(float)*nCols);

    for (int i=0; i<nRows; i++)
    {
        pSubOrig->GetRasterBand(1)->RasterIO(GF_Read,0,i,nCols,1,subRow,nCols,1,GDT_Float32,0,0);
        pMain->GetRasterBand(newBand)->RasterIO(GF_Write,0,i,nCols,1,subRow,nCols,1,GDT_Float32,0,0);
    }

    pMain->GetRasterBand(newBand)->SetNoDataValue(fNoDataValue);
    if (pMainVRT != NULL)
    {
        GDALClose(pMainVRT);
        QFile::remove(qsMainPathVRT);
    }
    if (pMainTemp != NULL)
    {
        GDALClose(pMainTemp);
        QFile::remove(qsMainTemp);
    }
    pMainTemp = pDriverTIFF->CreateCopy(qsMainTemp.toStdString().c_str(), pMain, FALSE, NULL, NULL, NULL);
    qDebug()<<"add success";
}

void GrainSize::addBandTop()
{
    //qDebug()<<"adding top band";

    double newBandMin;
    int newBand;

    if (pMain != NULL)
    {
        //qDebug()<<"gstb closing main";
        GDALClose(pMain);
        //qDebug()<<"gstb main closed";
        QFile::remove(qsMainPath);
        //qDebug()<<"gstb main gs removed";
    }

    pMainVRT = pDriverVRT->CreateCopy(qsMainPathVRT.toStdString().c_str(),pMainTemp,FALSE,NULL,NULL,NULL);
    //qDebug()<<"gstb vrt created";

    newBandMin = maxElev;
    maxElev += thickness;
    bandCount++;
    newBand = bandCount;
    bandNumber.append(bandCount);
    bandMinElev.append(newBandMin);
    //qDebug()<<"gstb variables updated";

    pMainVRT->AddBand(GDT_Float32, NULL);
    pMain = pDriverTIFF->CreateCopy(qsMainPath.toStdString().c_str(),pMainVRT,FALSE,NULL,NULL,NULL);
    //qDebug()<<"gstb main gs recreated";

    pMain->GetRasterBand(newBand)->Fill(fNoDataValue);
    pMain->GetRasterBand(newBand)->SetNoDataValue(fNoDataValue);
    //qDebug()<<"gstb main gs new band filled";
    if (pMainVRT != NULL)
    {
        //qDebug()<<"closing main vrt";
        GDALClose(pMainVRT);
        //qDebug()<<"main vrt closed";
        QFile::remove(qsMainPathVRT);
        //qDebug()<<"gstb vrt removed";
    }
    if (pMainTemp != NULL)
    {
        //qDebug()<<"closing main temp";
        GDALClose(pMainTemp);
        //qDebug()<<"main temp closed";
        QFile::remove(qsMainTemp);
        //qDebug()<<"gstb main temp removed";
    }
    pMainTemp = pDriverTIFF->CreateCopy(qsMainTemp.toStdString().c_str(),pMain,FALSE,NULL,NULL,NULL);
    //qDebug()<<"add success";
}

void GrainSize::addDeposition(int row, int col, double gs, double vol)
{
    pDeposition->GetRasterBand(2)->RasterIO(GF_Read,col,row,1,1,volume,1,1,GDT_Float32,0,0);
    pDeposition->GetRasterBand(1)->RasterIO(GF_Read,col,row,1,1,grainsize,1,1,GDT_Float32,0,0);
    if (*volume <= 0)
    {
        *volume = vol;
        pDeposition->GetRasterBand(2)->RasterIO(GF_Write,col,row,1,1,volume,1,1,GDT_Float32,0,0);
        *grainsize = gs;
        pDeposition->GetRasterBand(1)->RasterIO(GF_Write,col,row,1,1,grainsize,1,1,GDT_Float32,0,0);
    }
    else
    {
        double totalVol = *volume + vol;
        *grainsize = (((*grainsize) * (*volume))+(gs * vol)) / totalVol;
        *volume = totalVol;
        pDeposition->GetRasterBand(2)->RasterIO(GF_Write,col,row,1,1,volume,1,1,GDT_Float32,0,0);
        pDeposition->GetRasterBand(1)->RasterIO(GF_Write,col,row,1,1,grainsize,1,1,GDT_Float32,0,0);
    }
}

void GrainSize::closeGsDatasets()
{
    if (pDeposition != NULL)
    {
        GDALClose(pDeposition);
        pDeposition = NULL;
        qDebug()<<"depo closed";
    }
    if (pActive != NULL)
    {
        GDALClose(pActive);
        pActive = NULL;
        qDebug()<<"active closed";
    };
    if (pActiveOrig != NULL)
    {
        GDALClose(pActiveOrig);
        pActiveOrig = NULL;
        qDebug()<<"active orig closed";
    }
    if (pSubOrig != NULL)
    {
        GDALClose(pSubOrig);
        pSubOrig = NULL;
        qDebug()<<"sub orig closed";
    }
    if (pMainVRT != NULL)
    {
        GDALClose(pMainVRT);
        pMainVRT = NULL;
        qDebug()<<"main vrt closed";
    }
    if (pMain != NULL)
    {
        GDALClose(pMain);
        pMain = NULL;
        qDebug()<<"main closed";
    }
    if (pMainTemp != NULL)
    {
        GDALClose(pMainTemp);
        pMainTemp = NULL;
        qDebug()<<"main temp closed";
    }
}

double GrainSize::erodeMultipleLayers(int row, int col, double erodeDepth)
{
    int layers, band;
    double grainSizeAverage, totalVolume, bandElev, endThick;

    layers = ceil(erodeDepth/thickness);

    for (int i=0; i<layers; i++)
    {
        if ( i == 0)
        {
            pActive->GetRasterBand(2)->RasterIO(GF_Read,col,row,1,1,volume,1,1,GDT_Float32,0,0);
            pActive->GetRasterBand(1)->RasterIO(GF_Read,col,row,1,1,actBand,1,1,GDT_Float32,0,0);
            pMain->GetRasterBand(*actBand)->RasterIO(GF_Read,col,row,1,1,grainsize,1,1,GDT_Float32,0,0);
            grainSizeAverage = *grainsize;
            totalVolume = *volume;
            bandElev = findElev(*actBand);
        }
        else if (i == (layers-1))
        {
            bandElev -= thickness;
            if (bandElev<minElev)
            {
                addBandBottom();
            }
            band = findBand(bandElev);
            endThick = erodeDepth - totalVolume;
            pMain->GetRasterBand(band)->RasterIO(GF_Read,col,row,1,1,grainsize,1,1,GDT_Float32,0,0);
            grainSizeAverage = ((grainSizeAverage*totalVolume) + (*grainsize*endThick))/(totalVolume+endThick);
            totalVolume += endThick;

            *actBand = band;
            *volume = thickness - endThick;
            //qDebug()<<"actband "<<*actBand;
            //qDebug()<<"thickness "<<*volume;
            pActive->GetRasterBand(2)->RasterIO(GF_Write,col,row,1,1,volume,1,1,GDT_Float32,0,0);
            pActive->GetRasterBand(1)->RasterIO(GF_Write,col,row,1,1,actBand,1,1,GDT_Float32,0,0);
        }
        else
        {
            bandElev -= thickness;
            if (bandElev<minElev)
            {
                addBandBottom();
            }
            band = findBand(bandElev);
            pMain->GetRasterBand(band)->RasterIO(GF_Read,col,row,1,1,grainsize,1,1,GDT_Float32,0,0);
            grainSizeAverage = ((grainSizeAverage*totalVolume) + (*grainsize*thickness))/(totalVolume+thickness);
            totalVolume += thickness;
        }
    }
    return grainSizeAverage;
}

int GrainSize::findBand(double elev)
{
    bool found = false;
    int band;

    if (found == false)
    {
        for (int i=0; i<bandNumber.size(); i++)
        {
            if (elev >= (bandMinElev[i]) && elev < (bandMinElev[i]+thickness))
            {
                found = true;
                band = bandNumber[i];
                break;
            }
            else if (elev >= findMaxVector(bandMinElev,bandMinElev.size()) && elev < maxElev)
            {
                band = bandMinElev.indexOf(findMaxVector(bandMinElev,bandMinElev.size()));
                found = true;
            }
        }
    }

    if (found == true)
    {
        return band;
    }
    else
    {
        return (-1);
    }
}

double GrainSize::findElev(int band)
{
    int index;
    index = bandNumber.indexOf(band);
    if (index != (-1))
    {
        return bandMinElev[index];
    }
    else
    {
        return (-1);
    }
}

void GrainSize::openGSDatasets()
{
    pDeposition = (GDALDataset*) GDALOpen(qsDepoPath.toStdString().c_str(),GA_ReadOnly);
    pActive = (GDALDataset*) GDALOpen(qsActTempPath.toStdString().c_str(),GA_ReadOnly);
    pActiveOrig = (GDALDataset*) GDALOpen(qsActPath.toStdString().c_str(),GA_ReadOnly);
    pSubOrig = (GDALDataset*) GDALOpen(qsSubPath.toStdString().c_str(),GA_ReadOnly);
    pMain = (GDALDataset*) GDALOpen(qsMainPath.toStdString().c_str(),GA_ReadOnly);
    pMainTemp = (GDALDataset*) GDALOpen(qsMainTemp.toStdString().c_str(),GA_ReadOnly);
}

void GrainSize::populateLayers()
{
    if (subSurf == 1)
    {
        if (activeType == 1)
        {
            *actVal = activeSize;
            if (subType == 1)
            {
                *subVal = subSize;
                for (int i=0; i<nRows; i++)
                {
                    for (int j=0; j<nCols; j++)
                    {
                        pInitialSurface->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,elev,1,1,GDT_Float32,0,0);
                        if (*elev != fNoDataValue)
                        {
                            setupCell(i, j);
                        }
                    }
                }
            }

            else if (subType == 2)
            {
                *subVal = *actVal * (subSize/100.0);
                for (int i=0; i<nRows; i++)
                {
                    for (int j=0; j<nCols; j++)
                    {
                        pInitialSurface->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,elev,1,1,GDT_Float32,0,0);
                        if (*elev != fNoDataValue)
                        {
                            setupCell(i, j);
                        }
                    }
                }
            }
            else if (subType == 3)
            {
                for (int i=0; i<nRows; i++)
                {
                    for (int j=0; j<nCols; j++)
                    {
                        pInitialSurface->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,elev,1,1,GDT_Float32,0,0);
                        if (*elev != fNoDataValue)
                        {
                            pSubOrig->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,subVal,1,1,GDT_Float32,0,0);
                            setupCell(i, j);
                        }
                    }
                }
            }
            else
            {
                //error
            }
        }
        else if (activeType == 3)
        {
            if (subType == 1)
            {
                *subVal = subSize;
                for (int i=0; i<nRows; i++)
                {
                    for (int j=0; j<nCols; j++)
                    {
                        pInitialSurface->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,elev,1,1,GDT_Float32,0,0);
                        if (*elev != fNoDataValue)
                        {
                            pActiveOrig->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,actVal,1,1,GDT_Float32,0,0);
                            setupCell(i, j);
                        }
                    }
                }
            }

            else if (subType == 2)
            {
                for (int i=0; i<nRows; i++)
                {
                    for (int j=0; j<nCols; j++)
                    {
                        pInitialSurface->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,elev,1,1,GDT_Float32,0,0);
                        if (*elev != fNoDataValue)
                        {
                            pActiveOrig->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,actVal,1,1,GDT_Float32,0,0);
                            *subVal = *actVal * (subSize/100.0);
                            setupCell(i, j);
                        }
                    }
                }
            }
            else if (subType == 3)
            {
                for (int i=0; i<nRows; i++)
                {
                    for (int j=0; j<nCols; j++)
                    {
                        pInitialSurface->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,elev,1,1,GDT_Float32,0,0);
                        if (*elev != fNoDataValue)
                        {
                            pActiveOrig->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,actVal,1,1,GDT_Float32,0,0);
                            pSubOrig->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,subVal,1,1,GDT_Float32,0,0);
                            setupCell(i, j);
                        }
                    }
                }
            }
            else
            {
                //error
            }
        }
    }
    else if(subSurf == 0)
    {
        if (activeType == 1)
        {
            *actVal = activeSize;
            *subVal = activeSize;
            for (int i=0; i<nRows; i++)
            {
                for (int j=0; j<nCols; j++)
                {
                    pInitialSurface->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,elev,1,1,GDT_Float32,0,0);
                    if (*elev != fNoDataValue)
                    {
                        setupCell(i, j);
                    }
                }
            }
        }
        else if (activeType == 3)
        {
            for (int i=0; i<nRows; i++)
            {
                for (int j=0; j<nCols; j++)
                {
                    pInitialSurface->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,elev,1,1,GDT_Float32,0,0);
                    if (*elev != fNoDataValue)
                    {
                        pActiveOrig->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,actVal,1,1,GDT_Float32,0,0);
                        *subVal = *actVal;
                        setupCell(i, j);
                    }
                }
            }
        }
        else
        {
            //error
        }
    }
}

void GrainSize::setupCell(int row, int col)
{
    bool done = false;
    //int min = *elev - thickness;
    for (int i=0; i<bandMinElev.size(); i++)
    {
        if(bandMinElev[i]<*elev && *elev<=(bandMinElev[i]+thickness))
        {
            *depth = *elev - bandMinElev[i];
            if (*depth <= thickness)
            {
                *actBand = bandNumber[i];
                pMain->GetRasterBand(bandNumber[i])->RasterIO(GF_Write,col,row,1,1,actVal,1,1,GDT_Float32,0,0);
                pActive->GetRasterBand(1)->RasterIO(GF_Write,col,row,1,1,actBand,1,1,GDT_Float32,0,0);
                pActive->GetRasterBand(2)->RasterIO(GF_Write,col,row,1,1,depth,1,1,GDT_Float32,0,0);

                for (int j=0; j<bandMinElev.size(); j++)
                {
                    if (bandMinElev[j]<bandMinElev[i])
                    {
                        pMain->GetRasterBand(bandNumber[j])->RasterIO(GF_Write,col,row,1,1,subVal,1,1,GDT_Float32,0,0);
                    }
                }
                done = true;
                break;
            }

            if (i == bandMinElev.size()-1 && done == false)
            {
                qDebug()<<"No depth set "<<*depth<<" at address "<<row<<" "<<col;
            }
        }
    }
}

void GrainSize::setupMain()
{
    pMain->SetGeoTransform(transform);
    pActive->SetGeoTransform(transform);
    pActive->GetRasterBand(1)->Fill(fNoDataValue);
    pActive->GetRasterBand(1)->SetNoDataValue(fNoDataValue);
    pActive->GetRasterBand(2)->Fill(fNoDataValue);
    pActive->GetRasterBand(2)->SetNoDataValue(fNoDataValue);

    //fill all bands with no data and assign minimum elevations to each band (stored in vector)
    qDebug()<<"Starting to fill main gs";
    int band;
    for(int i=0; i<bandCount; i++)
    {
        band = i+1;
        bandNumber.append(band);
        bandMinElev.append(minElev + (i*thickness));

        pMain->GetRasterBand(band)->Fill(fNoDataValue);
        pMain->GetRasterBand(band)->SetNoDataValue(fNoDataValue);
    }

    populateLayers();
    qDebug()<<"main gs filled";
}

void GrainSize::updateLayers(GDALDataset *oldDS)
{
    double partVol;
    int bandThick, count = 0;
    for (int i=0; i<nRows; i++)
    {
        for (int j=0; j<nCols; j++)
        {
            pDeposition->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,grainsize,1,1,GDT_Float32,0,0);

            if (*grainsize != fNoDataValue && *grainsize != 0.0)
            {
                pDeposition->GetRasterBand(2)->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
                pActive->GetRasterBand(2)->RasterIO(GF_Read,j,i,1,1,volume,1,1,GDT_Float32,0,0);
                pActive->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,actBand,1,1,GDT_Float32,0,0);
                oldDS->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,elev,1,1,GDT_Float32,0,0);
                if (*actBand < 0)
                {
                    *actBand = findBand(*elev);
                    {
                        if (*actBand < 0)
                        {
                            qDebug()<<"active band error: changing active band to 3";
                            *actBand = 3;
                        }
                    }
                }
                pMain->GetRasterBand(*actBand)->RasterIO(GF_Read,j,i,1,1,actVal,1,1,GDT_Float32,0,0);

                if ((*depth + *volume) >= thickness)
                {
                    qDebug()<<"adding layers for "<<(*depth + *volume)<<" top layer elev = "<<maxElev;
                    partVol = thickness - *volume;
                    *elev += partVol;
                    *depth -= partVol;
                    *subVal = ((((*grainsize)*partVol)+((*actVal)*(*volume)))/(partVol+(*volume)));
                    pMain->GetRasterBand(*actBand)->RasterIO(GF_Write,j,i,1,1,subVal,1,1,GDT_Float32,0,0);
                    bandThick = ceil(*depth/thickness);

                    if (*elev > (maxElev + 20.0))
                    {
                        qDebug()<<"TOO MUCH DEPO elev is "<<*elev<<" max elev is "<<maxElev;
                    }
                    else
                    {
                        for (int k=0; k<bandThick; k++)
                        {
                            if (k == bandThick-1)
                            {
                                *elev += *depth;
                                *actBand = findBand(*elev);
                                if (*actBand < 0 && *depth <= 20.0)
                                {
                                    count = 0;
                                    while (*actBand < 0 && count < 5)
                                    {
                                        qDebug()<<"(in if) adding top band max bands to add is "<<bandThick<<" elevation is "<<*elev<<" k "<<k<<" active band "<<*actBand;
                                        addBandTop();
                                        *actBand = findBand(*elev);
                                        qDebug()<<"new band "<<*actBand;
                                        count++;
                                    }
                                }
                                else if (*actBand < 0)
                                {
                                    qDebug()<<"(in if statement) active band reference less than 0 "<<*actBand;
                                }
                                else
                                {
                                    pActive->GetRasterBand(1)->RasterIO(GF_Write,j,i,1,1,actBand,1,1,GDT_Float32,0,0);
                                    pActive->GetRasterBand(2)->RasterIO(GF_Read,j,i,1,1,depth,1,1,GDT_Float32,0,0);
                                    pMain->GetRasterBand(*actBand)->RasterIO(GF_Write,j,i,1,1,grainsize,1,1,GDT_Float32,0,0);
                                }
                            }
                            else
                            {
                                *actBand = findBand(*elev);
                                if (*actBand < 0 && *depth <= 20.0)
                                {
                                    qDebug()<<"(in else) adding top band max bands to add is "<<bandThick<<" elevation is "<<*elev<<" volume remaining is "<<*depth<<" k "<<k<<" active band "<<*actBand;
                                    addBandTop();
                                    *actBand = findBand(*elev);
                                    qDebug()<<"new band "<<*actBand;
                                    if (*actBand < 0)
                                    {
                                        qDebug()<<"Error getting active band. . . elev: "<<*elev;
                                        break;
                                    }
                                }
                                if (*actBand >= 0)
                                {
                                    *elev += thickness;
                                    *depth -= thickness;
                                    *volume = thickness;
                                    pActive->GetRasterBand(1)->RasterIO(GF_Write,j,i,1,1,actBand,1,1,GDT_Float32,0,0);
                                    pActive->GetRasterBand(2)->RasterIO(GF_Read,j,i,1,1,volume,1,1,GDT_Float32,0,0);
                                    pMain->GetRasterBand(*actBand)->RasterIO(GF_Write,j,i,1,1,grainsize,1,1,GDT_Float32,0,0);
                                }
                            }
                        }
                    }
                }
                else
                {
                    *grainsize = ((((*grainsize)*(*depth))+((*actVal)*(*volume)))/((*depth)+(*volume)));
                    *volume += *depth;
                    pMain->GetRasterBand(*actBand)->RasterIO(GF_Write,j,i,1,1,grainsize,1,1,GDT_Float32,0,0);
                    pActive->GetRasterBand(2)->RasterIO(GF_Write,j,i,1,1,volume,1,1,GDT_Float32,0,0);
                }
            }
        }
    }
    qDebug()<<"GS layers updated";
}

}
