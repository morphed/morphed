#include "delft3dio.h"

namespace MORPH{

Delft3DIO::Delft3DIO(QString xmlPath):Surfaces(xmlPath)
{
    setDelftData();
}

Delft3DIO::Delft3DIO(XMLReadWrite &XmlObj):Surfaces(XmlObj)
{
    setDelftData();
}

Delft3DIO::~Delft3DIO()
{
    if (pExtendedDEM != NULL)
    {
        GDALClose(pExtendedDEM);
    }
}

void Delft3DIO::setDelftData()
{
    //Get coordinates for boundaries
    usx1 = XmlInit.readNodeData("Delft3DParameters", "USX1").toDouble();
    usx2 = XmlInit.readNodeData("Delft3DParameters", "USX2").toDouble();
    usy1 = XmlInit.readNodeData("Delft3DParameters", "USY1").toDouble();
    usy2 = XmlInit.readNodeData("Delft3DParameters", "USY2").toDouble();
    dsx1 = XmlInit.readNodeData("Delft3DParameters", "DSX1").toDouble();
    dsx2 = XmlInit.readNodeData("Delft3DParameters", "DSX2").toDouble();
    dsy1 = XmlInit.readNodeData("Delft3DParameters", "DSY1").toDouble();
    dsy2 = XmlInit.readNodeData("Delft3DParameters", "DSY2").toDouble();

    //Get other parameters
    delftPath = XmlInit.readNodeData("Delft3DParameters", "DelftPath");
    simTime = XmlInit.readNodeData("Delft3DParameters", "SimTime").toDouble();
    timeStep = XmlInit.readNodeData("Delft3DParameters", "TimeStep").toDouble();
    roughness = XmlInit.readNodeData("Delft3DParameters", "Roughness").toDouble();
    HEV = XmlInit.readNodeData("Delft3DParameters", "HEV").toDouble();
    setOriginPoint();

    //Convert boundary coordinates to cell addresses
    nusx1 = xCellAddress(usx1);
    nusx2 = xCellAddress(usx2);
    nusy1 = yCellAddress(usy1);
    nusy2 = yCellAddress(usy2);
    ndsx1 = xCellAddress(dsx1);
    ndsx2 = xCellAddress(dsx2);
    ndsy1 = yCellAddress(dsy1);
    ndsy2 = yCellAddress(dsy2);
    qDebug()<<"downstream bound coords "<<ndsx1<<" "<<ndsx2<<" "<<ndsy1<<" "<<ndsy2;

     qsDemPath = qsTempDir +"/extended.tif";
     pInitialSurface->GetGeoTransform(extTransform);

     origDemMax = findMax(pInitialSurface);
}

void Delft3DIO::closeDelftDatasets()
{
    if (pExtendedDEM != NULL)
    {
        GDALClose(pExtendedDEM);
        QFile::remove(qsDemPath);
        pExtendedDEM = NULL;
    }
    closeDEMs();

    xUSCoords.clear();
    yUSCoords.clear();
    xObsCoords.clear();
    yObsCoords.clear();
    dsCoords.clear();
}

void Delft3DIO::extendDEMBoundary()
{
    loadDrivers();
    double dSlope, transAmt;;
    char **papszOptions = NULL;
    float *oldValue = (float*) CPLMalloc(sizeof(float)*nCols);
    float *newValue = (float*) CPLMalloc(sizeof(float)*nCols);

    qDebug()<<"extend vars set";

    if (nDirUSbound == 1)
    {
        nRowsExt = nRows + nAddCells;
        nColsExt = nCols;
        qDebug()<<"setting extended pointer";
        pExtendedDEM = pDriverTIFF->Create(qsDemPath.toStdString().c_str(), nColsExt, nRowsExt, 1, GDT_Float32, papszOptions);
        qDebug()<<"extended pointer set";
        pExtendedDEM->GetRasterBand(1)->Fill(fNoDataValue);
        qDebug()<<"filled with nodata";
        transAmt = nAddCells*cellWidth;
        if (transAmt < 0)
        {
            transAmt *= (-1);
        }
        qDebug()<<"changing transform";
        extTransform[3] = extTransform[3] + transAmt;
        qDebug()<<"transfrom chaged";
        pExtendedDEM->SetGeoTransform(extTransform);
        qDebug()<<"calculating average slope";

        dSlope = calculateAverageSlope_RowAve(pOldSurface, nRows-2, 2);

        //copy data from DEM
        qDebug()<<"copying data, average slope "<<dSlope;
        for (int i=0; i<nRows; i++)
        {
            pOldSurface->GetRasterBand(1)->RasterIO(GF_Read,0,i,nCols,1,oldValue,nCols,1,GDT_Float32,0,0);
            pExtendedDEM->GetRasterBand(1)->RasterIO(GF_Write,0,i+nAddCells,nCols,1,oldValue,nCols,1,GDT_Float32,0,0);
        }
        qDebug()<<"data copied";
        //add new data to extended DEM
        pExtendedDEM->GetRasterBand(1)->RasterIO(GF_Read,0,nAddCells+3,nCols,1,oldValue,nCols,1,GDT_Float32,0,0);
        for (int i=nAddCells+2; i>=0; i--)
        {
            for (int j=0; j<nCols; j++)
            {   
                if (oldValue[j] != fNoDataValue)
                {
                    newValue[j] = oldValue[j] + (dSlope * cellWidth * ((nAddCells+3)-i));
                }
                else
                {
                    newValue[j] = fNoDataValue;
                }
            }
            pExtendedDEM->GetRasterBand(1)->RasterIO(GF_Write,0,i,nCols,1,newValue,nCols,1,GDT_Float32,0,0);
        }
        qDebug()<<"data added";
    }
    else if (nDirUSbound == 2)
    {
        nRowsExt = nRows + nAddCells;
        nColsExt = nCols;
        pExtendedDEM = pDriverTIFF->Create(qsDemPath.toStdString().c_str(), nColsExt, nRowsExt, 1, GDT_Float32, papszOptions);
        pExtendedDEM->SetGeoTransform(extTransform);

        dSlope = calculateAverageSlope_RowAve(pOldSurface, nRows-2, 2);

        //copy data from DEM
        for (int i=0; i<nRows; i++)
        {
            pOldSurface->GetRasterBand(1)->RasterIO(GF_Read,0,i,nCols,1,oldValue,nCols,1,GDT_Float32,0,0);
            pExtendedDEM->GetRasterBand(1)->RasterIO(GF_Write,0,i,nCols,1,oldValue,nCols,1,GDT_Float32,0,0);
        }
        qDebug()<<"Data copied";
        //add new data to extended DEM
        pExtendedDEM->GetRasterBand(1)->RasterIO(GF_Read,0,nRows-2,nCols,1,oldValue,nCols,1,GDT_Float32,0,0);
        for (int i=nRows-2; i<nRowsExt; i++)
        {
            for (int j=0; j<nCols; j++)
            {
                if (oldValue[j] != fNoDataValue)
                {
                    newValue[j] = oldValue[j] + (dSlope * cellWidth * (i-(nRows-3)));
                }
                else
                {
                    newValue[j] = fNoDataValue;
                }
            }
            pExtendedDEM->GetRasterBand(1)->RasterIO(GF_Write,0,i,nCols,1,newValue,nCols,1,GDT_Float32,0,0);
        }
        qDebug()<<"Data added";
    }
    else if (nDirUSbound == 3)
    {
        nRowsExt = nRows;
        nColsExt = nCols + nAddCells;
        pExtendedDEM = pDriverTIFF->Create(qsDemPath.toStdString().c_str(), nColsExt, nRowsExt, 1, GDT_Float32, papszOptions);
        pExtendedDEM->GetRasterBand(1)->Fill(fNoDataValue);
        pOldSurface->GetGeoTransform(extTransform);
        pExtendedDEM->SetGeoTransform(extTransform);

        dSlope = calculateAverageSlope_ColAve(pOldSurface, nCols-1, 0);

        //copy data from DEM
        for (int i=0; i<nRows; i++)
        {
            for (int j=0; j<nCols; j++)
            {
                pOldSurface->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,oldValue,1,1,GDT_Float32,0,0);
                pExtendedDEM->GetRasterBand(1)->RasterIO(GF_Write,j,i,1,1,oldValue,1,1,GDT_Float32,0,0);
            }
        }

        //add new data to extended DEM
        for (int i=0; i<nRows; i++)
        {
            for (int j=nCols; j<nCols; j++)
            {
                pExtendedDEM->GetRasterBand(1)->RasterIO(GF_Read,j-1,i,1,1,oldValue,1,1,GDT_Float32,0,0);
                *newValue = *oldValue + dSlope * cellWidth;
                if (*newValue < 0)
                {
                    *newValue *= (-1);
                }
                if (*oldValue != fNoDataValue)
                {
                    pExtendedDEM->GetRasterBand(1)->RasterIO(GF_Write,j,i,1,1,newValue,1,1,GDT_Float32,0,0);
                }
            }
        }
    }
    else if (nDirUSbound == 4)
    {
        nRowsExt = nRows;
        nColsExt = nCols + nAddCells;
        pExtendedDEM = pDriverTIFF->Create(qsDemPath.toStdString().c_str(), nColsExt, nRowsExt, 1, GDT_Float32, papszOptions);
        pExtendedDEM->GetRasterBand(1)->Fill(fNoDataValue);

        pInitialSurface->GetGeoTransform(extTransform);
        transAmt = nAddCells*cellWidth;
        if (transAmt < 0)
        {
            transAmt *= (-1);
        }
        extTransform[0] -= transAmt;
        pExtendedDEM->SetGeoTransform(extTransform);

        dSlope = calculateAverageSlope_RowAve(pOldSurface, nCols-1, 0);

        //copy data from DEM
        for (int i=0; i<nRows; i++)
        {
            for (int j=0; j<nCols; j++)
            {
                pOldSurface->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,oldValue,1,1,GDT_Float32,0,0);
                pExtendedDEM->GetRasterBand(1)->RasterIO(GF_Write,j+nAddCells,i,1,1,oldValue,1,1,GDT_Float32,0,0);
            }
        }

        //add new data to extended DEM
        for (int i=0; i<nRows; i++)
        {
            for (int j=nAddCells-1; j>=0; j--)
            {
                pExtendedDEM->GetRasterBand(1)->RasterIO(GF_Read,j+1,i,1,1,oldValue,1,1,GDT_Float32,0,0);
                *newValue = *oldValue + dSlope * cellHeight;
                if (*newValue < 0)
                {
                    *newValue *= (-1);
                }
                if (*oldValue != fNoDataValue)
                {
                    pExtendedDEM->GetRasterBand(1)->RasterIO(GF_Write,j,i,1,1,newValue,1,1,GDT_Float32,0,0);
                }
            }
        }
    }
    else
    {

    }
    qDebug()<<"Freeing memory";
    CPLFree(oldValue);
    CPLFree(newValue);
    oldValue = NULL;
    newValue = NULL;
    qDebug()<<"Memory freed, finished";
}

void Delft3DIO::run(QThread *thread)
{
    double max;
    QString delftDir = qsInputs + "/Delft3D";
    QString qpQuit = "TASKKILL /F /IM d3d_qp.exec";
    QFile xvt(qsInputs + "/Hydraulics/Xvelocity.xyz"),yvt(qsInputs + "/Hydraulics/Yvelocity.xyz"),sst(qsInputs + "/Hydraulics/BedShearStress.xyz"),wdt(qsInputs + "/Hydraulics/WaterDepth.xyz"), data(qsInputs + "/Delft3D/trim-" + qsFloodName + ".dat");
    bool exist1, exist2, exist3, exist4, existData;
    int count;

    extendDEMBoundary();
    //rescale DSWE to new maximum elevation
    qDebug()<<"old dswe "<<dswe[nCurrentIteration];
    max = findMax(pOldSurface);
    dswe[nCurrentIteration] = dswe[nCurrentIteration] - fabs(max - origDemMax);
    qDebug()<<"new dswe "<<dswe[nCurrentIteration];

    setBoundaries();
    qDebug()<<"boundaries set";
    setOutputPaths();
    writeXYZ();
    writeBCQ();
    writeBND();
    writeDEP();
    writeDIS();
    writeENC();
    writeFIL();
    writeGRD();
    writeINI();
    writeMDF();
    writeOBS();
    writeOutputMacro();
    writeSRC();
    qDebug()<<"delft files printed";

    QString flowCall = delftPath + "/w32/flow/bin/deltares_hydro.exe " + qsFloodName + ".ini";
    QString qpCall = delftPath + "/w32/quickplot/bin/d3d_qp.exe run " + qsFloodName + ".m";
    QProcess flowRun;
    QProcess qpRun;
    qDebug()<<"delft sim setup";
    //QProcess qpEnd;
    chdir(delftDir.toStdString().c_str());
    qDebug()<<"dir changed";
    flowRun.execute(flowCall);
    flowRun.waitForFinished(-1);
    thread->sleep(5);
    chdir(delftDir.toStdString().c_str());

    existData = data.exists();

    if (existData)
    {
        qpRun.execute(qpCall);
        qpRun.waitForFinished(-1);
        thread->sleep(5);

        exist1 = xvt.exists(), exist2 = yvt.exists(), exist3 = sst.exists(), exist4 = wdt.exists();

        if (!exist1 || !exist2 || !exist3 || !exist4)
        {
            count = 0;

            while ((!exist1 || !exist2 || !exist3 || !exist4) && count < 15)
            {
                qpRun.execute(qpQuit);
                thread->sleep(2);
                qpRun.execute(qpCall);
                thread->sleep(4);
                exist1 = xvt.exists(), exist2 = yvt.exists(), exist3 = sst.exists(), exist4 = wdt.exists();
                if (!exist1 || !exist2 || !exist3 || !exist4)
                {
                    thread->sleep(4);
                    exist1 = xvt.exists(), exist2 = yvt.exists(), exist3 = sst.exists(), exist4 = wdt.exists();
                    if (!exist1 || !exist2 || !exist3 || !exist4)
                    {
                        thread->sleep(4);
                    }
                }
                count++;
                exist1 = xvt.exists(), exist2 = yvt.exists(), exist3 = sst.exists(), exist4 = wdt.exists();
                if ((!exist1 || !exist2 || !exist3 || !exist4) && count == 9)
                {
                    qDebug()<<"Hydraulics files do not exist after 10 attempts\n";
                }
            }
        }
        else
        {
            qpRun.execute(qpQuit);
        }
    }
    else
    {
        qDebug()<<"Delft3D data file does not exist";
    }

    qpRun.execute(qpQuit);
    flowRun.close();
    qpRun.close();

}

void Delft3DIO::runFlow()
{
    QString flowCall = delftPath + "/w32/flow/bin/deltares_hydro.exe " + qsFloodName + ".ini";
    QString delftDir = qsInputs + "/Delft3D";
    QProcess flowRun;
    chdir(delftDir.toStdString().c_str());
    flowRun.execute(flowCall);
    flowRun.waitForFinished(-1);
}

void Delft3DIO::runQuickplot()
{
    QString qpCall = delftPath + "/w32/quickplot/bin/d3d_qp.exe run " + qsFloodName + ".m";
    QString delftDir = qsInputs + "/Delft3D";
    QProcess qpRun;
    chdir(delftDir.toStdString().c_str());
    qpRun.execute(qpCall);
    qpRun.waitForFinished(-1);
}

void Delft3DIO::setBoundaries()
{
    //upstream boundary
    if (nDirUSbound == 1)
    {
        setXDischargeAddresses();
    }
    else if (nDirUSbound == 2)
    {
        setXDischargeAddresses();
    }
    else if (nDirUSbound == 3)
    {

    }
    else if (nDirUSbound == 4)
    {

    }
    else
    {

    }

    //downstream boundary
    if (nDirDSbound == 1)
    {
        if (nusx1>nusx2)
        {
            dsCoords.append(70);
            dsCoords.append(752);
            dsCoords.append(223);
            dsCoords.append(752);
        }
        else if (nusx2>nusx1)
        {
            dsCoords.append(70);
            dsCoords.append(752);
            dsCoords.append(223);
            dsCoords.append(752);
        }
        qDebug()<<"DS bounds set";
    }
    else if (nDirDSbound == 2)
    {
        if (nusx1>nusx2)
        {
            dsCoords.append(ndsx2);
            dsCoords.append(3);
            dsCoords.append(ndsx1);
            dsCoords.append(3);
        }
        else if (nusx2>nusx1)
        {
            dsCoords.append(ndsx1);
            dsCoords.append(1);
            dsCoords.append(ndsx2);
            dsCoords.append(1);
        }
    }
    else if (nDirDSbound == 3)
    {

    }
    else if (nDirDSbound == 4)
    {

    }
    else
    {

    }

    //obeservation points
    if (nDirUSbound == 1)
    {
        for (int i=0; i<3; i++)
        {
            yObsCoords.append(yUSCoords[0] - 10 - i);
            xObsCoords.append(xUSCoords[0]);
        }
    }
    else if (nDirUSbound == 2)
    {
        for (int i=0; i<3; i++)
        {
            yObsCoords.append(yUSCoords[0] + 10 + i);
            xObsCoords.append(xUSCoords[0]);
        }
        qDebug()<<"obs points set";

    }
    else if (nDirUSbound == 3)
    {
        for (int i=0; i<3; i++)
        {
            yObsCoords.append(yUSCoords[i]);
            xObsCoords.append(xUSCoords[i] - 10);
        }
    }
    else if (nDirUSbound == 4)
    {
        for (int i=0; i<3; i++)
        {
            yObsCoords.append(yUSCoords[i]);
            xObsCoords.append(xUSCoords[i] + 10);
        }
    }
    else
    {

    }
}

void Delft3DIO::setOriginPoint()
{
    if (nDirUSbound == 1)
    {
        xOrigin = xTopLeft;
        yOrigin = yTopLeft - (nRows*cellWidth);
    }
    else if (nDirUSbound == 2)
    {
        xOrigin = xTopLeft;
        yOrigin = yTopLeft - ((nRows+nAddCells)*cellWidth);
    }
    else if (nDirUSbound == 3)
    {
        xOrigin = xTopLeft;
        yOrigin = yTopLeft - (nRows*cellWidth);
    }
    else if (nDirUSbound == 4)
    {
        xOrigin = xTopLeft - (nAddCells*cellWidth);
        yOrigin = yTopLeft - (nRows*cellWidth);
    }
    else
    {

    }
}

void Delft3DIO::setOutputPaths()
{
    qsBcq = qsInputs + "/Delft3D/" + qsFloodName + ".bcq";
    qsBnd = qsInputs + "/Delft3D/" + qsFloodName + ".bnd";
    qsDep = qsInputs + "/Delft3D/" + qsFloodName + ".dep";
    qsDis = qsInputs + "/Delft3D/" + qsFloodName + ".dis";
    qsEnc = qsInputs + "/Delft3D/" + qsFloodName + ".enc";
    qsFil = qsInputs + "/Delft3D/" + qsFloodName + ".fil";
    qsGrd = qsInputs + "/Delft3D/" + qsFloodName + ".grd";
    qsIni = qsInputs + "/Delft3D/" + qsFloodName + ".ini";
    qsMacro = qsInputs + "/Delft3D/" + qsFloodName + ".m";
    qsMdf = qsInputs + "/Delft3D/" + qsFloodName + ".mdf";
    qsObs = qsInputs + "/Delft3D/" + qsFloodName + ".obs";
    qsSrc = qsInputs + "/Delft3D/" + qsFloodName + ".src";
    qsXyz = qsInputs + "/Delft3D/" + qsFloodName + ".xyz";
}

void Delft3DIO::setXDischargeAddresses()
{
    int addedCells = 0, count = 0, addedRows = 0;

    //find which address has the higher value and assign it as start
    if(nusx1>nusx2)
    {
        //move edge cells 1 cell into channel to prevent discharge on dry cell
        xstart = nusx2+1;
        xend = nusx1-1;
    }
    else
    {
        xstart = nusx1+1;
        xend = nusx2-1;
    }
    //determine number of cells along boundary
    count = xend-xstart;
    //if less than 10 boundary cells, add cells to boundary
    if (count < 10)
    {
        //add a row of count-2 cells, subract 2 in case channel turns
        addedCells = count-2;
        count += addedCells;
        //update added rows
        addedRows = 1;
        //check if count is still less than 10
        if (count < 10)
        {
            //add another row of the same size
            count += addedCells;
            addedRows = 2;
            //update added cells
            addedCells *= 2;
        }
    }
    qDebug()<<"cell count determined";
    //assign x address for all discharge elements and add to vector
    if (nDirUSbound == 1)
    {
        for (int i=0; i<count; i++)
        {
            //first row of cells
            if (i < count-addedCells || addedRows == 0)
            {
                xUSCoords.append(xstart+i);
                yUSCoords.append(nusy1);
            }
            else if (i < count - (addedCells/addedRows) || addedRows == 1)
            {
                xUSCoords.append(xstart+(i-(addedCells/addedRows))+1);
                yUSCoords.append(nusy1+1);
            }
            else
            {
                xUSCoords.append(xstart+(i-addedCells)+1);
                yUSCoords.append(nusy1+2);
            }
        }
    }
    else if (nDirUSbound == 2)
    {
        for (int i=0; i<count; i++)
        {
            //first row of cells
            if (i < count-addedCells || addedRows == 0)
            {
                xUSCoords.append(xstart+i);
                yUSCoords.append(nusy1-nAddCells);
            }
            else if (i < count - (addedCells/addedRows) || addedRows == 1)
            {
                xUSCoords.append(xstart+(i-(addedCells/addedRows))-1);
                yUSCoords.append(nusy1+1-nAddCells);
            }
            else
            {
                xUSCoords.append(xstart+(i-addedCells)-1);
                yUSCoords.append(nusy1+2-nAddCells);
            }
        }
    }
    else if (nDirUSbound == 3)
    {

    }
    else if (nDirUSbound == 4)
    {

    }
    else
    {

    }
    disCount = count;
    qDebug()<<"X addresses set";
}

void Delft3DIO::setYDischargeAddresses()
{
    int addedCells = 0, count = 0, addedCols = 0;

    if (nusy1 > nusy2)
    {
        ystart = nusy1+1;
        yend = nusy2-1;
    }
    else
    {
        ystart = nusy2+1;
        yend = nusy1-1;
    }
    count = yend-ystart;

    if (count < 10)
    {
        addedCells = count-2;
        count += addedCells;
        addedCols = 1;
        if (count < 10)
        {
            count += addedCells;
            addedCols = 2;
            addedCells *= 2;
        }
    }

    if (nDirUSbound == 3)
    {
        for (int i=0; i<count; i++)
        {
            if (i < count-addedCells || addedCols==0)
            {
                yUSCoords.append(ystart-i);
                xUSCoords.append(nusx1);
            }
            else if (i < count - (addedCells/addedCols))
            {
                yUSCoords.append(ystart-i-1);
                xUSCoords.append(nusx1-1);
            }
            else
            {
                yUSCoords.append(ystart-i-1);
                xUSCoords.append(nusx1-2);
            }
        }
    }
    else if (nDirUSbound == 4)
    {
        for (int i=0; i<count; i++)
        {
            if (i < count-addedCells || addedCols==0)
            {
                yUSCoords.append(ystart-i);
                xUSCoords.append(nusx1);
            }
            else if (i < count - (addedCells/addedCols))
            {
                yUSCoords.append(ystart-i-1);
                xUSCoords.append(nusx1+1);
            }
            else
            {
                yUSCoords.append(ystart-i-1);
                xUSCoords.append(nusx1+2);
            }
        }
    }
    disCount = count;
}

void Delft3DIO::writeBND()
{
    QFile fout(qsBnd);
    fout.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&fout);

    out<<"("<<dsCoords[0]<<","<<dsCoords[1]<<")..("<<dsCoords[2]<<","<<dsCoords[3]<<")    Z Q    "<<dsCoords[0]<<"    "<<dsCoords[1]<<"    "<<dsCoords[2]<<"    "<<dsCoords[3]<<"    0.0";

    fout.close();
}

void Delft3DIO::writeBCQ()
{
    QFile fout(qsBcq);
    fout.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&fout);

    out<<"table-name\t\t\t'Boundary Section : 1'\n";
    out<<"contents\t\t\t'Uniform             '\n";
    out<<"location\t\t\t'("<<dsCoords[0]<<","<<dsCoords[1]<<")..("<<dsCoords[2]<<","<<dsCoords[3]<<") '\n";
    out<<"xy-function\t\t\t'equidistant'\n";
    out<<"interpolation\t\t'linear'\n";
    out<<"parameter\t\t\t'total discharge (t) end A'\t\t\tunit '[m3/s]'\n";
    out<<"parameter\t\t\t'water elevation (z) end B'\t\t\tunit '[m]'\n";
    out<<"records-in-table\t2\n";
    out<<"\t"<<q[nCurrentIteration]<<"\t"<<dswe[nCurrentIteration]<<"\n";
    out<<"\t"<<q[nCurrentIteration]<<"\t"<<dswe[nCurrentIteration]<<"\n";

    fout.close();
}

void Delft3DIO::writeDEP()
{
    QFile fout(qsDep);
    fout.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&fout);
    int count = 0;
    double max = findMax(pExtendedDEM);
    float *value = (float*) CPLMalloc(sizeof(float)*1);

    out.setRealNumberNotation(QTextStream::ScientificNotation);
    out.setRealNumberPrecision(7);
    //add row of no data values to top of file
    out<<DELFT_NODATA<<"\t";
    count++;

    for (int i=0; i<nColsExt; i++)
    {
        out<<DELFT_NODATA<<"\t";
        count++;
        if (count == 12)
        {
            out<<"\n";
            count = 0;
        }
    }
    out<<DELFT_NODATA;
    out<<"\n";

    for (int i=nRowsExt-1; i>=0; i--)
    {
        //add column of no data values to edge of file
        count = 0;
        out<<DELFT_NODATA<<"\t";
        count++;

        for (int j=0; j<nColsExt; j++)
        {
            pExtendedDEM->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,value,1,1,GDT_Float32,0,0);
            if (*value >= 0)
            {
                out<<" ";
            }
            if (*value == fNoDataValue)
            {
                out<<DELFT_NODATA<<"\t";
            }
            else
            {
                *value = (*value-max) * (-1.0);
                out<<*value<<"\t";
            }
            count++;
            if (count == 12)
            {
                out<<"\n";
                count = 0;
            }
        }
        out<<DELFT_NODATA;
        out<<"\n";
    }
    //add row of no data values to bottom of file
    count = 0;
    out<<DELFT_NODATA<<"\t";
    count++;
    for (int i=0; i<nColsExt; i++)
    {
        out<<DELFT_NODATA<<"\t";
        count++;
        if (count ==12)
        {
            out<<"\n";
            count = 0;
        }
    }
    out<<DELFT_NODATA;
    out<<"\n";

    fout.close();

    CPLFree(value);
    value = NULL;
}

void Delft3DIO::writeDIS()
{
    QFile fout(qsDis);
    fout.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&fout);
    QDateTime Itdate;
    Itdate = QDateTime::fromTime_t(date[nCurrentIteration]);

    for (int i=0; i<disCount; i++)
    {
        out<<"table-name\t\t\t 'Discharge : "<<i+1<<"'\n";
        out<<"contents\t\t\t 'regular   '\n";
        out<<"location\t\t\t '("<<xUSCoords[i]<<","<<yUSCoords[i]<<")             '\n";
        out<<"time-function\t\t 'non-equidistant'\n";
        out<<"reference-time\t\t "<<Itdate.toString("yyyyMMdd")<<"\n";
        out<<"time-unit\t\t\t 'minutes'\n";
        out<<"interpolation\t\t 'linear'\n";
        out<<"parameter\t\t\t 'time                '                     unit '[min]'\n";
        out<<"parameter\t\t\t 'flux/discharge rate '                     unit '[m3/s]'\n";
        out<<"records-in-table\t 2\n";
        out.setRealNumberNotation(QTextStream::ScientificNotation);
        out<<" "<<"0.0"<<"\t"<<q[nCurrentIteration]/disCount<<"\n";
        out<<" "<<simTime<<"\t"<<q[nCurrentIteration]/disCount<<"\n";
        out.setRealNumberNotation(QTextStream::FixedNotation);
    }
    fout.close();
}

void Delft3DIO::writeENC()
{
    QFile fout(qsEnc);
    fout.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&fout);

    out<<"\t 1\t1\t*** begin external enclosure\n";
    out<<"\t"<<nColsExt+2<<"\t1\n";
    out<<"\t"<<nColsExt+2<<"\t"<<nRowsExt+2<<"\n";
    out<<"\t 1\t"<<nRowsExt+2<<"\n";
    out<<"\t 1\t1\t*** end external grid enclosure\n";
}

void Delft3DIO::writeFIL()
{
    QFile fout(qsFil);
    fout.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&fout);

    out<<"Domain, Checked : Yes\n";
    out<<"\t Grid : "<<qsFloodName<<".grd\n";
    out<<"\t Grid enclosure : "<<qsFloodName<<".enc\n";
    out<<"\t Bathymetry : "<<qsFloodName<<".dep\n";
    out<<"\t Dry points : none\n";
    out<<"\t Thin dams : none\n";
    out<<"\t "<<endl;

    out<<"Time frame, Checked : Yes\n\n";

    out<<"Processes, Checked : Yes\n\n";

    out<<"Initial conditions, Checked : Yes\n\n";

    out<<"Boundaries, Checked : Yes\n";
    out<<"\t Boundary definitions : "<<qsFloodName<<".bnd\n";
    out<<"\t Astronomical flow conditions : none\n";
    out<<"\t Astronomical corrections : none\n";
    out<<"\t Harmonic flow conditions : none\n";
    out<<"\t QH-relation flow conditions : "<<qsFloodName<<".bcq\n";
    out<<"\t Time series flow conditions : none\n";
    out<<"\t Transport conditions : none\n";
    out<<"\t \n";

    out<<"Physical parameters, Checked : Yes\n";
    out<<"\t Roughness coefficients : none\n";
    out<<"\t Hor. viscosity/diffusivity : none\n";
    out<<"\t Heat flux model data : none\n";
    out<<"\t Sediment data : none\n";
    out<<"\t Morphology data : none\n";
    out<<"\t Uniform wind data : none\n";
    out<<"\t Space varying wind data : none\n";
    out<<"\t \n";

    out<<"Numerical parameters, Checked : Yes\n\n";

    out<<"Operations, Checked : Yes\n";
    out<<"\t Discharge definitions : "<<qsFloodName<<".src\n";
    out<<"\t Discharge data : "<<qsFloodName<<".dis\n";
    out<<"\t Dredging and dumping data : none\n";
    out<<"\t \n";

    out<<"Monitoring, Checked : Yes\n";
    out<<"\t Observation points : "<<qsFloodName<<".obs\n";
    out<<"\t Drogues : none\n";
    out<<"\t Cross-sections : none\n";
    out<<"\t \n";

    out<<"Additional parameters, Checked : Yes\n\n";

    out<<"Output, Checked : No\n";
    out<<"\t Fourier analysis data : none\n";
    out<<"\t ";

    fout.close();
}

void Delft3DIO::writeGRD()
{
    QFile fout(qsGrd);
    fout.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&fout);

    QDateTime currentDT = QDateTime::currentDateTime();
    QString dt = currentDT.toString("yyyy-MM-dd, hh:mm:ss");
    int count;

    out<<"*"<<endl;
    out<<"* Deltares, Delft3D-RGFGRID Version 4.18.01.18383, Sep  7 2011, 14:23:22\n";
    out<<"* File creation date: "<<dt<<"\n";
    out<<"* \n";
    out<<"Coordinate System = Cartesian\n";
    out<<"\t "<<nColsExt+1<<"\t "<<nRowsExt+1<<"\n";
    out<<" 0 0 0"<<"\n";

    out.setRealNumberNotation(QTextStream::ScientificNotation);
    out.setRealNumberPrecision(15);
    for (int i=0; i<nRowsExt+1; i++)
    {
        count = 0;
        out<<" ETA=\t"<<i+1<<"\t";

        for (int j=0; j<nColsExt+1; j++)
        {
            out<<xTopLeft+(j*cellWidth)<<"\t";
            count++;
            if (count == 5)
            {
                out<<"\n\t\t\t";
                count = 0;
            }
        }
        out<<"\n";
    }

    for (int i=0; i<nRowsExt+1; i++)
    {
        count = 0;
        out<<" ETA=\t"<<i+1<<"\t";

        for (int j=0; j<nColsExt+1; j++)
        {
            out<<yOrigin+(i*cellWidth)<<"\t";
            count++;
            if (count == 5)
            {
                out<<"\n\t\t\t";
                count = 0;
            }
        }
        out<<endl;
    }

    fout.close();
}

void Delft3DIO::writeINI()
{
    QFile fout(qsIni);
    fout.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&fout);

    out<<"[Compnent]\n";
    out<<"Name = flow2d3d\n";
    out<<"MDFfile = "<<qsFloodName<<".mdf";

    fout.close();
}

void Delft3DIO::writeMDF()
{
    QFile fout(qsMdf);
    fout.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&fout);
    QDateTime Itdate;
    Itdate = QDateTime::fromTime_t(date[nCurrentIteration]);

    out.setRealNumberNotation(QTextStream::ScientificNotation);
    out.setRealNumberPrecision(7);

    out<<"Ident  = #Delft3D-FLOW 3.43.05.22651#"<<"\n";
    out<<"Commnt = "<<"\n";
    out<<"Runtxt = "<<"\n";
    out<<"Filcco = #"<<qsFloodName<<".grd#"<<"\n";
    out<<"Anglat =  0.0"<<"\n";
    out<<"Grdang =  0.0"<<"\n";
    out<<"Filgrd = #"<<qsFloodName<<".enc#"<<"\n";
    out<<"MNKmax = "<<nColsExt+2<<" "<<nRowsExt+2<<" "<<"1.0\n";//cellWidth<<"\n";
    out<<"Thick  =  100.0"<<"\n";
    out<<"Commnt = "<<"\n";
    out<<"Fildep = #"<<qsFloodName<<".dep#"<<"\n";
    out<<"Commnt = "<<"\n";
    out<<"Commnt =                 no. dry points: 0"<<"\n";
    out<<"Commnt =                 no. thin dams: 0"<<"\n";
    out<<"Commnt = "<<"\n";
    out<<"Itdate = #"<<Itdate.toString("yyyy-MM-dd")<<"#"<<"\n";
    out<<"Tunit  = #M#"<<"\n";
    out<<"Tstart =  "<<0.0<<"\n";
    out<<"Tstop  =  "<<simTime<<"\n";
    out<<"Dt     = "<<timeStep<<"\n";
    out<<"Tzone  = 0"<<"\n";
    out<<"Commnt = "<<"\n";
    out<<"Sub1   = #    #"<<"\n";
    out<<"Sub2   = #   #"<<"\n";
    out<<"Commnt = "<<"\n";
    out<<"Wnsvwp = #N#"<<"\n";
    out<<"Wndint = #Y#"<<"\n";
    out<<"Commnt = "<<"\n";
    out<<"Zeta0  = "<<dswe[nCurrentIteration]<<"\n";
    out<<"Commnt = "<<"\n";
    out<<"Commnt = no. open boundaries: 1"<<"\n";
    out<<"Filbnd = #"<<qsFloodName<<".bnd#"<<"\n";
    out<<"FilbcQ = #"<<qsFloodName<<".bcq#"<<"\n";
    out<<"Commnt = "<<"\n";
    out<<"Ag     =  9.81"<<"\n";
    out<<"Rhow   =  1000.0"<<"\n";
    out<<"Tempw  =  15.0"<<"\n";
    out<<"Salw   =  31.0"<<"\n";
    out<<"Wstres =  0.00063 0 0.00723 100.0 0.00723 100.0"<<"\n";
    out<<"Rhoa   =  1.0"<<"\n";
    out<<"Betac  =  0.5"<<"\n";
    out<<"Equil  = #N#"<<"\n";
    out<<"Ktemp  = 0"<<"\n";
    out<<"Fclou  =  0.0"<<"\n";
    out<<"Sarea  =  0.0"<<"\n";
    out<<"Temint = #Y#"<<"\n";
    out<<"Commnt = "<<"\n";
    out<<"Roumet = #W#"<<"\n";
    out<<"Ccofu  =  "<<roughness <<"\n";
    out<<"Ccofv  =  "<<roughness <<"\n";
    out<<"Xlo    =  0.0"<<"\n";
    out<<"Vicouv =  "<<HEV <<"\n";
    out<<"Dicouv =  10.0"<<"\n";
    out<<"Htur2d = #N#"<<"\n";
    out<<"Irov   = 0"<<"\n";
    out<<"Commnt = "<<"\n";
    out<<"Iter   =      2"<<"\n";
    out<<"Dryflp = #YES#"<<"\n";
    out<<"Dpsopt = #DP#"<<"\n";
    out<<"Dpuopt = #MIN#"<<"\n";
    out<<"Dryflc =  0.05"<<"\n";
    out<<"Dco    = -999.0"<<"\n";
    out<<"Tlfsmo = 1.0"<<"\n";
    out<<"ThetQH = 0.0"<<"\n";
    out<<"Forfuv = #Y#"<<"\n";
    out<<"Forfuw = #N#"<<"\n";
    out<<"Sigcor = #N#"<<"\n";
    out<<"Trasol = #Cyclic-method#"<<"\n";
    out<<"Momsol = #Cyclic#"<<"\n";
    out<<"Commnt = "<<"\n";
    out<<"Commnt =                 no. discharges: 5"<<"\n";
    out<<"Filsrc = #"<<qsFloodName<<".src#"<<"\n";
    out<<"Fildis = #"<<qsFloodName<<".dis#"<<"\n";
    out<<"Commnt =                 no. observation points: 3"<<"\n";
    out<<"Filsta = #"<<qsFloodName<<".obs#"<<"\n";
    out<<"Commnt =                 no. drogues: 0"<<"\n";
    out<<"Commnt = "<<"\n";
    out<<"Commnt = "<<"\n";
    out<<"Commnt =                 no. cross sections: 0"<<"\n";
    out<<"Commnt = "<<"\n";
    out<<"SMhydr = #YYYYY#"<<"\n";
    out<<"SMderv = #YYYYYY#"<<"\n";
    out<<"SMproc = #YYYYYYYYYY#"<<"\n";
    out<<"PMhydr = #YYYYYY#"<<"\n";
    out<<"PMderv = #YYY#"<<"\n";
    out<<"PMproc = #YYYYYYYYYY#"<<"\n";
    out<<"SHhydr = #YYYY#"<<"\n";
    out<<"SHderv = #YYYYY#"<<"\n";
    out<<"SHproc = #YYYYYYYYYY#"<<"\n";
    out<<"SHflux = #YYYY#"<<"\n";
    out<<"PHhydr = #YYYYYY#"<<"\n";
    out<<"PHderv = #YYY#"<<"\n";
    out<<"PHproc = #YYYYYYYYYY#"<<"\n";
    out<<"PHflux = #YYYY#"<<"\n";
    out<<"Online = #N#"<<"\n";
    out<<"Wagmod = #N#"<<"\n";
    out<<"Flmap  =  0.0 "<<simTime<<" "<<simTime<<"\n";
    out<<"Flhis  =  0.0 "<<simTime<<" "<<simTime<<"\n";
    out<<"Flpp   =  0.0 "<<simTime<<" "<<simTime<<"\n";
    out<<"Flrst  = "<<simTime<<"\n";
    out<<"Commnt = "<<"\n";
    out<<"Commnt = "<<"\n";

    fout.close();
}

void Delft3DIO::writeOBS()
{
    QFile fout(qsObs);
    fout.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&fout);

    out<<"("<<xObsCoords[0]<<","<<yObsCoords[0]<<")               "<<xObsCoords[0]<<"     "<<yObsCoords[0]<<"\n";
    out<<"("<<xObsCoords[1]<<","<<yObsCoords[1]<<")               "<<xObsCoords[1]<<"     "<<yObsCoords[1]<<"\n";
    out<<"("<<xObsCoords[2]<<","<<yObsCoords[2]<<")               "<<xObsCoords[2]<<"     "<<yObsCoords[2]<<"\n";

    fout.close();
}

void Delft3DIO::writeOutputMacro()
{
    QFile fout(qsMacro);
    fout.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&fout);

    out<<"d3d_qp('openfile', '"+qsInputs+"/Delft3D/trim-"<<qsFloodName<<".dat')\n";
    out<<"d3d_qp('selectfield','depth averaged velocity')\n";
    out<<"d3d_qp('component','x component')\n";
    out<<"d3d_qp('exporttype','sample file')\n";
    out<<"d3d_qp('exportdata','"+qsInputs+"/Hydraulics/Xvelocity.xyz')\n";

    out<<"d3d_qp('selectfield','depth averaged velocity')\n";
    out<<"d3d_qp('component','y component')\n";
    out<<"d3d_qp('exporttype','sample file')\n";
    out<<"d3d_qp('exportdata','"<<qsInputs+"/Hydraulics/Yvelocity.xyz')\n";

    out<<"d3d_qp('selectfield','water depth')\n";
    out<<"d3d_qp('exporttype','sample file')\n";
    out<<"d3d_qp('exportdata','"+qsInputs+"/Hydraulics/WaterDepth.xyz')\n";

    out<<"d3d_qp('selectfield','bed shear stress')\n";
    out<<"d3d_qp('exporttype','sample file')\n";
    out<<"d3d_qp('exportdata','"+qsInputs+"/Hydraulics/BedShearStress.xyz')\n";

    fout.close();
}

void Delft3DIO::writeSRC()
{
    QFile fout(qsSrc);
    fout.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&fout);

    for (int i=0; i<disCount; i++)
    {
        out<<"("<<xUSCoords[i]<<","<<yUSCoords[i]<<")              Y     "<<xUSCoords[i]<<"     "<<yUSCoords[i]<<"     "<<"0    N\n";
    }
    fout.close();
}

void Delft3DIO::writeXYZ()
{
    double dMax, dInvertValue;
    double xtlCenter, ytlCenter, xCenter, yCenter;

    dMax = findMax(pExtendedDEM);
    xtlCenter = xTopLeft + (cellWidth/2);
    ytlCenter = yTopLeft + (cellHeight/2);

    float *read = (float*) CPLMalloc(sizeof(float)*1);

    QFile fout(qsXyz);
    fout.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&fout);
    out.setRealNumberNotation(QTextStream::FixedNotation);

    for (int i=0; i<nRowsExt; i++)
    {
        yCenter = ytlCenter + (i*cellHeight);
        for (int j=0; j<nColsExt; j++)
        {
            pExtendedDEM->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,read,1,1,GDT_Float32,0,0);
            if (*read != fNoDataValue)
            {
                dInvertValue = (*read - dMax) * (-1);
                xCenter = xtlCenter + (j*cellWidth);
                out.setRealNumberPrecision(5);
                out << xCenter << "\t" <<yCenter << "\t" << dInvertValue <<"\n";
            }
        }
    }
    fout.close();
    CPLFree(read);
    read = NULL;
}

int Delft3DIO::xCellAddress(double coord)
{
    int address;
    address = round((coord - xOrigin) / cellWidth);
    return address;
}

int Delft3DIO::yCellAddress(double coord)
{
    int address;
    address = round((coord - yOrigin) / cellWidth);
    return address;
}

}
