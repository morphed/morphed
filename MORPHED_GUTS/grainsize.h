#ifndef GRAINSIZE_H
#define GRAINSIZE_H

#include "surfaces.h"

namespace MORPH{

class GrainSize : public Surfaces
{
    friend class SedimentTransport;
private:
protected:
    GDALDataset *pDeposition, *pMain, *pActive, *pActiveOrig, *pSubOrig, *pMainVRT, *pMainTemp;
    QVector<double> bandMinElev;
    QVector<int> bandNumber;

    int type, activeType, subSurf, subType, bandCount;
    double activeSize, subSize, thickness, demMax, demMin, minElev, maxElev;
    float *volume, *grainsize, *actVal, *subVal, *elev, *depth, *actBand;
    QString qsActPath, qsSubPath, qsDepoPath, qsMainPath, qsMainPathVRT, qsMainTemp, qsActTempPath;

public:
    GrainSize(XMLReadWrite &XmlObj);
    ~GrainSize();

    void load();

    void addBandBottom();
    void addBandTop();
    void addDeposition(int row, int col, double grainSize, double volume);
    void closeGsDatasets();
    double erodeMultipleLayers(int row, int col, double erodeDepth);
    int findBand(double elev);
    double findElev(int band);
    void openGSDatasets();
    void populateLayers();
    void setupCell(int row, int col);
    void setupMain();
    void updateLayers(GDALDataset *oldDS);
};

}

#endif // GRAINSIZE_H
