#ifndef DELFT3DIO_H
#define DELFT3DIO_H

#include "surfaces.h"
#include "unistd.h"
#include <cstdlib>

namespace MORPH{

class MORPHED_GUTSSHARED_EXPORT Delft3DIO : public Surfaces
{
private:
    int nRowsExt, nColsExt;
    double extTransform[6];
    double usx1, usx2, usy1, usy2, dsx1, dsx2, dsy1, dsy2;
    double simTime, roughness, timeStep, HEV, xLowerLeft, yLowerLeft, xOrigin, yOrigin, origDemMax;
    int nusx1, nusx2, nusy1, nusy2, ndsx1, ndsx2, ndsy1, ndsy2;
    int xstart, xend, ystart, yend, disCount;
    QString delftPath, qsDemPath;
    QString qsBnd, qsBcq, qsDep, qsDis, qsEnc, qsFil, qsGrd, qsIni, qsMdf, qsObs, qsMacro, qsSrc, qsXyz;
    QVector<int> xUSCoords, yUSCoords, xObsCoords, yObsCoords, dsCoords;
    GDALDataset *pExtendedDEM;

public:
    static const int nAddCells = 20;
    static const double DELFT_NODATA = -999.000;

    Delft3DIO(QString xmlPath);
    Delft3DIO(XMLReadWrite &XmlObj);
    ~Delft3DIO();

    void setDelftData();

    void closeDelftDatasets();
    void extendDEMBoundary();
    void run(QThread *thread);
    void runFlow();
    void runQuickplot();
    void setBoundaries();
    void setOriginPoint();
    void setOutputPaths();
    void setXDischargeAddresses();
    void setYDischargeAddresses();
    void writeBND();
    void writeBCQ();
    void writeDEP();
    void writeDIS();
    void writeENC();
    void writeFIL();
    void writeGRD();
    void writeINI();
    void writeMDF();
    void writeOBS();
    void writeOutputMacro();
    void writeSRC();
    void writeXYZ();
    int xCellAddress(double coord);
    int yCellAddress(double coord);
};
}

#endif // DELFT3DIO_H
