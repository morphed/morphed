#include "hydraulics.h"

namespace MORPH{

Hydraulics::Hydraulics(QString xmlPath):MORPHEDBaseClass(xmlPath)
{
}

Hydraulics::Hydraulics(XMLReadWrite &XmlObj):MORPHEDBaseClass(XmlObj)
{

}

Hydraulics::~Hydraulics()
{
    if (pXvel != NULL)
    {
        GDALClose(pXvel);
    }
    if (pYvel != NULL)
    {
        GDALClose(pYvel);
    }
    if (pShear != NULL)
    {
        GDALClose(pShear);
    }
    if (pDepth != NULL)
    {
        GDALClose(pDepth);
    }
    if (pFlowDir != NULL)
    {
        GDALClose(pFlowDir);
    }
}

void Hydraulics::setData()
{
    loadDrivers();
    qsXvel = qsTempDir + "/XVelocity.tif";
    qsYvel = qsTempDir + "/YVelocity.tif";
    qsShear = qsTempDir + "/BedShearStress.tif";
    qsDepth = qsTempDir + "/WaterDepth.tif";
    qsFlowDir = qsTempDir + "/flowdir.tif";
    pXvel = loadDatasetFromXYZ(qsXvel, qsInputs + "/Hydraulics/Xvelocity.xyz");
    pYvel = loadDatasetFromXYZ(qsYvel, qsInputs +"/Hydraulics/Yvelocity.xyz");
    pShear = loadDatasetFromXYZ(qsShear, qsInputs +"/Hydraulics/BedShearStress.xyz");
    pDepth = loadDatasetFromXYZ(qsDepth, qsInputs +"/Hydraulics/WaterDepth.xyz");
    char **papszOptions = NULL;
    pFlowDir = pDriverTIFF->Create(qsFlowDir.toStdString().c_str(),nCols, nRows, 3, GDT_Float32, papszOptions);
    qDebug()<<"Hydro data set";
}

QVector<double> Hydraulics::calculateBraidIndex(GDALRasterBand *pDepthBand)
{
    QVector<double> qvBraid(3);
    GDALRasterBand *pDEMband = pInitialSurface->GetRasterBand(1);

    int nChannels=0, nIslands=0, nDry=0, nWet=0, nChangeW2D=0, nChangeD2W=0, added = 0, nPrevChannels, nConDifVal, nConfluences = 0, nDiffluences = 0;
    bool bWet=false, bDry=false, bAdded = false;
    double index;

    if ((nDirUSbound == 1 || nDirUSbound == 2) && (nDirDSbound == 1 || nDirDSbound == 2))
    {
        qDebug()<<"running braid index";
        float *demRow = (float*)CPLMalloc(sizeof(float)*nCols);
        float *depRow = (float*)CPLMalloc(sizeof(float)*nCols);

        for (int i=1; i<nRows-1; i++)
        {
            pDEMband->RasterIO(GF_Read,0,i,nCols,1,demRow,nCols,1,GDT_Float32,0,0);
            pDepthBand->RasterIO(GF_Read,0,i,nCols,1,depRow,nCols,1,GDT_Float32,0,0);

            nDry = 0, nChangeD2W = 0, nChangeW2D = 0, added = 0;
            bWet = false, bDry = false, bAdded = false;

            for (int j=0; j<nCols; j++)
            {
                    if (depRow[j] > 0.0 && demRow[j] != fNoDataValue)
                    {
                        //qDebug()<<"wet "<<j<<" "<<depRow[j];
                        if (bDry)
                        {
                            bDry = false;
                            bAdded = false;
                            nDry = 0;
                        }
                        bWet = true;
                        nWet++;

                        if (nWet > 1 &&!bAdded)
                        {
                            bAdded = true;
                            nChannels++;
                            nChangeD2W++;
                            added++;
                            //qDebug()<<"new Channel added: channels "<<added;
                        }
                    }
                    else if (depRow[j] <= 0.0 || demRow[j] == fNoDataValue)
                    {

                        if (demRow[j] == fNoDataValue)
                        {
                            //qDebug()<<"nodata "<<j<<" "<<depRow[j];
                        }
                        else
                        {
                            //qDebug()<<"dry "<<j<<" "<<depRow[j];
                        }

                        if (bWet)
                        {
                            bWet = false;
                            bAdded = false;
                            nWet = 0;
                        }
                        bDry = true;
                        nDry++;

                        if (nDry > 2 && !bAdded)
                        {
                            bAdded = true;
                            nChangeW2D++;
                            nIslands++;
                        }
                }
            }

            //qDebug()<<added<<" channels at row "<<i;

            if (i != 1 && nPrevChannels > 0)
            {
                nConDifVal = added - nPrevChannels;

                if (nConDifVal > 0)
                {
                    nDiffluences += nConDifVal;
                }
                else if (nConDifVal < 0)
                {
                    nConfluences += abs(nConDifVal);
                }
            }

            nPrevChannels = added;
        }
        //qDebug()<<"channels "<<nChannels;
        index = nChannels*1.0 / nRows;
        qvBraid[0] = index;
        qvBraid[1] = nConfluences;
        qvBraid[2] = nDiffluences;

        CPLFree(demRow);
        CPLFree(depRow);
        demRow = NULL;
        depRow = NULL;
    }
    else
    {

    }
    return qvBraid;
}

void Hydraulics::calculateFlowDirection()
{
    double angleDegree;
    float* xvel = (float*) CPLMalloc(sizeof(float)*1);
    float* yvel = (float*) CPLMalloc(sizeof(float)*1);
    float* fdir = (float*) CPLMalloc(sizeof(float)*1);
    float* fmag = (float*) CPLMalloc(sizeof(float)*1);

    pFlowDir->SetGeoTransform(transform);
    pFlowDir->GetRasterBand(1)->Fill(0.0);
    pFlowDir->GetRasterBand(1)->SetNoDataValue(0.0);

    for (int i=0; i<nRows; i++)
    {
        for (int j=0; j<nCols; j++)
        {
            pXvel->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,xvel,1,1,GDT_Float32,0,0);
            pYvel->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,yvel,1,1,GDT_Float32,0,0);
            if (*xvel != fNoDataValue && *yvel != fNoDataValue)
            {

                angleDegree = qAtan(fabs(*xvel / *yvel)) * (180.0/PI);
                *fmag = sqrtf(fabs(pow(*xvel,2)+pow(*yvel,2)));

                if(*xvel > 0.0 && *yvel > 0.0)
                {
                    *fdir = 0.0 + angleDegree;
                }
                else if (*yvel < 0.0 && *xvel > 0.0)
                {
                    *fdir = 180.0 - angleDegree;
                }
                else if (*xvel < 0.0 && *yvel < 0.0)
                {
                    *fdir = 180.0 + angleDegree;
                }
                else if (*xvel < 0.0 && *yvel > 0.0)
                {
                    *fdir = 360.0 - angleDegree;
                }
                else if (*xvel > 0.0 && *yvel == 0.0)
                {
                    *fdir = 270.0;
                }
                else if (*xvel < 0.0 && *yvel == 0.0)
                {
                    *fdir = 90.0;
                }
                else if (*xvel == 0.0 && *yvel > 0.0)
                {
                    *fdir = 360.0;
                }
                else if (*xvel == 0.0 && *yvel < 0.0)
                {
                    *fdir = 180.0;
                }
                else if (*xvel == 0.0 && *yvel == 0.0)
                {
                    *fdir = 0.0;
                }
                else
                {
                    qDebug()<<"Flow direction velocity assignment error "<<i<<" "<<j;
                }

                if (*fdir > 360.0 || *fdir < 0.0)
                {
                    qDebug()<<"Flow direction range error "<<i<<" "<<j<<" "<<*fdir;
                }
                else
                {
                    pFlowDir->GetRasterBand(1)->RasterIO(GF_Write,j,i,1,1,fdir,1,1,GDT_Float32,0,0);
                    pFlowDir->GetRasterBand(2)->RasterIO(GF_Write,j,i,1,1,fmag,1,1,GDT_Float32,0,0);
                }
            }
        }
    }
    CPLFree(xvel);
    CPLFree(yvel);
    CPLFree(fdir);
    CPLFree(fmag);
    xvel = NULL;
    yvel = NULL;
    fdir = NULL;
    fmag = NULL;
    qDebug()<<"Flow direction calculated";
}

void Hydraulics::calculateFlowDirection5x5()
{
    double flowAve, angleDegree;
    //double weight[25] = {0.25,0.25,0.25,0.25,0.25,0.25,0.5,0.5,0.5,0.25,0.25,0.5,1.0,0.5,0.25,0.25,0.5,0.5,0.5,0.25,0.25,0.25,0.25,0.25,0.25};
    int flowCell;
    double sinSum, cosSum;
    GDALRasterBand *pXvelB, *pYvelB;
    pXvelB = pXvel->GetRasterBand(1);
    pYvelB = pYvel->GetRasterBand(1);

    float *depVal = (float*) CPLMalloc(sizeof(float)*1);
    float *fdirVal = (float*) CPLMalloc(sizeof(float)*1);
    float *xVal = (float*) CPLMalloc(sizeof(float)*25);
    float *yVal = (float*) CPLMalloc(sizeof(float)*25);

    for(int row=0; row<nRows; row++)
    {
        for(int col=0; col<nCols; col++)
        {
            pDepth->GetRasterBand(1)->RasterIO(GF_Read,col,row,1,1,depVal,1,1,GDT_Float32,0,0);
            if (*depVal > 0.0)
            {
                if (row>1 && col>1 && row<nRows-2 && col<nCols-2)
                {
                    pXvelB->RasterIO(GF_Read,col-2,row-2,5,5,xVal,5,5,GDT_Float32,0,0);
                    pYvelB->RasterIO(GF_Read,col-2,row-2,5,5,yVal,5,5,GDT_Float32,0,0);
                    sinSum = 0.0, cosSum = 0.0, flowCell = 0;
                    for (int i=0; i<25; i++)
                    {
                        if (xVal[i] != 0.0 || yVal[i] != 0.0)
                        {
                            cosSum += xVal[i];// * weight[i];
                            sinSum += yVal[i];// * weight[i];
                            flowCell++;
                        }
                    }
                    if (flowCell > 5)
                    {
                        angleDegree = qAtan(fabs(cosSum / sinSum)) * (180.0/Hydraulics::PI);

                        if(cosSum > 0.0 && sinSum > 0.0)
                        {
                            flowAve = 0.0 + angleDegree;
                        }
                        else if (sinSum < 0.0 && cosSum > 0.0)
                        {
                            flowAve = 180.0 - angleDegree;
                        }
                        else if (cosSum < 0.0 && sinSum < 0.0)
                        {
                            flowAve = 180.0 + angleDegree;
                        }
                        else if (cosSum < 0.0 && sinSum > 0.0)
                        {
                            flowAve = 360.0 - angleDegree;
                        }
                        else if (cosSum > 0.0 && sinSum == 0.0)
                        {
                            flowAve = 270.0;
                        }
                        else if (cosSum < 0.0 && sinSum == 0.0)
                        {
                            flowAve = 90.0;
                        }
                        else if (cosSum == 0.0 && sinSum > 0.0)
                        {
                            flowAve = 360.0;
                        }
                        else if (cosSum == 0.0 && sinSum < 0.0)
                        {
                            flowAve = 180.0;
                        }
                        else
                        {
                            qDebug()<<"error in 5x5 flow assign, flow direction invalid";
                        }
                    }
                    else
                    {
                        flowAve = 0.0;
                    }
                }
                else
                {
                    pFlowDir->GetRasterBand(1)->RasterIO(GF_Read,col,row,1,1,fdirVal,1,1,GDT_Float32,0,0);
                    flowAve = *fdirVal;
                }
            }
            else
            {
                flowAve = 0.0;
            }
            *fdirVal = flowAve;
            pFlowDir->GetRasterBand(3)->RasterIO(GF_Write,col,row,1,1,fdirVal,1,1,GDT_Float32,0,0);
        }
    }

    CPLFree(depVal);
    CPLFree(xVal);
    CPLFree(yVal);
    depVal = NULL;
    xVal = NULL;
    yVal = NULL;
}

void Hydraulics::calculateFlowMagnitude()
{

}

void Hydraulics::closeDatasets()
{
    if (pXvel != NULL)
    {
        GDALClose(pXvel);
    }
    if (pYvel != NULL)
    {
        GDALClose(pYvel);
    }
    if (pShear != NULL)
    {
        GDALClose(pShear);
    }
    if (pDepth != NULL)
    {
        GDALClose(pDepth);
    }
    if (pFlowDir != NULL)
    {
        GDALClose(pFlowDir);
    }
}

void Hydraulics::thresholdWaterDepth()
{
    float* value = (float*) CPLMalloc(sizeof(float)*1);
    for (int i=0; i<pDepth->GetRasterBand(1)->GetYSize(); i++)
    {
        for (int j=0; j<pDepth->GetRasterBand(1)->GetXSize(); j++)
        {
            pDepth->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,value,1,1,GDT_Float32,0,0);
            if (*value < WD_THRESH)
            {
                *value = 0.0;
                pDepth->GetRasterBand(1)->RasterIO(GF_Write,j,i,1,1,value,1,1,GDT_Float32,0,0);
            }
        }
    }
    CPLFree(value);
    value = NULL;
    qDebug()<<"Water depth thresholded";
}


}
