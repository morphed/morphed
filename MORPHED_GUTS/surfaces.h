#ifndef SURFACES_H
#define SURFACES_H

#include "morphedbaseclass.h"

namespace MORPH{

class Surfaces : public MORPHEDBaseClass
{
private:
protected:
    float *oldElev, *newElev;

public:
    GDALDataset *pNewSurface, *pOldSurface, *pCumDoD, *pRecentDoD, *pSlope, *pAspect, *pDetrend;
    QString qsOldPath, qsNewPath, qsSlopePath, qsCumDoDPath, qsRecentDoDPath, qsAspectPath, qsDetrendPath;

    Surfaces(QString xmlPath);
    Surfaces(XMLReadWrite &XmlObj);
    ~Surfaces();

    void setData();

    void calculateAspect();
    double calculateAverageSlope_ColAve(GDALDataset *pDataset, int col1, int col2);
    double calculateAverageSlope_ColMin(GDALDataset *pDataset, int col1, int col2);
    double calculateAverageSlope_RowAve(GDALDataset *pDataset, int row1, int row2);
    double calculateAverageSlope_RowMin(GDALDataset *pDataset, int row1, int row2);
    double calculateColAverage(GDALDataset *pDataset, int col);
    GDALDataset* calculateDoDCumulative(QString qsDodPath);
    GDALDataset* calculateDoDRecent(QString qsDodPath);
    double calculateRowAverage(int row);
    double calculateRowAverage(GDALDataset *pDataset, int row);
    void calculateSlope(QString qsSlopePath);
    void calculateSlopeThirdOrderFinite(GDALDataset *pDemDS);
    void closeDatasets();
    void closeDEMs();
    void copyDSBoundary();
    void detrendDEM(GDALDataset *dem, Direction dirUS, Direction dirDS, QString detrendPath);
    double findColMin(GDALDataset *pDataset, int col);
    QVector<int> findLowestNeighbor(int row, int col, double &amount);
    double findRowMin(GDALDataset *pDataset, int row);
    GDALDataset* getOldSurface();
    void loadNewSurface();
    void loadNewSurface(QString qsSurfacePath);
    void loadOldSurface(GDALDataset *pCopyDS);
    void loadOldSurface(QString qsSurfacePath);
    void recalcSlope3x3TOF(GDALDataset *pDemDS, int row, int col);
    void setNewSurface(GDALDataset *pDataset);

    static void calculateDoD(GDALDataset *oldSurface, GDALDataset *newSurface, GDALDataset *dodDataset);
    static void filterHighPass(GDALDataset *pOldDS, GDALDataset *pTargetDS);
    static void filterLowPass(GDALDataset *pOldDS, GDALRasterBand *pTargetBand);
};

}

#endif // SURFACES_H
