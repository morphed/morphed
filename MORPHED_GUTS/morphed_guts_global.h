#ifndef MORPHED_GUTS_GLOBAL_H
#define MORPHED_GUTS_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(MORPHED_GUTS_LIBRARY)
#  define MORPHED_GUTSSHARED_EXPORT Q_DECL_EXPORT
#else
#  define MORPHED_GUTSSHARED_EXPORT Q_DECL_IMPORT
#endif

//#ifndef MY_DLL_EXPORT
//# define DLL_API __declspec(dllexport)
//#else
//# define DLL_API __declspec(dllimport)
//#endif

#endif // MORPHED_GUTS_GLOBAL_H
