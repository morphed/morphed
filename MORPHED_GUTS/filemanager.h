#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QtCore>
#include "morphed_guts_global.h"
#include "gdal_priv.h"

namespace MORPH
{
class MORPHED_GUTSSHARED_EXPORT FileManager
{
public:
    FileManager();

    static QString copyFileToDirectory(QString originalPath, QString newDirectory, QString newFileName);
    static void createBaseDirectories(QString rootDir);
    static void createDirectory(QString rootDir, QString dirName);
    static QString createFloodDirectories(QString rootDir, int flood);
    static QString getFloodPath(int flood);
    static void printTIFFDataset(QString filename, GDALDataset *pDS);
};
}

#endif // FILEMANAGER_H
