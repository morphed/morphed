#include "morphedmodel.h"

namespace MORPH{

MORPHEDModel::MORPHEDModel(XMLReadWrite &XmlObj, QObject *parent) :
    QObject(parent)
{
    XmlMorph = XmlObj;
    qsRootDir = XmlMorph.readNodeData("ProjectDirectory");
    FileManager::createBaseDirectories(qsRootDir);
    DelftManager = new Delft3DIO(XmlMorph);
    SedimentManager = new SedimentTransport(XmlMorph);

    iterations = XmlMorph.readNodeData("Inputs", "ModelIterations").toInt();
    currentIteration = 0;
    QString projdir, name;
    QStringList list;

    projdir = XmlMorph.readNodeData("ProjectDirectory");
    list = projdir.split("/");
    name = projdir + "/" + list[list.size()-1] + ".display.morph";

    XmlOut.writeXMLdocViewer(iterations);
    XmlOut.setDocumentFilename(name);
    XmlOut.printXML();
}

MORPHEDModel::~MORPHEDModel()
{
    qDebug()<<"Model destructor";
    delete DelftManager;
    delete SedimentManager;
}

void MORPHEDModel::copyInputFiles()
{
    QString newName;

    qsFloodDir = FileManager::createFloodDirectories(qsRootDir, currentIteration+1);
    newName = FileManager::copyFileToDirectory(XmlMorph.readNodeData("OriginalDEMPath"), XmlMorph.readNodeData("ProjectDirectory")+"/Inputs/01_InitialInputs","InitialDEM");
    XmlMorph.writeNodeData("Inputs","DEMPath",newName);
    newName = FileManager::copyFileToDirectory(XmlMorph.readNodeData("OriginalHydroSediPath"), XmlMorph.readNodeData("ProjectDirectory")+"/Inputs/01_InitialInputs","InitialHydroSedi");
    XmlMorph.writeNodeData("Inputs","HydroSediPath",newName);
    QFile file(newName);
    if (file.exists())
    {
        XmlOut.writeNodeData("InputHydroSedi", "Inputs/01_InitialInputs/InitialHydroSedi.txt");
    }
    if (!XmlMorph.readNodeData("OriginalGrainSizeActivePath").isNull() && !XmlMorph.readNodeData("OriginalGrainSizeActivePath").isEmpty())
    {
        newName = FileManager::copyFileToDirectory(XmlMorph.readNodeData("OriginalGrainSizeActivePath"), XmlMorph.readNodeData("ProjectDirectory")+"/Inputs/01_InitialInputs","IntialActiveGS");
        if (!newName.isNull() && !newName.isEmpty())
        {
            XmlMorph.writeNodeData("Inputs","GrainSize","ActiveRasterPath",newName);
        }
    }
    if (!XmlMorph.readNodeData("OriginalGrainSizeSubSurfPath").isNull() && !XmlMorph.readNodeData("OriginalGrainSizeSubSurfPath").isEmpty())
    {
        newName = FileManager::copyFileToDirectory(XmlMorph.readNodeData("OriginalGrainSizeSubSurfPath"), XmlMorph.readNodeData("ProjectDirectory")+"/Inputs/01_InitialInputs","InitialSubSurfGS");
        if (!newName.isNull() && !newName.isEmpty())
        {
            XmlMorph.writeNodeData("Inputs","GrainSize","SubSurfRasterPath",newName);
        }
    }
    if (!XmlMorph.readNodeData("OriginalCustomPath").isNull() && !XmlMorph.readNodeData("OriginalCustomPath").isEmpty())
    {
            newName = FileManager::copyFileToDirectory(XmlMorph.readNodeData("OriginalCustomPath"), XmlMorph.readNodeData("ProjectDirectory")+"/Inputs/01_InitialInputs","CustomPL");
    }
}

void MORPHEDModel::increment()
{
    currentIteration++;
    //DelftManager->incrementCurrentIteration();
    //HydroData->incrementCurrentIteration();
    //SedimentManager->incrementCurrentIteration();
}

void MORPHEDModel::run()
{
    FileManager::createBaseDirectories(qsRootDir);
}

void MORPHEDModel::setup(QThread &thread)
{
    connect(&thread, SIGNAL(started()), this, SLOT(runOnThread()));
}

void MORPHEDModel::writeFloodOutputs()
{
    qDebug()<<"writing outputs";
    QString qsDoDRec, qsDoDCum, qsWaterDepth, qsNewDem, qsNewCohesion, floodName;
    floodName = "Flood"+QString::number(SedimentManager->nCurrentIteration+1);
    qsDoDRec = SedimentManager->qsOutputs + "/GTIFF/DoD_Recent" + QString::number(SedimentManager->nCurrentIteration +1) +".tif";
    qsDoDCum = SedimentManager->qsOutputs + "/GTIFF/DoD_Cumulative" + QString::number(SedimentManager->nCurrentIteration +1) +".tif";
    qsWaterDepth = SedimentManager->qsOutputs + "/GTIFF/WaterDepth" + QString::number(SedimentManager->nCurrentIteration +1) +".tif";
    qsNewDem = SedimentManager->qsOutputs + "/GTIFF/DEM" + QString::number(SedimentManager->nCurrentIteration +1) +".tif";
    qsNewCohesion = SedimentManager->qsOutputs + "/GTIFF/Cohesion" + QString::number(SedimentManager->nCurrentIteration +1) +".tif";

    GDALDataset *pTempDS, *pDodDS;

    qDebug()<<"water depth";
    pTempDS = SedimentManager->pDriverTIFF->CreateCopy(qsWaterDepth.toStdString().c_str(),SedimentManager->pDepthDS,FALSE, NULL, NULL, NULL);
    pTempDS->SetGeoTransform(SedimentManager->transform);
    qsWaterDepth = SedimentManager->qsOutputs + "/PNG/WaterDepth" + QString::number(SedimentManager->nCurrentIteration +1) +".png";
    Symbology::symbolizeWaterDepthasPNG(pTempDS,qsWaterDepth,SedimentManager->qsTempDir);
    qDebug()<<"closing gdal dataset";
    GDALClose(pTempDS);
    pTempDS = NULL;
    qDebug()<<"closed";
    qsWaterDepth = "Outputs/" + SedimentManager->qsFloodName + "/PNG/WaterDepth" + QString::number(SedimentManager->nCurrentIteration +1) +".tif";
    XmlOut.writeNodeData(floodName,"WaterDepthPath", qsWaterDepth);

    qDebug()<<"hillshade";
    pTempDS = SedimentManager->pDriverTIFF->CreateCopy(qsNewDem.toStdString().c_str(),SedimentManager->pNewSurface,FALSE, NULL, NULL, NULL);
    pTempDS->SetGeoTransform(SedimentManager->transform);
    qsNewDem = SedimentManager->qsOutputs + "/PNG/DEM" + QString::number(SedimentManager->nCurrentIteration +1) +".png";
    Symbology::symbolizeDEMasPNG(pTempDS,qsNewDem,SedimentManager->qsTempDir);
    qsNewDem = "Outputs/" + SedimentManager->qsFloodName + "/PNG/DEM" + QString::number(SedimentManager->nCurrentIteration +1) +".tif";
    XmlOut.writeNodeData(floodName,"DEMPath", qsNewDem);

    qsNewDem = SedimentManager->qsOutputs + "/PNG/hlsd" + QString::number(SedimentManager->nCurrentIteration +1) +".png";
    Symbology::createHillshadeAsPNG(pTempDS,qsNewDem,SedimentManager->qsTempDir);
    qDebug()<<"closing gdal dataset";
    GDALClose(pTempDS);
    pTempDS = NULL;
    qDebug()<<"closed";
    qsNewDem = "Outputs/" + SedimentManager->qsFloodName + "/PNG/hlsd" + QString::number(SedimentManager->nCurrentIteration +1) +".png";
    XmlOut.writeNodeData(floodName,"HillshadePath", qsNewDem);

    qDebug()<<"dod cum";
    pDodDS = SedimentManager->pDriverTIFF->Create(qsDoDCum.toStdString().c_str(),SedimentManager->nCols, SedimentManager->nRows, 1, GDT_Float32,NULL);
    SedimentManager->calculateDoD(SedimentManager->pInitialSurface, SedimentManager->pNewSurface, pDodDS);
    qsDoDCum = SedimentManager->qsOutputs + "/PNG/DoD_Cumulative" + QString::number(SedimentManager->nCurrentIteration +1) +".png";
    Symbology::symbolizeDoDasPNG(pDodDS,qsDoDCum,SedimentManager->qsTempDir);
    qDebug()<<"closing gdal dataset";
    GDALClose(pDodDS);
    pDodDS = NULL;
    qDebug()<<"closed";
    //GDALClose(pTempDS);
    //pTempDS = NULL;
    qsDoDCum = "Outputs/" + SedimentManager->qsFloodName + "/PNG/DoD_Cumulative" + QString::number(SedimentManager->nCurrentIteration +1) +".png";
    XmlOut.writeNodeData(floodName,"DoDCumulativePath", qsDoDCum);

    qDebug()<<"dod recent";
    pDodDS = SedimentManager->pDriverTIFF->Create(qsDoDRec.toStdString().c_str(),SedimentManager->nCols, SedimentManager->nRows, 1, GDT_Float32,NULL);
    SedimentManager->calculateDoD(SedimentManager->pOldSurface, SedimentManager->pNewSurface, pDodDS);
    qDebug()<<"closing gdal dataset";
    GDALClose(pDodDS);
    pDodDS = NULL;
    qDebug()<<"closed";
    qsDoDRec = SedimentManager->qsOutputs + "/PNG/DoD_Recent" + QString::number(SedimentManager->nCurrentIteration +1) +".png";

    qDebug()<<"xml";
    XmlOut.writeNodeData(floodName,"Discharge", QString::number(SedimentManager->q[SedimentManager->nCurrentIteration]));
    XmlOut.writeNodeData(floodName,"ExportedSediment","Event",QString::number(SedimentManager->exported));
    XmlOut.writeNodeData(floodName,"ExportedSediment","Total",QString::number(SedimentManager->exportedTotal));
    XmlOut.writeNodeData(floodName,"ImportedSediment","Event",QString::number(SedimentManager->imported));
    XmlOut.writeNodeData(floodName,"ImportedSediment","Total",QString::number(SedimentManager->importedTotal));
    XmlOut.writeNodeData(floodName,"BedErosion","Event",QString::number(SedimentManager->bedErode));
    XmlOut.writeNodeData(floodName,"BedErosion","Total",QString::number(SedimentManager->bedErodeTotal));
    XmlOut.writeNodeData(floodName,"BedDeposition","Event",QString::number(SedimentManager->bedDepo));
    XmlOut.writeNodeData(floodName,"BedDeposition","Total",QString::number(SedimentManager->bedDepoTotal));
    XmlOut.writeNodeData(floodName,"SloughErosion","Event",QString::number(SedimentManager->bankErode));
    XmlOut.writeNodeData(floodName,"SloughErosion","Total",QString::number(SedimentManager->bankErodeTotal));
    XmlOut.writeNodeData(floodName,"SloughDeposition","Event",QString::number(SedimentManager->bankDepo));
    XmlOut.writeNodeData(floodName,"SloughDeposition","Total",QString::number(SedimentManager->bankDepoTotal));

    qDebug()<<"done";
    XmlOut.printXML();
}

void MORPHEDModel::runOnThread()
{
    QString result;

    for (int i=0; i<iterations; i++)
    {
        copyInputFiles();
        result = "Flood " + QString::number(currentIteration + 1) + " of " + QString::number(iterations) + ":  ";
        if (i>0)
        {
            qDebug()<<"Incrementing class data";
            DelftManager->incrementCurrentIteration();
            qDebug()<<"Delft incremented";
            SedimentManager->updateIteration();
            qDebug()<<"Finished incrementing";
            thread()->sleep(1);
            DelftManager->loadOldSurface(SedimentManager->getOldSurface());
        }
        emit updateStatus(result+"Running Delft3D flow simulation. . . this will take several minutes");
        DelftManager->run(thread());
        emit updateStatus(result+"Delft3D finished running");
        SedimentManager->setupHydraulics();
        emit updateStatus(result+"Running Bed Transport");
        SedimentManager->runBedTransport();
        emit updateStatus(result+"Running Bank Slough");
        SedimentManager->bankSloughing();
        DelftManager->closeDelftDatasets();
        emit updateStatus(result+"Writing Outputs");
        qDebug()<<"Delft datasets closed";
        writeFloodOutputs();
        emit updateStatus(result+"Setting up next flood");
        qDebug()<<"outputs written";
        increment();
    }
    thread()->sleep(1);
    result = "Simulation Finished";
    emit updateStatus(result);
    thread()->sleep(5);
    SedimentManager->closeSedDatasets();
    qDebug()<<"Finished";
}

}
