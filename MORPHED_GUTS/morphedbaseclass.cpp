#include "morphedbaseclass.h"
#include <iostream>

namespace MORPH{

MORPHEDBaseClass::MORPHEDBaseClass()
{
}

MORPHEDBaseClass::MORPHEDBaseClass(QString xmlPath)
{
    init(xmlPath);
}

MORPHEDBaseClass::MORPHEDBaseClass(MORPHEDBaseClass &obj)
{
    init(obj);
}

MORPHEDBaseClass::MORPHEDBaseClass(XMLReadWrite &obj)
{
    XmlInit = obj;
    init();
}

MORPHEDBaseClass::~MORPHEDBaseClass()
{
    if (pInitialSurface != NULL)
    {
        GDALClose(pInitialSurface);
    }
}

void MORPHEDBaseClass::init()
{
    qsXmlPath = XmlInit.getDocumentFilename();

    //get input text file and load data
    qsInputText = XmlInit.readNodeData("Inputs", "HydroSediPath");
    loadInputText();

    //get initial dem path and load data
    qsInitialPath = XmlInit.readNodeData("Inputs","DEMPath");
    pInitialSurface = (GDALDataset*) GDALOpen(qsInitialPath.toStdString().c_str(),GA_ReadOnly);

    //load dem properties from transformation data and xml
    pInitialSurface->GetGeoTransform(transform);
    cellWidth = transform[1];
    cellHeight = transform[5];
    xTopLeft = transform[0];
    yTopLeft = transform[3];
    nRows = XmlInit.readNodeData("DEMProperties", "Rows").toInt();
    nCols = XmlInit.readNodeData("DEMProperties", "Cols").toInt();

    //load other parameters from xml
    nIterations = XmlInit.readNodeData("Inputs", "ModelIterations").toInt();
    nDirUSbound = XmlInit.readNodeData("Delft3DParameters", "USBoundLocation").toInt();
    nDirDSbound = XmlInit.readNodeData("Delft3DParameters", "DSBoundLocation").toInt();
    nImport = XmlInit.readNodeData("Inputs", "ImportType").toInt();
    fNoDataValue = pInitialSurface->GetRasterBand(1)->GetNoDataValue();
    nCurrentIteration = 0;
    qsRootDir = XmlInit.readNodeData("ProjectDirectory");
    qsTempDir = qsRootDir + "/temp";

    setDirection(nDirUSbound, dirUS);
    setDirection(nDirDSbound, dirDS);

    setInOutPaths();
}

void MORPHEDBaseClass::init(QString xmlPath)
{
    //load xml configuration file
    qsXmlPath = xmlPath;
    XmlInit.loadDocument(qsXmlPath,1);

    //get input text file and load data
    qsInputText = XmlInit.readNodeData("Inputs", "HydroSediPath");
    loadInputText();

    //get initial dem path and load data
    qsInitialPath = XmlInit.readNodeData("Inputs","DEMPath");
    pInitialSurface = (GDALDataset*) GDALOpen(qsInitialPath.toStdString().c_str(),GA_ReadOnly);

    //load dem properties from transformation data and xml
    pInitialSurface->GetGeoTransform(transform);
    cellWidth = transform[1];
    cellHeight = transform[5];
    xTopLeft = transform[0];
    yTopLeft = transform[3];
    nRows = XmlInit.readNodeData("DEMProperties", "Rows").toInt();
    nCols = XmlInit.readNodeData("DEMProperties", "Cols").toInt();

    //load other parameters from xml
    nIterations = XmlInit.readNodeData("Inputs", "ModelIterations").toInt();
    nDirUSbound = XmlInit.readNodeData("Delft3DParameters", "USBoundLocation").toInt();
    nDirDSbound = XmlInit.readNodeData("Delft3DParameters", "DSBoundLocation").toInt();
    nImport = XmlInit.readNodeData("Inputs", "ImportType").toInt();
    fNoDataValue = pInitialSurface->GetRasterBand(1)->GetNoDataValue();
    nCurrentIteration = 0;
    qsRootDir = XmlInit.readNodeData("ProjectDirectory");
    qsTempDir = qsRootDir + "/temp";

    setInOutPaths();
}

void MORPHEDBaseClass::init(MORPHEDBaseClass &obj)
{
    //load xml configuration file
    XmlInit.loadDocument(obj.qsXmlPath,1);

    //get input text file and load data
    qsInputText = obj.qsInputText;
    loadInputText();

    //get initial dem path and load data
    qsInitialPath = obj.qsInitialPath;
    pInitialSurface = obj.pInitialSurface;

    //load dem properties from transformation data and xml
    cellWidth = obj.transform[1];
    cellHeight = obj.transform[5];
    xTopLeft = obj.transform[0];
    yTopLeft = obj.transform[3];
    transform[2] = obj.transform[2];
    transform[4] = obj.transform[4];
    nRows = obj.nRows;
    nCols = obj.nCols;

    //load other parameters from xml
    nIterations = obj.nIterations;
    nDirUSbound = obj.nDirUSbound;
    nDirDSbound = obj.nDirDSbound;
    nImport = obj.nImport;
    fNoDataValue = obj.fNoDataValue;
    q = obj.q;
    dswe = obj.dswe;
    date = obj.date;
    import = obj.import;
    nCurrentIteration = obj.nCurrentIteration;
}

void MORPHEDBaseClass::incrementCurrentIteration()
{
    nCurrentIteration++;
    setInOutPaths();
}

GDALDataset *MORPHEDBaseClass::loadDatasetFromXYZ(QString qsRasterPath, QString qsXyzPath)
{
    //declare variables
    int i, j;
    int count = 0;
    double x, y, yTLCenter, xTLCenter;
    double newTransform[6];
    float z;
    char** papszOptions = NULL;
    QString qsDummy, qsX, qsY, qsZ;

    //setup new GDAL raster dataset into which the xyz data will be read
    GDALDataset *pDatasetNew;
    pDatasetNew = pDriverTIFF->Create(qsRasterPath.toStdString().c_str(),nCols,nRows,1, GDT_Float32,papszOptions);
    pInitialSurface->GetGeoTransform(newTransform);
    pDatasetNew->SetGeoTransform(newTransform);
    pDatasetNew->GetRasterBand(1)->Fill(fNoDataValue);

    //set center of top left pixel as the anchor point from which to calculate rows and columns
    xTLCenter = newTransform[0] + (newTransform[1]/2);
    yTLCenter = newTransform[3] + (newTransform[5]/2);

    //allocate memory to read into raster dataset
    float *value = (float*) CPLMalloc(sizeof(float)*1);

    //load file and read xyz data
    QFile in(qsXyzPath);
    if(in.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&in);
        while (!stream.atEnd())
        {
            //don't read the header with column names
            if (count != 0)
            {
                //read first element from the stream as a QString
                stream >> qsX;
                //convert the QString to a double
                x = qsX.toDouble();
                stream >> qsY;
                y = qsY.toDouble();
                stream >> qsZ;
                z = qsZ.toDouble();

                //assign a value to the raster input data
                *value = z;

                //calculate row and column for cell address
                i = (yTLCenter - y)/cellWidth;
                j = (x - xTLCenter)/cellWidth;

                //check that address falls in raster extent
                if(i>=0 && i<nRows && j>=0 && j<nCols)
                {
                    //write data value to raster dataset
                    pDatasetNew->GetRasterBand(1)->RasterIO(GF_Write,j,i,1,1,value,1,1,GDT_Float32,0,0);
                }
                else
                {
                    //std::cout<<"Address "<<i<<" "<<j<<" "<<"out of range"<<std::endl;
                }

                //increment row
                count++;
            }
            //for first row in file containing column headers
            else
            {
                //read header line to dummy variable
                qsDummy = stream.readLine();
                //increment count to first row containing data
                count++;
            }
        }
    }
    pDatasetNew->GetRasterBand(1)->SetNoDataValue(fNoDataValue);
    CPLFree(value);
    value = NULL;
    return pDatasetNew;
}

void MORPHEDBaseClass::loadDrivers()
{
    const char *formatPNG = "PNG";
    const char *formatTIFF = "GTiff";
    const char *formatVRT = "VRT";
    const char *formatIMG = "IMG";
    GDALAllRegister();
    pDriverPNG = GetGDALDriverManager()->GetDriverByName(formatPNG);
    pDriverTIFF = GetGDALDriverManager()->GetDriverByName(formatTIFF);
    pDriverVRT = GetGDALDriverManager()->GetDriverByName(formatVRT);
    pDriverIMG = GetGDALDriverManager()->GetDriverByName(formatIMG);
}

void MORPHEDBaseClass::loadInputText()
{
    //declare temp variables to hold stream data
    QString qsDate, qsQ, qsDSWE, qsImport;
    QDateTime tempDate;

    //load file
    QFile in(qsInputText);
    if (in.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&in);
        while (!stream.atEnd())
        {
            //read elements from stream to temp variable, convert to double, and store in a QVector
            stream >> qsDate;
            tempDate = QDateTime::fromString("MM/dd/yyyy,hh:mm");
            date.append(tempDate.toTime_t());
            stream >> qsQ;
            q.append(qsQ.toDouble());
            stream >> qsDSWE;
            dswe.append(qsDSWE.toDouble());
            stream >> qsImport;
            import.append(qsImport.toDouble());
        }
    }
}

void MORPHEDBaseClass::setInOutPaths()
{
    qsFloodName = FileManager::getFloodPath(nCurrentIteration + 1);
    qsInputs = qsRootDir + "/Inputs/" + qsFloodName;
    qsOutputs = qsRootDir + "/Outputs/" + qsFloodName;
}

int MORPHEDBaseClass::xCoordToCellAddress(double coord)
{
    int address;
    address = round((coord - xTopLeft) / cellWidth);
    return address;
}

int MORPHEDBaseClass::yCoordtoCellAddress(double coord)
{
    int address;
    address = round((coord - yTopLeft) / cellHeight);
    return address;
}

double MORPHEDBaseClass::findMax(GDALDataset *pDataset)
{
    double dMax=0, noData;
    float *value = (float*)CPLMalloc(sizeof(float)*1);
    int maxSet = false;
    noData = pDataset->GetRasterBand(1)->GetNoDataValue();
    for (int i=0; i<pDataset->GetRasterBand(1)->GetYSize(); i++)
    {
        for (int j=0; j<pDataset->GetRasterBand(1)->GetXSize(); j++)
        {
            pDataset->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,value,1,1,GDT_Float32,0,0);
            if (!maxSet && *value!=noData)
            {
                dMax = *value;
                maxSet = true;
            }
            else if (maxSet && *value!=noData)
            {
                if (*value > dMax)
                {
                    dMax = *value;
                }
            }
        }
    }
    CPLFree(value);
    value = NULL;
    return dMax;
}

double MORPHEDBaseClass::findMaxVector(QVector<double> vector, int count)
{
    double max =0;

    for (int i=0; i<count; i++)
    {
        if (max < vector[i])
        {
            max = vector[i];
        }
    }
    return max;
}

double MORPHEDBaseClass::findMin(GDALDataset *pDataset)
{
    double dMin=0, noData;
    float *value = (float*)CPLMalloc(sizeof(float)*1);
    int minSet = false;
    noData = pDataset->GetRasterBand(1)->GetNoDataValue();
    for (int i=0; i<pDataset->GetRasterBand(1)->GetYSize(); i++)
    {
        for (int j=0; j<pDataset->GetRasterBand(1)->GetXSize(); j++)
        {
            pDataset->GetRasterBand(1)->RasterIO(GF_Read,j,i,1,1,value,1,1,GDT_Float32,0,0);
            if (!minSet && *value!=noData && *value>0.0)
            {
                dMin = *value;
                minSet = true;
            }
            else if (minSet && *value!=noData && *value>0.0)
            {
                if (*value < dMin)
                {
                    dMin = *value;
                }
            }
        }
    }
    CPLFree(value);
    value = NULL;
    return dMin;
}

QVector<double> MORPHEDBaseClass::getDistribution(int type, double sigA, double muB, int length, double cellSize)
{
    QVector<double> pathLength;
    bool goodDist = true;
    double factor;
    double sum = 0;

    //exponential
    if (type == 1)
    {
        for (int i=0; i<length; i++)
        {
            pathLength.append((1.0/sqrt(2.0*PI))*exp(((-1.0)*sigA)*muB*(i+1.0)));
            sum += pathLength[i];
        }
    }
    //gaussian
    else if (type == 2)
    {
        sigA /= cellSize;
        muB /= cellSize;
        for (int i=0; i<length; i++)
        {
            pathLength.append((1/(sigA*sqrt(2*PI)))*exp((-0.5)*pow((((i+1)-muB)/sigA),2)));
            sum += pathLength[i];
        }
    }
    //custom
    else if (type == 3)
    {

    }
    else
    {
        goodDist = false;
    }

    if(goodDist)
    {
        factor = 1/sum;
        for (int i=0; i<length; i++)
        {
            pathLength[i] *= factor;
        }
    }
    return pathLength;
}

int MORPHEDBaseClass::getLength(double distance, double cellSize)
{
    int nLength;
    nLength = rint(distance/cellSize);
    return nLength;
}

void MORPHEDBaseClass::loadInputText(QString inputFile, QVector<double> &dateTime, QVector<double> &discharge, QVector<double> &waterElev, QVector<double> &importQuant, int iterations)
{
    //declare temp variables to hold stream data
    QString qsDate, qsQ, qsDSWE, qsImport;
    QDateTime tempDate;
    int count = 0;

    //make sure vectors are empty
    dateTime.clear();
    discharge.clear();
    waterElev.clear();
    importQuant.clear();

    //load file
    QFile in(inputFile);
    if (in.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&in);
        while (!stream.atEnd() && count<iterations)
        {
            //read elements from stream to temp variable, convert to double, and store in a QVector
            stream >> qsDate;
            tempDate = QDateTime::fromString(qsDate,"MM/dd/yyyy,hh:mm");
            dateTime.append(tempDate.toTime_t());
            stream >> qsQ;
            discharge.append(qsQ.toDouble());
            stream >> qsDSWE;
            waterElev.append(qsDSWE.toDouble());
            stream >> qsImport;
            importQuant.append(qsImport.toDouble());
            count++;
        }
    }
    in.close();
}

void MORPHEDBaseClass::setDirection(int dirCode, Direction &dirVar)
{
    if (dirCode == 1)
    {
        dirVar = MDIR_North;
    }
    else if (dirCode == 2)
    {
        dirVar = MDIR_South;
    }
    else if (dirCode == 3)
    {
        dirVar = MDIR_East;
    }
    else if (dirCode == 4)
    {
        dirVar = MDIR_West;
    }
    else
    {

    }
}

}
