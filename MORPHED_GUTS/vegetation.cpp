#include "vegetation.h"

namespace MORPH{

Vegetation::Vegetation(XMLReadWrite &XmlObj):MORPHEDBaseClass(XmlObj)
{
    pCohesion = NULL;
    qsCohesion = qsTempDir + "/cohesion.tif";
}

Vegetation::~Vegetation()
{
    if (pCohesion != NULL)
    {
        GDALClose(pCohesion);
    }
}

void Vegetation::closeVegDatasets()
{
    if (pCohesion != NULL)
    {
        GDALClose(pCohesion);
    }
}


void Vegetation::createInitialCohesion(GDALDataset *pDetrendDS)
{
    qDebug()<<"Starting cohesion assign";
    loadDrivers();
    double min, max, mean, stdev, upper, lower;

    pCohesion = pDriverTIFF->Create(qsCohesion.toStdString().c_str(),nCols,nRows,2,GDT_Float32,NULL);
    qDebug()<<"cohesion tiff created";
    pCohesion->GetRasterBand(1)->SetNoDataValue(fNoDataValue);
    pCohesion->GetRasterBand(2)->SetNoDataValue(fNoDataValue);
    pCohesion->SetGeoTransform(transform);
    qDebug()<<"cohesion set up";

    pDetrendDS->GetRasterBand(1)->GetStatistics(FALSE, TRUE, &min, &max, &mean, &stdev);
    upper = mean + (1*stdev);
    lower = mean - (1*stdev);

    float *dElev = (float*) CPLMalloc(sizeof(float)*nCols);
    float *cVal = (float*) CPLMalloc(sizeof(float)*nCols);
    float *tVal = (float*) CPLMalloc(sizeof(float)*nCols);
    qDebug()<<"starting loop";

    for (int i=0; i<nRows; i++)
    {
        pDetrendDS->GetRasterBand(1)->RasterIO(GF_Read,0,i,nCols,1,dElev,nCols,1,GDT_Float32,0,0);
        for (int j=0; j<nCols; j++)
        {
            if (dElev[j] != fNoDataValue)
            {
                if (dElev[j] > upper)
                {
                    cVal[j] = 3.0;
                }
                else if (dElev[j] < lower)
                {
                    cVal[j] = 1.0;
                }
                else
                {
                    cVal[j] = 2.0;
                }
                tVal[j] = 1.0;
            }
            else
            {
                cVal[j] = fNoDataValue;
                tVal[j] = fNoDataValue;
            }
        }
        pCohesion->GetRasterBand(1)->RasterIO(GF_Write,0,i,nCols,1,cVal,nCols,1,GDT_Float32,0,0);
        pCohesion->GetRasterBand(2)->RasterIO(GF_Write,0,i,nCols,1,tVal,nCols,1,GDT_Float32,0,0);
    }
    qDebug()<<"loop finished";
    CPLFree(dElev);
    dElev = NULL;
    CPLFree(cVal);
    cVal = NULL;
    CPLFree(tVal);
    tVal = NULL;
}

GDALDataset *Vegetation::getCohesionDS()
{
    return pCohesion;
}

void Vegetation::makeBare(int row, int col)
{
    float *value = (float*) CPLMalloc(sizeof(float));
    *value = 1.0;

    pCohesion->GetRasterBand(1)->RasterIO(GF_Write,col,row,1,1,value,1,1,GDT_Float32,0,0);
    *value = 0.0;
    pCohesion->GetRasterBand(2)->RasterIO(GF_Write,col,row,1,1,value,1,1,GDT_Float32,0,0);
}

void Vegetation::openVegDatasets()
{
    pCohesion = (GDALDataset*) GDALOpen(qsCohesion.toStdString().c_str(),GA_ReadOnly);
}

}
