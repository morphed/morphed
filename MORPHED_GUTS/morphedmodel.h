#ifndef MORPHEDMODEL_H
#define MORPHEDMODEL_H

#include <QObject>
#include "delft3dio.h"
#include "sedimenttransport.h"
#include "filemanager.h"
#include "symbology.h"

namespace MORPH{

class MORPHED_GUTSSHARED_EXPORT MORPHEDModel : public QObject
{
    Q_OBJECT

private:
    int iterations;
protected:
    QString qsRootDir, qsFloodDir;
    int currentIteration;
    Delft3DIO *DelftManager;
    SedimentTransport *SedimentManager;
    XMLReadWrite XmlMorph, XmlOut;
public:
    explicit MORPHEDModel(XMLReadWrite &XmlObj, QObject *parent = 0);
    ~MORPHEDModel();

    void copyInputFiles();
    void increment();
    void run();
    void setup(QThread &thread);
    void symbolizeDoD(GDALDataset *pDoDTiff, GDALDataset *pDoDPng);
    void writeFloodOutputs();

signals:
    void updateStatus(const QString &status);

public slots:
    void runOnThread();

};

}

#endif // MORPHEDMODEL_H
