#include "filemanager.h"

namespace MORPH{

FileManager::FileManager()
{
}

QString FileManager::copyFileToDirectory(QString originalPath, QString newDirectory, QString newFileName)
{
    bool result = false;
    QFile file(originalPath);
    QFileInfo fileInfo(file);
    QString suf;
    suf = fileInfo.suffix();
    QString newPath = newDirectory+"/"+newFileName + "." + suf;
    QFile file1(newPath);
    if (file1.exists())
    {
        file1.remove();
    }
    result = QFile::copy(originalPath, newPath);
    qDebug()<<"copying "<<originalPath<<" "<<newPath;
    if (!result)
    {
        newPath = QString();
        qDebug()<<"copy unsuccessful";
    }
    return newPath;
}

void FileManager::createBaseDirectories(QString rootDir)
{
    QDir root(rootDir);
    QDir in (rootDir + "/Inputs");
    QDir out (rootDir + "/Outputs");

    if (!in.exists())
    {
        root.mkdir("Inputs");
    }
    root.mkdir("Outputs");
    root.mkdir("temp");

    in.mkdir("01_InitialInputs");
    out.mkdir("01_FinalOutputs");
}

QString FileManager::createFloodDirectories(QString rootDir, int flood)
{
    QString floodName;

    QDir inputs(rootDir + "/Inputs");
    QDir outputs(rootDir + "/Outputs");

    if (flood < 10)
    {
        floodName = "Flood00" + QString::number(flood);
    }
    else if (flood < 100)
    {
        floodName = "Flood0" + QString::number(flood);
    }
    else if (flood <1000)
    {
        floodName = "Flood" + QString::number(flood);
    }
    else
    {

    }

    outputs.mkdir(floodName);
    inputs.mkdir(floodName);
    QDir inFlood (inputs.absolutePath() + "/" + floodName);
    QDir outFlood (outputs.absolutePath() + "/" + floodName);
    outFlood.mkdir("GTIFF");
    outFlood.mkdir("PNG");
    inFlood.mkdir("Delft3D");
    inFlood.mkdir("Hydraulics");

    return floodName;
}

QString FileManager::getFloodPath(int flood)
{
    QString floodName;
    if (flood < 10)
    {
        floodName = "Flood00" + QString::number(flood);
    }
    else if (flood < 100)
    {
        floodName = "Flood0" + QString::number(flood);
    }
    else if (flood <1000)
    {
        floodName = "Flood" + QString::number(flood);
    }
    else
    {

    }
    return floodName;
}

void FileManager::printTIFFDataset(QString filename, GDALDataset *pDS)
{
    GDALDriver *pDriver;
    pDriver = GetGDALDriverManager()->GetDriverByName("GTiff");
    GDALDataset *pPrintDS;
    pPrintDS = pDriver->CreateCopy(filename.toStdString().c_str(),pDS,FALSE,NULL,NULL,NULL);
    GDALClose(pPrintDS);
}

}
