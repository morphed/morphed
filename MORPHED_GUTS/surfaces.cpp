#include "surfaces.h"
#include "sedimenttransport.h"

namespace MORPH{

Surfaces::Surfaces(QString xmlPath):MORPHEDBaseClass(xmlPath)
{
}

Surfaces::Surfaces(XMLReadWrite &XmlObj):MORPHEDBaseClass(XmlObj)
{
    pOldSurface = NULL;
    pNewSurface = NULL;
    pSlope = NULL;
    pAspect = NULL;
    loadDrivers();
    pOldSurface = pInitialSurface;
    qsOldPath = qsTempDir + "/olddem.tif";
    qsNewPath = qsTempDir + "/newdem.tif";
    qsSlopePath = qsTempDir + "/slopeTOF.tif";
    qsAspectPath = qsTempDir + "/aspect.tif";
    qsDetrendPath = qsTempDir + "/detrend.tif";
    oldElev = (float*) CPLMalloc(sizeof(float)*1);
    newElev = (float*) CPLMalloc(sizeof(float)*1);
}

Surfaces::~Surfaces()
{
    if (pNewSurface != NULL)
    {
        GDALClose(pNewSurface);
    }
    if (pOldSurface != NULL)
    {
        GDALClose(pOldSurface);
    }
    if (pSlope != NULL)
    {
        GDALClose(pSlope);
    }
    if (pAspect != NULL)
    {
        GDALClose(pAspect);
    }
    if (pDetrend != NULL)
    {
        GDALClose(pDetrend);
    }
    CPLFree(oldElev);
    CPLFree(newElev);
    oldElev = NULL;
    newElev = NULL;
}

void Surfaces::setData()
{
    loadDrivers();
    if (nCurrentIteration != 0)
    {
//        if (pNewSurface != NULL)
//        {
//            GDALClose(pNewSurface);
//        }
//        pNewSurface = (GDALDataset*) GDALOpen(qsNewPath.toStdString().c_str(), GA_Update);
        if (pOldSurface != NULL)
        {
            qDebug()<<"Removing old dem";
            QFile file (qsOldPath);
            GDALClose(pOldSurface);
            if (file.remove())
            {
            //pOldSurface = NULL;
                qDebug()<<"Old dem removed";
            }
            else
            {
                qDebug()<<file.error();
            }
        }
        pOldSurface = pDriverTIFF->CreateCopy(qsOldPath.toStdString().c_str(),pNewSurface,FALSE,NULL,NULL,NULL);
        qDebug()<<"Old dem created";
    }
    else
    {
        pOldSurface = pDriverTIFF->CreateCopy(qsOldPath.toStdString().c_str(),pInitialSurface,FALSE,NULL,NULL,NULL);
        pNewSurface = pDriverTIFF->CreateCopy(qsNewPath.toStdString().c_str(),pInitialSurface,FALSE,NULL,NULL,NULL);
    }
    if (nCurrentIteration == 0)
    {
        detrendDEM(pOldSurface,dirUS,dirDS,qsDetrendPath);
        pDetrend = (GDALDataset*) GDALOpen(qsDetrendPath.toStdString().c_str(),GA_ReadOnly);
    }

    if (pSlope != NULL)
    {
        GDALClose(pSlope);
    }
    pSlope = pDriverTIFF->Create(qsSlopePath.toStdString().c_str(),nCols,nRows,1,GDT_Float32,NULL);
    pSlope->SetGeoTransform(transform);
    pSlope->GetRasterBand(1)->Fill(fNoDataValue);
    pSlope->GetRasterBand(1)->SetNoDataValue(fNoDataValue);
    calculateSlopeThirdOrderFinite(pOldSurface);
}

void Surfaces::calculateAspect()
{
    char **papszOptions = NULL;
    int xsize = pOldSurface->GetRasterBand(1)->GetXSize();
    int ysize = pOldSurface->GetRasterBand(1)->GetYSize();

    pAspect = pDriverTIFF->Create(qsAspectPath.toStdString().c_str(),xsize,ysize,1,GDT_Float32,papszOptions);
    pAspect->SetGeoTransform(transform);
    pAspect->GetRasterBand(1)->Fill(fNoDataValue);
    pAspect->GetRasterBand(1)->SetNoDataValue(fNoDataValue);

    int indexCell = xsize;
    double z1,z2,z3,z4,z5,z6,z7,z8,dx,dy;

    float* rows3 = (float*) CPLMalloc(sizeof(float)*(xsize*3));
    float* aspectVal = (float*) CPLMalloc(sizeof(float)*1);
    for (int i=0; i<nRows-2; i++)
    {
        pOldSurface->GetRasterBand(1)->RasterIO(GF_Read,0,i,xsize,3,rows3,xsize,3,GDT_Float32,0,0);
        for (int j=1; j<nCols-1; j++)
        {
            indexCell = xsize+j;
            z1 = rows3[indexCell-xsize-1], z2 = rows3[indexCell-xsize], z3 = rows3[indexCell-xsize+1], z4 = rows3[indexCell-1];
            z5 = rows3[indexCell+1], z6 = rows3[indexCell+xsize-1], z7 = rows3[indexCell+xsize], z8 = rows3[indexCell+xsize+1];
            if (rows3[indexCell] == fNoDataValue || z1 == fNoDataValue || z2 == fNoDataValue || z3 == fNoDataValue || z4 == fNoDataValue || z5 == fNoDataValue || z6 == fNoDataValue || z7 == fNoDataValue || z8 == fNoDataValue)
            {
                *aspectVal = fNoDataValue;
            }
            else
            {
                dx = ((z3 + z5 + z5 + z8) - (z1 + z4 + z4 + z6));
                dy = ((z6 + z7 + z7 + z8) - (z1 + z2 + z2 + z3));
                *aspectVal = atan2(dy/8.0,((-1.0)*dx/8.0))*180.0/MORPH::PI;
                if (dx == 0)
                {
                    if (*aspectVal < 0)
                        *aspectVal = 90.0 - *aspectVal;
                    else if (*aspectVal > 90.0)
                        *aspectVal = 360.0 - *aspectVal + 90.0;
                    else
                        *aspectVal = 90.0 - *aspectVal;
                }
                else
                {
                    if (*aspectVal > 90.0)
                        *aspectVal = 450.0 - *aspectVal;
                    else
                        *aspectVal = 90.0 - *aspectVal;
                }

                if (*aspectVal == 0.0)
                    *aspectVal = 0.001;
            }
            pAspect->GetRasterBand(1)->RasterIO(GF_Write,j,(i+1),1,1,aspectVal,1,1,GDT_Float32,0,0);
        }
    }
    CPLFree(rows3);
    CPLFree(aspectVal);
    rows3 = NULL;
    aspectVal = NULL;
}

double Surfaces::calculateAverageSlope_ColAve(GDALDataset *pDataset, int col1, int col2)
{
    double col1Ave, col2Ave, aveSlope, distance;

    distance = col1-col2;
    if (distance < 0)
    {
        distance *= (-1);
    }

    col1Ave = calculateColAverage(pDataset, col1);
    col2Ave = calculateColAverage(pDataset, col2);
    aveSlope = (col1Ave - col2Ave)/(cellWidth*distance);

    if (aveSlope < 0)
    {
        aveSlope *= (-1);
    }

    return aveSlope;
}

double Surfaces::calculateAverageSlope_ColMin(GDALDataset *pDataset, int col1, int col2)
{
    double col1Ave, col2Ave, aveSlope, distance;

    distance = col1-col2;
    if (distance < 0)
    {
        distance *= (-1);
    }

    col1Ave = findColMin(pDataset, col1);
    col2Ave = findColMin(pDataset, col2);
    aveSlope = (col1Ave - col2Ave)/(cellWidth*distance);

    if (aveSlope < 0)
    {
        aveSlope *= (-1);
    }

    return aveSlope;
}

double Surfaces::calculateAverageSlope_RowAve(GDALDataset *pDataset, int row1, int row2)
{
    double row1Ave, row2Ave, aveSlope, distance;

    distance = row1-row2;
    if (distance < 0)
    {
        distance *= (-1);
    }

    row1Ave = calculateRowAverage(pDataset, row1);
    row2Ave = calculateRowAverage(pDataset, row2);
    aveSlope = (row1Ave - row2Ave)/(cellHeight*distance);

    if (aveSlope < 0)
    {
        aveSlope *= (-1);
    }

    return aveSlope;
}

double Surfaces::calculateAverageSlope_RowMin(GDALDataset *pDataset, int row1, int row2)
{
    double row1Ave, row2Ave, aveSlope, distance;

    distance = row1-row2;
    if (distance < 0)
    {
        distance *= (-1);
    }

    row1Ave = findRowMin(pDataset, row1);
    row2Ave = findRowMin(pDataset, row2);
    aveSlope = (row1Ave - row2Ave)/(cellHeight*distance);

    if (aveSlope < 0)
    {
        aveSlope *= (-1);
    }

    return aveSlope;
}

double Surfaces::calculateColAverage(GDALDataset *pDataset, int col)
{
    double sum = 0;
    double count = 0;
    float* value = (float*) CPLMalloc(sizeof(float)*1);

    for (int i=0; i<nRows; i++)
    {
        pDataset->GetRasterBand(1)->RasterIO(GF_Read, col, i, 1, 1, value, 1, 1, GDT_Float32, 0, 0);

        if(*value != fNoDataValue)
        {
            sum += *value;
            count++;
        }
    }
    CPLFree(value);
    value = NULL;
    return sum/count;
}
GDALDataset *Surfaces::calculateDoDCumulative(QString qsDodPath)
{
    GDALDataset *pDoD;
    pDoD = pDriverTIFF->Create(qsDodPath.toStdString().c_str(),nCols,nRows,1,GDT_Float32,NULL);
    pDoD->GetRasterBand(1)->SetNoDataValue(fNoDataValue);
    pDoD->SetGeoTransform(transform);

    float *oldRow = (float*) CPLMalloc(sizeof(float)*nCols);
    float *newRow = (float*) CPLMalloc(sizeof(float)*nCols);
    float *dodRow = (float*) CPLMalloc(sizeof(float)*nCols);

    for (int i=0; i<nRows; i++)
    {
        pInitialSurface->GetRasterBand(1)->RasterIO(GF_Read,0,i,nCols,1,oldRow,nCols,1,GDT_Float32,0,0);
        pNewSurface->GetRasterBand(1)->RasterIO(GF_Read,0,i,nCols,1,newRow,nCols,1,GDT_Float32,0,0);

        for (int j=0; j<nRows; j++)
        {
            if (newRow[j] == fNoDataValue || oldRow[j] == fNoDataValue)
            {
                dodRow[j] = fNoDataValue;
            }
            else
            {
                dodRow[j] = newRow[j] - oldRow[j];
            }
        }
        pDoD->GetRasterBand(1)->RasterIO(GF_Write,0,i,nCols,1,dodRow,nCols,1,GDT_Float32,0,0);
    }
    qDebug()<<"dod generation done";

    CPLFree(oldRow);
    oldRow = NULL;
    CPLFree(newRow);
    newRow = NULL;
    CPLFree(dodRow);
    dodRow = NULL;
    qDebug()<<"dod memory freed";

    return pDoD;
}

GDALDataset *Surfaces::calculateDoDRecent(QString qsDodPath)
{
    GDALDataset *pDoD;
    pDoD = pDriverTIFF->Create(qsDodPath.toStdString().c_str(),nCols,nRows,1,GDT_Float32,NULL);
    pDoD->GetRasterBand(1)->SetNoDataValue(fNoDataValue);
    pDoD->SetGeoTransform(transform);

    float *oldRow = (float*) CPLMalloc(sizeof(float)*nCols);
    float *newRow = (float*) CPLMalloc(sizeof(float)*nCols);
    float *dodRow = (float*) CPLMalloc(sizeof(float)*nCols);
    qDebug()<<"dod generation started";

    for (int i=0; i<nRows; i++)
    {
        pOldSurface->GetRasterBand(1)->RasterIO(GF_Read,0,i,nCols,1,oldRow,nCols,1,GDT_Float32,0,0);
        pNewSurface->GetRasterBand(1)->RasterIO(GF_Read,0,i,nCols,1,newRow,nCols,1,GDT_Float32,0,0);

        for (int j=0; j<nRows; j++)
        {
            if (newRow[j] == fNoDataValue || oldRow[j] == fNoDataValue)
            {
                dodRow[j] = fNoDataValue;
            }
            else
            {
                dodRow[j] = newRow[j] - oldRow[j];
            }
        }
        pDoD->GetRasterBand(1)->RasterIO(GF_Write,0,i,nCols,1,dodRow,nCols,1,GDT_Float32,0,0);
    }

    qDebug()<<"dod generation done";

    CPLFree(oldRow);
    oldRow = NULL;
    CPLFree(newRow);
    newRow = NULL;
    CPLFree(dodRow);
    dodRow = NULL;
    qDebug()<<"dod memory freed";

    return pDoD;
}

double Surfaces::calculateRowAverage(int row)
{
    double sum = 0;
    double count = 0;
    float* value = (float*) CPLMalloc(sizeof(float)*1);

    for (int i=0; i<nCols; i++)
    {
        pOldSurface->GetRasterBand(1)->RasterIO(GF_Read, i, row, 1, 1, value, 1, 1, GDT_Float32, 0, 0);

        if(*value != fNoDataValue)
        {
            sum += *value;
            count++;
        }
    }
    return sum/count;
}

double Surfaces::calculateRowAverage(GDALDataset *pDataset, int row)
{
    double sum = 0;
    double count = 0;
    float* value = (float*) CPLMalloc(sizeof(float)*1);

    for (int i=0; i<nCols; i++)
    {
        pDataset->GetRasterBand(1)->RasterIO(GF_Read, i, row, 1, 1, value, 1, 1, GDT_Float32, 0, 0);

        if(*value != fNoDataValue)
        {
            sum += *value;
            count++;
        }
    }
    CPLFree(value);
    value = NULL;
    return sum/count;
}

void Surfaces::calculateSlopeThirdOrderFinite(GDALDataset *pDemDS)
{
    double xslope,yslope,xyslope,xypow;

    float* eVals = (float*) CPLMalloc(sizeof(float)*9);
    float* sVals = (float*) CPLMalloc(sizeof(float)*nCols);
    for (int i=0; i<nRows-1; i++)
    {
        for (int j=0; j<nCols-1; j++)
        {
            if (i == 0 || i == nRows || j == 0 || j == nCols)
            {
                sVals[j] = fNoDataValue;
            }
            else
            {
                pDemDS->GetRasterBand(1)->RasterIO(GF_Read,j-1,i-1,3,3,eVals,3,3,GDT_Float32,0,0);
                if (eVals[4] == fNoDataValue || eVals[0] == fNoDataValue || eVals[1] == fNoDataValue || eVals[2] == fNoDataValue || eVals[3] == fNoDataValue || eVals[5] == fNoDataValue || eVals[6] == fNoDataValue || eVals[7] == fNoDataValue || eVals[8] == fNoDataValue)
                {
                    sVals[j] = fNoDataValue;
                }
                else
                {
                    xslope = ((eVals[2]-eVals[0]) + ((2*eVals[5])-(2*eVals[3])) + (eVals[8]-eVals[6])) / (8*cellWidth);
                    yslope = ((eVals[0]-eVals[6]) + ((2*eVals[1])-(2*eVals[7])) + (eVals[2]-eVals[8]))/(8*cellWidth);
                    xyslope = pow(xslope,2.0) + pow(yslope,2.0);
                    xypow = pow(xyslope,0.5);
                    sVals[j] = (atan(xypow)*180.0/MORPH::PI);
                }
            }
        }
        pSlope->GetRasterBand(1)->RasterIO(GF_Write,0,i,nCols,1,sVals,nCols,1,GDT_Float32,0,0);
    }
    CPLFree(eVals);
    CPLFree(sVals);
    eVals = NULL;
    sVals = NULL;
}

void Surfaces::closeDatasets()
{
    if (pOldSurface != NULL)
    {
        GDALClose(pOldSurface);
    }
    if (pNewSurface != NULL)
    {
        GDALClose(pNewSurface);
    }
    if (pSlope != NULL)
    {
        GDALClose(pSlope);
    }
    if (pAspect != NULL)
    {
        GDALClose(pAspect);
    }
    if (pDetrend != NULL)
    {
        GDALClose(pDetrend);
    }
}

void Surfaces::closeDEMs()
{
    if (pOldSurface != NULL)
    {
        GDALClose(pOldSurface);
        pOldSurface = NULL;
    }
    if (pNewSurface != NULL)
    {
        GDALClose(pNewSurface);
        pNewSurface = NULL;
    }
    if (pSlope != NULL)
    {
        GDALClose(pSlope);
        pSlope = NULL;
    }
}

void Surfaces::copyDSBoundary()
{
    if (nDirDSbound == 1)
    {
        float *newRow = (float*) CPLMalloc(sizeof(float)*nCols);
        pNewSurface->GetRasterBand(1)->RasterIO(GF_Read,0,2,nCols,1,newRow,nCols,1,GDT_Float32,0,0);
        pNewSurface->GetRasterBand(1)->RasterIO(GF_Write,0,1,nCols,1,newRow,nCols,1,GDT_Float32,0,0);
        pNewSurface->GetRasterBand(1)->RasterIO(GF_Write,0,0,nCols,1,newRow,nCols,1,GDT_Float32,0,0);
        CPLFree(newRow);
        newRow = NULL;
    }
    else if (nDirDSbound == 2)
    {
        float *newRow = (float*) CPLMalloc(sizeof(float)*nCols);
        pNewSurface->GetRasterBand(1)->RasterIO(GF_Read,0,nRows-2,nCols,1,newRow,nCols,1,GDT_Float32,0,0);
        pNewSurface->GetRasterBand(1)->RasterIO(GF_Write,0,nRows-1,nCols,1,newRow,nCols,1,GDT_Float32,0,0);
        pNewSurface->GetRasterBand(1)->RasterIO(GF_Write,0,nRows-0,nCols,1,newRow,nCols,1,GDT_Float32,0,0);
        CPLFree(newRow);
        newRow = NULL;
    }
    else if (nDirDSbound == 3)
    {

    }
    else if (nDirDSbound == 4)
    {

    }
}

void Surfaces::detrendDEM(GDALDataset *pDemDS, Direction dirUS, Direction dirDS, QString detrendPath)
{
    double slope, detrendVal, ndv, cellRes, distance;
    double demTransform[6];
    int xsize, ysize, midindex;
    float *oldVals, *detrendVals;
    GDALDataset *pDetrendDS;

    xsize = pDemDS->GetRasterBand(1)->GetXSize();
    ysize = pDemDS->GetRasterBand(1)->GetYSize();
    pDemDS->GetGeoTransform(demTransform);
    ndv = pDemDS->GetRasterBand(1)->GetNoDataValue();
    cellRes = demTransform[1];

    loadDrivers();

    pDetrendDS = pDriverTIFF->Create(detrendPath.toStdString().c_str(),xsize,ysize,1,GDT_Float32,NULL);
    pDetrendDS->SetGeoTransform(demTransform);
    pDetrendDS->GetRasterBand(1)->SetNoDataValue(ndv);

    if (dirUS == MDIR_South && dirDS == MDIR_North)
    {
        midindex = round(ysize/2.0);
        slope = calculateAverageSlope_RowAve(pDemDS, ysize-1, 0);
        qDebug()<<"detrend slope "<<slope;

        oldVals = (float*) CPLMalloc(sizeof(float)*xsize);
        detrendVals = (float*) CPLMalloc(sizeof(float)*xsize);

        for (int i=0; i<ysize; i++)
        {
            distance = (midindex-i)*cellRes;
            detrendVal = distance * slope;
            pDemDS->GetRasterBand(1)->RasterIO(GF_Read,0,i,xsize,1,oldVals,xsize,1,GDT_Float32,0,0);
            for (int j=0; j<xsize; j++)
            {
                if (oldVals[j] != ndv)
                {
                    detrendVals[j] = oldVals[j] + detrendVal;
                }
                else
                {
                    detrendVals[j] = ndv;
                }
            }
            pDetrendDS->GetRasterBand(1)->RasterIO(GF_Write,0,i,xsize,1,detrendVals,xsize,1,GDT_Float32,0,0);
        }
    }
    GDALClose(pDetrendDS);
    CPLFree(oldVals);
    oldVals = NULL;
    CPLFree(detrendVals);
    detrendVals = NULL;
}

double Surfaces::findColMin(GDALDataset *pDataset, int col)
{
    double min = fNoDataValue;
    int count = 0;
    float* value = (float*) CPLMalloc(sizeof(float)*1);

    for (int i=0; i<nRows; i++)
    {
        pDataset->GetRasterBand(1)->RasterIO(GF_Read, col, i, 1, 1, value, 1, 1, GDT_Float32, 0, 0);
        if(*value != fNoDataValue)
        {
            if (count == 0)
            {
                min = *value;
                count++;
            }
            else
            {
                if (*value < min)
                {
                    min = *value;
                }
            }
        }
    }
    CPLFree(value);
    value = NULL;
    return min;
}

QVector<int> Surfaces::findLowestNeighbor(int row, int col, double& amount)
{
    QVector<int> lowcell;
    lowcell.resize(2);
    int index = 4;
    double min, diff;
    double removed;

    float *neighborVals = (float*) CPLMalloc(sizeof(float)*9);
    float *newVal = (float*) CPLMalloc(sizeof(float));

    if (row > 0 && row < nRows && col > 0 && col <nCols)
    {
        pNewSurface->GetRasterBand(1)->RasterIO(GF_Read,col-1,row-1,3,3,neighborVals,3,3,GDT_Float32,0,0);

        //find lowest neighbor
        min = 0.0;

        for (int i=0; i<9; i++)
        {
            if (i != 4 && neighborVals[i] != fNoDataValue)
            {
                diff = fabs(neighborVals[i] - neighborVals[4]);
                if (diff > min)
                {
                    min = diff;
                    index = i;
                }
            }
        }

        removed = min * 0.1;
        //make sure low neighbor is less than target cell and adjust target cell height
        if (neighborVals[index] < neighborVals[4])
        {
            *newVal = neighborVals[4] - removed;
            if (removed > SedimentTransport::MAXFAIL_MIN)
            {
                amount = removed;
                pNewSurface->GetRasterBand(1)->RasterIO(GF_Write,col,row,1,1,newVal,1,1,GDT_Float32,0,0);
                lowcell[0] = row + ROW_OFFSET[index], lowcell[1] = col + COL_OFFSET[index];
                //qDebug()<<"subract from target: min "<<min<<" removed "<<removed<<" index "<<index;
            }
            else
            {
                amount = 0.0;
                lowcell[0] = 0, lowcell[1] = 0;
            }
        }
        else
        {
            neighborVals[index] -= removed;
            if (removed > SedimentTransport::MAXFAIL_MIN)
            {
                amount = removed;
                pNewSurface->GetRasterBand(1)->RasterIO(GF_Write,col-1,row-1,3,3,neighborVals,3,3,GDT_Float32,0,0);
                lowcell[0] = row, lowcell[1] = col;
                //qDebug()<<"add to target: min "<<min<<" removed "<<removed<<" index "<<index;
            }
            else
            {
                amount = 0.0;
                lowcell[0] = 0, lowcell[1] = 0;
            }
        }
    }
    else
    {
        lowcell[0] = 0, lowcell[1] = 0;
        amount = 0.0;
    }
    if (amount > 10.0)
    {
        qDebug()<<"amount to great "<<row<<" "<<col<<" index = "<<index<<" elevs = "<<neighborVals[0]<<" "<<neighborVals[1]<<" "<<neighborVals[2]<<" "<<neighborVals[3]<<" "<<neighborVals[4]<<" "<<neighborVals[5]<<" "<<neighborVals[6]<<" "<<neighborVals[7]<<" "<<neighborVals[8];
    }
    CPLFree(neighborVals);
    neighborVals = NULL;
    CPLFree(newVal);
    newVal = NULL;

    return lowcell;
}

double Surfaces::findRowMin(GDALDataset *pDataset, int row)
{
    double min = fNoDataValue;
    int count = 0;
    float* value = (float*) CPLMalloc(sizeof(float)*1);

    for (int i=0; i<nCols; i++)
    {
        pDataset->GetRasterBand(1)->RasterIO(GF_Read, i, row, 1, 1, value, 1, 1, GDT_Float32, 0, 0);
        if(*value != fNoDataValue)
        {
            if (count == 0)
            {
                min = *value;
                count++;
            }
            else
            {
                if (*value < min)
                {
                    min = *value;
                }
            }
        }
    }
    CPLFree(value);
    value = NULL;
    return min;
}

GDALDataset *Surfaces::getOldSurface()
{
    return pOldSurface;
}

void Surfaces::loadNewSurface()
{
    if (pNewSurface != NULL)
    {
        GDALClose(pNewSurface);
    }
    pNewSurface = (GDALDataset*) GDALOpen(qsNewPath.toStdString().c_str(), GA_ReadOnly);
}

void Surfaces::loadOldSurface(GDALDataset *pCopyDS)
{
    if (pOldSurface != NULL)
    {
        GDALClose(pOldSurface);
    }
   //pOldSurface = (GDALDataset*) GDALOpen(qsOldPath.toStdString().c_str(), GA_ReadOnly);
    pOldSurface  = pDriverTIFF->CreateCopy(qsOldPath.toStdString().c_str(),pCopyDS,FALSE,NULL,NULL,NULL);
}

void Surfaces::recalcSlope3x3TOF(GDALDataset *pDemDS, int row, int col)
{
    double xslope,yslope,xyslope,xypow;

    float* eVals = (float*) CPLMalloc(sizeof(float)*9);
    float* slopeVal = (float*) CPLMalloc(sizeof(float)*1);
    for (int i=row-1; i<row+2; i++)
    {
        for (int j=col-1; j<col+2; j++)
        {
            pDemDS->GetRasterBand(1)->RasterIO(GF_Read,j-1,i-1,3,3,eVals,3,3,GDT_Float32,0,0);
            if (eVals[4] == fNoDataValue || eVals[0] == fNoDataValue || eVals[1] == fNoDataValue || eVals[2] == fNoDataValue || eVals[3] == fNoDataValue || eVals[5] == fNoDataValue || eVals[6] == fNoDataValue || eVals[7] == fNoDataValue || eVals[8] == fNoDataValue)
            {
                *slopeVal = fNoDataValue;
            }
            else
            {
                xslope = ((eVals[2]-eVals[0]) + ((2*eVals[5])-(2*eVals[3])) + (eVals[8]-eVals[6])) / (8*cellWidth);
                yslope = ((eVals[0]-eVals[6]) + ((2*eVals[1])-(2*eVals[7])) + (eVals[2]-eVals[8]))/(8*cellWidth);
                xyslope = pow(xslope,2.0) + pow(yslope,2.0);
                xypow = pow(xyslope,0.5);
                *slopeVal = (atan(xypow)*180.0/MORPH::PI);
            }
            pSlope->GetRasterBand(1)->RasterIO(GF_Write,j,(i+1),1,1,slopeVal,1,1,GDT_Float32,0,0);
        }
    }
    CPLFree(eVals);
    CPLFree(slopeVal);
    eVals = NULL;
    slopeVal = NULL;
}

void Surfaces::calculateDoD(GDALDataset *oldSurface, GDALDataset *newSurface, GDALDataset *dodDataset)
{
    int rows, cols;
    double geotrans[6];
    double ndv;

    rows = oldSurface->GetRasterBand(1)->GetYSize();
    cols = oldSurface->GetRasterBand(1)->GetXSize();
    oldSurface->GetGeoTransform(geotrans);
    dodDataset->SetGeoTransform(geotrans);
    ndv = oldSurface->GetRasterBand(1)->GetNoDataValue();
    dodDataset->GetRasterBand(1)->SetNoDataValue(ndv);

    float *oldRow = (float*) CPLMalloc(sizeof(float)*cols);
    float *newRow = (float*) CPLMalloc(sizeof(float)*cols);
    float *dodRow = (float*) CPLMalloc(sizeof(float)*cols);

    for (int i=0; i<rows; i++)
    {
        oldSurface->GetRasterBand(1)->RasterIO(GF_Read,0,i,cols,1,oldRow,cols,1,GDT_Float32,0,0);
        newSurface->GetRasterBand(1)->RasterIO(GF_Read,0,i,cols,1,newRow,cols,1,GDT_Float32,0,0);
        for (int j=0; j<cols; j++)
        {
            if (oldRow[j] == ndv || newRow[j] == ndv)
            {
                dodRow[j] = ndv;
            }
            else
            {
                dodRow[j] = newRow[j] - oldRow[j];
            }
        }
        dodDataset->GetRasterBand(1)->RasterIO(GF_Write,0,i,cols,1,dodRow,cols,1,GDT_Float32,0,0);
    }

    CPLFree(oldRow);
    CPLFree(newRow);
    CPLFree(dodRow);
}

void Surfaces::filterHighPass(GDALDataset *pOldDS, GDALDataset *pTargetDS)
{
    QVector<double> highPass;
    double newValue;
    int rows = pOldDS->GetRasterBand(1)->GetYSize();
    int cols = pOldDS->GetRasterBand(1)->GetXSize();
    float *data = (float*) CPLMalloc(sizeof(float)*9);
    float *newRow = (float*) CPLMalloc(sizeof(float)*cols);

    newRow[0] = pOldDS->GetRasterBand(1)->GetNoDataValue();
    newRow[rows-1] = pOldDS->GetRasterBand(1)->GetNoDataValue();

    pTargetDS->GetRasterBand(1)->SetNoDataValue(pOldDS->GetRasterBand(1)->GetNoDataValue());
    pTargetDS->GetRasterBand(1)->Fill(pTargetDS->GetRasterBand(1)->GetNoDataValue());

    highPass.resize(9);
    highPass[0] = (-0.7), highPass[1] = (-1.0), highPass[2] = (-0.7), highPass[3] = (-1.0), highPass[4] = 6.8, highPass[5] = (-1.0), highPass[6] = (-0.7), highPass[7] = (-1.0), highPass[8] = (-0.7);

    for (int i=1; i<rows-1; i++)
    {
        for (int j=1; j<cols-1; j++)
        {
            pOldDS->GetRasterBand(1)->RasterIO(GF_Read,j-1,i-1,3,3,data,3,3,GDT_Float32,0,0);
            if (data[4] != pOldDS->GetRasterBand(1)->GetNoDataValue())
            {
                newValue = 0.0;
                for (int i=0; i<9; i++)
                {
                    if (data[i] != pOldDS->GetRasterBand(1)->GetNoDataValue())
                    {
                        newValue += data[i] * highPass[i];
                    }
                }
                newRow[j] = newValue;
            }
            else
            {
                newRow[j] = pOldDS->GetRasterBand(1)->GetNoDataValue();
            }

        }
        pTargetDS->GetRasterBand(1)->RasterIO(GF_Write,0,i,cols,1,newRow,cols,1,GDT_Float32,0,0);
    }
}

void Surfaces::filterLowPass(GDALDataset *pOldDS, GDALRasterBand *pTargetBand)
{
    double average;
    int rows = pOldDS->GetRasterBand(1)->GetYSize();
    int cols = pOldDS->GetRasterBand(1)->GetXSize();
    float *data = (float*) CPLMalloc(sizeof(float)*9);
    float *newVal = (float*) CPLMalloc(sizeof(float)*1);

    double sum;
    int count;

    pTargetBand->SetNoDataValue(pOldDS->GetRasterBand(1)->GetNoDataValue());
    pTargetBand->Fill(pTargetBand->GetNoDataValue());

    for (int i=1; i<rows-1; i++)
    {
        for (int j=1; j<cols-1; j++)
        {
            pOldDS->GetRasterBand(1)->RasterIO(GF_Read,j-1,i-1,3,3,data,3,3,GDT_Float32,0,0);
            sum = 0.0, count = 0;

            for (int k=0; k<9; k++)
            {
                if (data[k] != pOldDS->GetRasterBand(1)->GetNoDataValue())
                {
                    sum += data[k];
                    count++;
                }
            }
            average = sum / (count*1.0);
            *newVal = average;
            pTargetBand->RasterIO(GF_Write,j,i,1,1,newVal,1,1,GDT_Float32,0,0);
        }
    }
}

}
