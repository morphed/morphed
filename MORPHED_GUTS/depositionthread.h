#ifndef DEPOSITIONTHREAD_H
#define DEPOSITIONTHREAD_H

#include <QThread>
//#include "sedimenttransport.h"
class SedimentTransport;

namespace MORPH{

class DepositionThread : public QThread
{
private:
    SedimentTransport *TransportObj;
    int startRow, startCol;
    double depositionAmount, grainSize;
protected:

public:
    DepositionThread(SedimentTransport *SedObj, int row, int col, double amt, double gsz);

    void run();
};

}

#endif // DEPOSITIONTHREAD_H
