#ifndef MORPHEDBASECLASS_H
#define MORPHEDBASECLASS_H

#include <QtCore>
#include <iostream>
#include <cmath>
#include "xmlreadwrite.h"
#include "filemanager.h"
#include "gdal.h"
#include "gdal_priv.h"

class Surfaces;
class Hydraulics;
class PathLengthDistribution;

namespace MORPH{

typedef enum {MDIR_North = 1, MDIR_South = 2, MDIR_East = 3, MDIR_West = 4} Direction;
typedef enum {EXP = 1, GAUSSIAN = 2, CUSTOM = 3} Distribution;
typedef enum {EXACT = 1, PROPORTION = 2} Volume;
typedef enum {SIZE = 1, PERCENT = 2, RASTER = 3} GS_Type;
const double PI = 3.14159265;
const int ROW_OFFSET[9] = {-1,-1,-1,0,0,0,1,1,1};
const int COL_OFFSET[9] = {-1,0,1,-1,0,1,-1,0,1};
const int ROW_OFFSET5[25] = {-2,-2,-2,-2,-2,-1,-1,-1,-1,-1,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2};
const int COL_OFFSET5[25] = {-2,-1,0,1,2,-2,-1,0,1,2,-2,-1,0,1,2,-2,-1,0,1,2,-2,-1,0,1,2};

class MORPHED_GUTSSHARED_EXPORT MORPHEDBaseClass
{
protected:
    XMLReadWrite XmlInit;
    QString qsInitialPath, qsInputText, qsXmlPath; //Path of starting surface, input file, and xml
    double cellWidth, cellHeight, xTopLeft, yTopLeft; //Cell dimensions current
    int nDirUSbound, nDirDSbound, nImport; //boundary locations, sediment import type
    Direction dirUS, dirDS;


public:
    GDALDriver *pDriverTIFF, *pDriverPNG, *pDriverVRT, *pDriverIMG; //GDAL drivers
    double transform[6]; //Affine transformation data for the current surface
    QString qsOutputs, qsInputs, qsRootDir, qsTempDir, qsFloodName;
    GDALDataset *pInitialSurface; //Original starting surface for the model run
    int nIterations, nCurrentIteration; //Total iterations to run the model, current iteration
    QVector<double> date, q, dswe, import; //vectors hold data from input text file
    int nRows, nCols; //rows and columns of the initial surface
    float fNoDataValue; //No data value to be used

    static const float NO_DATA = -9999;

    MORPHEDBaseClass();
    MORPHEDBaseClass(QString xmlPath);
    MORPHEDBaseClass(MORPHEDBaseClass &obj);
    MORPHEDBaseClass(XMLReadWrite &obj);
    ~MORPHEDBaseClass();

    void init();
    void init(QString xmlPath);
    void init(MORPHEDBaseClass &obj);

    void incrementCurrentIteration();
    GDALDataset* loadDatasetFromXYZ(QString qsRasterPath, QString qsXyzPath);
    void loadDrivers();
    void loadInputText();
    void printDatasetToXYZ(GDALDataset *pDataset);
    void setInOutPaths();
    int xCoordToCellAddress(double coord);
    int yCoordtoCellAddress(double coord);

    static double findMax(GDALDataset *pDataset);
    static double findMaxVector(QVector<double> vector, int count);
    static double findMin(GDALDataset *pDataset);
    static QVector<double> getDistribution(int type, double sigA, double muB, int length, double cellSize);
    static int getLength(double distance, double cellSize);
    static void loadInputText(QString inputFile, QVector<double> &dateTime, QVector<double> &discharge, QVector<double> &waterElev, QVector<double> &importQuant, int iterations);
    static void setDirection(int dirCode, Direction& dirVar);
};
}

#endif // MORPHEDBASECLASS_H
