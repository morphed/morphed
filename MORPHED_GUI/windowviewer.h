#ifndef WINDOWVIEWER_H
#define WINDOWVIEWER_H

//#include "xmlreadwrite.h"
#include "dialoganimate.h"
#include "morphedbaseclass.h"
#include <QMainWindow>
#include <QtGui>
#include <QtCore>
#include <QGraphicsView>

namespace Ui {
class WindowViewer;
}

class WindowViewer : public QMainWindow
{
    Q_OBJECT

public:
    explicit WindowViewer(QWidget *parent = 0);
    ~WindowViewer();

    void setupPlots();
    void updatePlots();
    void updateTable();
    void updateView();

private slots:
    void on_actionOpen_Project_triggered();

    void on_intSpin_flood_valueChanged(int arg1);

    void on_tbtn_prev_clicked();

    void on_tbtn_next_clicked();

    void on_actionAnimate_triggered();

    void on_actionAnimation_triggered();

    void on_actionClose_triggered();

private:
    Ui::WindowViewer *ui;

    MORPH::XMLReadWrite XmlDisplay;
    int nIterations, nCurrent, nAnimateSecs;
    double bedEE, bedET, bedDE, bedDT, sloughEE, sloughET, sloughDE, sloughDT, importE, importT, exportE, exportT;
    QVector<double> qvEventImbal, qvTotalImbal, qvEventMech, qvTotalMech, qvDate, qvQ, qvDswe, qvSedin, qvHydroPosX, qvHydroPosY;
    QVector<QColor> qvImbalColor, qvMechColor;
    QString floodName, qsDirPath;
    QGraphicsScene *scene;
    QGraphicsItem *dem, *dod, *depth, *hlsd;
};

#endif // WINDOWVIEWER_H
