#include "dialoginputs.h"
#include "ui_dialoginputs.h"

DialogInputs::DialogInputs(MORPH::XMLReadWrite &XmlObj, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogInputs)
{
    ui->setupUi(this);
    update = false;
    XmlProj = XmlObj;
    setParamsFromXML();

    ui->plot_hydroSedi->addGraph();
    ui->plot_hydroSedi->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc,5));
    ui->plot_hydroSedi->graph(0)->setLineStyle(QCPGraph::lsLine);
    ui->plot_hydroSedi->graph(0)->setPen(QPen(Qt::blue));
    ui->plot_hydroSedi->addGraph();
    ui->plot_hydroSedi->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc,5));
    ui->plot_hydroSedi->graph(1)->setLineStyle(QCPGraph::lsLine);
    ui->plot_hydroSedi->graph(1)->setPen(QPen(Qt::black));
    ui->plot_hydroSedi->yAxis->setLabel("Discharge (cms)");
    ui->plot_hydroSedi->yAxis2->setVisible(true);
    ui->plot_hydroSedi->yAxis2->setLabel("Sediment Imported");
    ui->plot_hydroSedi->xAxis->setLabel("Date");
}

DialogInputs::~DialogInputs()
{
    delete ui;
}

int DialogInputs::getIterations()
{
    return iterations;
}

QString DialogInputs::getOrigDEMPath()
{
    return demOrig;
}

bool DialogInputs::getUpdate()
{
    return update;
}

void DialogInputs::setParamsFromXML()
{
    QString nodeData;
    nodeData = XmlProj.readNodeData("Inputs", "DEMPath");
    if (!nodeData.isNull() || !nodeData.isEmpty())
    {
        dem = nodeData;
        ui->line_dem->setText(dem);
    }
    nodeData = XmlProj.readNodeData("Inputs", "ModelIterations");
    if (!nodeData.isNull() || !nodeData.isEmpty())
    {
        iterations = nodeData.toInt();
        ui->intSpin_iterations->setValue(iterations);
    }
    nodeData = XmlProj.readNodeData("Inputs", "HydroSediPath");
    if (!nodeData.isNull() || !nodeData.isEmpty())
    {
        hydroSedi = nodeData;
        ui->line_hydrosedi->setText(hydroSedi);
    }
    nodeData = XmlProj.readNodeData("Inputs", "ImportType");
    if (!nodeData.isNull() || !nodeData.isEmpty())
    {
        volType = nodeData.toInt();
        if (volType == 1)
        {
            ui->rbtn_sedVol->setChecked(true);
            ui->rbtn_sedProp->setChecked(false);
        }
        else if (volType == 2)
        {
            ui->rbtn_sedVol->setChecked(false);
            ui->rbtn_sedProp->setChecked(true);
        }
    }

}

void DialogInputs::writeParamsToXML()
{
    //Read data from dialog to data members
    //dem = XmlProj.readNodeData("ProjectDirectory") + "/Inputs/01_InitialInputs/InitialDEM.tif";
    //hydroSedi = XmlProj.readNodeData("ProjectDirectory") + "/Inputs/01_InitialInputs/HydroSediGraph.txt";
    iterations = ui->intSpin_iterations->value();
    if (ui->rbtn_sedVol->isChecked())
    {
        volType = 1;
    }
    else if (ui->rbtn_sedProp->isChecked())
    {
        volType = 2;
    }
    else
    {
        volType = 0;
    }

    //Write data from members to XML
    XmlProj.writeNodeData("Inputs", "DEMPath", dem);
    XmlProj.writeNodeData("Inputs", "HydroSediPath", hydroSedi);
    XmlProj.writeNodeData("Inputs", "ModelIterations", QString::number(iterations));
    XmlProj.writeNodeData("Inputs", "ImportType", QString::number(volType));
}

void DialogInputs::on_btn_dem_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, "Select DEM");
    if (!filename.isNull() || !filename.isEmpty())
    {
        QFile file(filename);
        if (file.exists())
        {
            QFileInfo fi(file);
            ui->line_dem->setText(filename);
            XmlProj.writeNodeData("OriginalDEMPath", filename);
            XmlProj.writeNodeData("Inputs","DEMPath", filename);
            demOrig = filename;
            dem = filename;
            suffix = fi.suffix();
        }
    }
}

void DialogInputs::on_btn_hydrosedi_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, "Select Hydro/Sedi text file");
    if (!filename.isNull() || !filename.isEmpty())
    {
        QFile file(filename);
        if (file.exists())
        {
            //set filename and write to xml
            ui->line_hydrosedi->setText(filename);
            XmlProj.writeNodeData("OriginalHydroSediPath", filename);
            XmlProj.writeNodeData("Inputs", "HydroSediPath", filename);
            hydroSediOrig = filename;
            hydroSedi = filename;

            //read file and display data on graph
            MORPH::MORPHEDBaseClass::loadInputText(hydroSediOrig, date, q, dswe, sedi, iterations);
            ui->plot_hydroSedi->graph(0)->setData(date, q);
            ui->plot_hydroSedi->graph(1)->setData(date, sedi);
            ui->plot_hydroSedi->xAxis->setTickLabelType((QCPAxis::ltDateTime));
            ui->plot_hydroSedi->xAxis->setDateTimeFormat("MM/dd\nyyyy");
            ui->plot_hydroSedi->xAxis->setAutoTickStep(true);
            ui->plot_hydroSedi->xAxis->setRange(date.first()-24*3600, date.last()+24*3600);
            ui->plot_hydroSedi->yAxis->setAutoTickStep(true);
            ui->plot_hydroSedi->yAxis2->setAutoTickStep(true);
            ui->plot_hydroSedi->yAxis->setRange(0, MORPH::MORPHEDBaseClass::findMaxVector(q,iterations) + MORPH::MORPHEDBaseClass::findMaxVector(q,iterations)*0.05);
            ui->plot_hydroSedi->yAxis2->setRange(0, MORPH::MORPHEDBaseClass::findMaxVector(sedi,iterations) + MORPH::MORPHEDBaseClass::findMaxVector(sedi,iterations)*0.05);
        }
    }
}

void DialogInputs::on_btn_grainSize_clicked()
{
    DialogGrainSize dialog(XmlProj, this);
    dialog.setModal(true);
    dialog.exec();
}

void DialogInputs::on_btn_ok_clicked()
{
    writeParamsToXML();
    XmlProj.printXML();
    update = true;
    this->close();
}

void DialogInputs::on_btn_cancel_clicked()
{
    this->close();
}

void DialogInputs::on_intSpin_iterations_valueChanged(int arg1)
{
    iterations = arg1;
}
