#include "dialogoutputs.h"
#include "ui_dialogoutputs.h"

DialogOutputs::DialogOutputs(MORPH::XMLReadWrite &XmlObj, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogOutputs)
{
    ui->setupUi(this);

    XmlProj = XmlObj;

    setParamsFromXML();
}

DialogOutputs::~DialogOutputs()
{
    delete ui;
}

bool DialogOutputs::getUpdate()
{
    return update;
}

void DialogOutputs::setParamsFromDialog()
{
    if (ui->rbtn_fsFinal->isChecked())
    {
        fsType = 1;
    }
    else if (ui->rbtn_fsEvery->isChecked())
    {
        fsType = 2;
    }
    else if (ui->rbtn_fsInterval->isChecked())
    {
        fsType = 3;
        fsTs = ui->intSpin_fsInterval->value();
    }
    else
    {

    }

    if (ui->rbtn_ssFinal->isChecked())
    {
        ssType = 1;
    }
    else if (ui->rbtn_ssEvery->isChecked())
    {
        ssType = 2;
    }
    else if (ui->rbtn_ssInterval->isChecked())
    {
        ssType = 3;
        ssTs = ui->intSpin_ssInterval->value();
    }
    else
    {

    }

    dem = ui->chbx_dem->isChecked();
    dodTotal = ui->chbx_dodTotal->isChecked();
    dodRecent = ui->chbx_dodRecent->isChecked();
    grain = ui->chbx_grainSize->isChecked();
    veg = ui->chbx_veg->isChecked();
    morpho = ui->chbx_morph->isChecked();
    slope = ui->chbx_slope->isChecked();
    readme = ui->chbx_readme->isChecked();
    depth = ui->chbx_depth->isChecked();
    shear = ui->chbx_shear->isChecked();
    xvel = ui->chbx_xvel->isChecked();
    yvel = ui->chbx_yvel->isChecked();
}

void DialogOutputs::setParamsFromXML()
{
    fsType = XmlProj.readNodeData("Outputs", "FullType").toInt();
    if (fsType == 1)
    {
        ui->rbtn_fsFinal->setChecked(true);
    }
    else if (fsType == 2)
    {
        ui->rbtn_fsEvery->setChecked(true);
    }
    else if (fsType == 3)
    {
        ui->rbtn_fsInterval->setChecked(true);
        fsTs = XmlProj.readNodeData("Outputs", "FullInterval").toInt();
        ui->intSpin_fsInterval->setValue(fsTs);
    }

    ssType = XmlProj.readNodeData("Outputs", "SparseType").toInt();
    if (ssType == 1)
    {
        ui->rbtn_ssFinal->setChecked(true);
    }
    else if (ssType == 2)
    {
        ui->rbtn_ssEvery->setChecked(true);
    }
    else if (ssType == 3)
    {
        ui->rbtn_ssInterval->setChecked(true);
        ssTs = XmlProj.readNodeData("Outputs", "SparseInterval").toInt();
        ui->intSpin_ssInterval->setValue(ssTs);
    }

    dem = XmlProj.readNodeData("Outputs", "DEM").toInt();
    dodTotal = XmlProj.readNodeData("Outputs", "DoDCumulative").toInt();
    dodRecent = XmlProj.readNodeData("Outputs", "DoDSinceLast").toInt();
    veg = XmlProj.readNodeData("Outputs", "Veg").toInt();
    slope = XmlProj.readNodeData("Outputs", "Slope").toInt();
    grain = XmlProj.readNodeData("Outputs", "GrainSize").toInt();
    morpho = XmlProj.readNodeData("Outputs", "MorphUnits").toInt();
    readme = XmlProj.readNodeData("Outputs", "ReadMe").toInt();
    depth = XmlProj.readNodeData("Outputs", "WaterDepth").toInt();
    shear = XmlProj.readNodeData("Outputs", "Shear").toInt();
    xvel = XmlProj.readNodeData("Outputs", "XVel").toInt();
    yvel = XmlProj.readNodeData("Outputs", "YVel").toInt();
    ui->chbx_dem->setChecked(dem);
    ui->chbx_dodTotal->setChecked(dodTotal);
    ui->chbx_dodRecent->setChecked(dodRecent);
    ui->chbx_veg->setChecked(veg);
    ui->chbx_slope->setChecked(slope);
    ui->chbx_grainSize->setChecked(grain);
    ui->chbx_morph->setChecked(morpho);
    ui->chbx_readme->setChecked(readme);
    ui->chbx_depth->setChecked(depth);
    ui->chbx_shear->setChecked(shear);
    ui->chbx_xvel->setChecked(xvel);
    ui->chbx_yvel->setChecked(yvel);
}

void DialogOutputs::writeParamsToXML()
{
    XmlProj.writeNodeData("Outputs", "FullType", QString::number(fsType));
    XmlProj.writeNodeData("Outputs", "SparseType", QString::number(ssType));
    XmlProj.writeNodeData("Outputs", "FullInterval", QString::number(fsTs));
    XmlProj.writeNodeData("Outputs", "SparseInterval", QString::number(ssTs));
    XmlProj.writeNodeData("Outputs", "DEM", QString::number(dem));
    XmlProj.writeNodeData("Outputs", "DoDCumulative", QString::number(dodTotal));
    XmlProj.writeNodeData("Outputs", "DoDSinceLast", QString::number(dodRecent));
    XmlProj.writeNodeData("Outputs", "GrainSize", QString::number(grain));
    XmlProj.writeNodeData("Outputs", "Veg", QString::number(veg));
    XmlProj.writeNodeData("Outputs", "Slope", QString::number(slope));
    XmlProj.writeNodeData("Outputs", "MorphUnits", QString::number(morpho));
    XmlProj.writeNodeData("Outputs", "ReadMe", QString::number(readme));
    XmlProj.writeNodeData("Outputs", "WaterDepth", QString::number(depth));
    XmlProj.writeNodeData("Outputs", "ShearStress", QString::number(shear));
    XmlProj.writeNodeData("Outputs", "XVel", QString::number(xvel));
    XmlProj.writeNodeData("Outputs", "YVel", QString::number(yvel));
}

void DialogOutputs::on_btn_ok_clicked()
{
    setParamsFromDialog();
    writeParamsToXML();
    XmlProj.printXML();
    update = true;
    this->close();
}
