#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "delft3dio.h"
#include "dialogaboutmorphed.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    GDALAllRegister();
    pDataset = NULL;
    statusLabel = new QLabel(this);
    ui->statusBar->addPermanentWidget(statusLabel);
    statusLabel->setText("Ready");
}

MainWindow::~MainWindow()
{
    delete ui;
    if (pDataset != NULL)
    {
        GDALClose(pDataset);
    }
    morphThread.quit();
    morphThread.wait();
}

void MainWindow::on_btn_Description_clicked()
{
    DialogDescription dialog(XmlGui, this);
    dialog.setModal(true);
    dialog.exec();
}

void MainWindow::on_btn_Inputs_clicked()
{
    QString dem;
    DialogInputs dialog(XmlGui, this);
    dialog.setModal(true);
    dialog.exec();

    if (dialog.getUpdate())
    {
        XmlGui.printXML();
        dem = XmlGui.readNodeData("OriginalDEMPath");
        XmlGui.writeRasterProperties(dem);
        pDataset = (GDALDataset*) GDALOpen(dem.toStdString().c_str(), GA_ReadOnly);
        XmlGui.writeRasterProperties(pDataset);
        iterations = dialog.getIterations();
        XmlGui.printXML();
    }
}

void MainWindow::on_btn_morphParams_clicked()
{
    DialogMorphParams dialog(XmlGui, this);
    dialog.setModal(true);
    dialog.exec();
}

void MainWindow::on_btn_delftParams_clicked()
{
    DialogDelftParams dialog(XmlGui, this);
    dialog.setModal(true);
    dialog.exec();
}

void MainWindow::on_btn_Outputs_clicked()
{
    DialogOutputs dialog(XmlGui, this);
    dialog.setModal(true);
    dialog.exec();
}

void MainWindow::on_btn_run_clicked()
{
//    MORPH::Delft3DIO *delft = new MORPH::Delft3DIO(XmlGui);
//    MORPH::FileManager::createBaseDirectories(XmlGui.readNodeData("ProjectDirectory"));
//    MORPH::FileManager::createFloodDirectories(XmlGui.readNodeData("ProjectDirectory"),1);
//    delft->run();
//    delete delft;
    MORPH::MORPHEDModel *MorphModel = new MORPH::MORPHEDModel(XmlGui);
    std::cout<<"ObjectCreated\n";
    MorphModel->setup(morphThread);
    MorphModel->moveToThread(&morphThread);
    connect(&morphThread, SIGNAL(finished()), MorphModel, SLOT(deleteLater()));
    connect(&morphThread, SIGNAL(finished()), this, SLOT(threadFinished()));
    connect(MorphModel, SIGNAL(updateStatus(QString)), this, SLOT(morphUpdated(QString)));
    //connect(this, &MainWindow::runMorphed, MorphModel, &MORPH::MORPHEDModel::runOnThread);
    morphThread.start();
}

void MainWindow::on_btn_viewer_clicked()
{
    outputWindow = new WindowViewer(this);
    outputWindow->show();
}

void MainWindow::on_actionNew_Project_triggered()
{
    QString filename, name;
    QStringList list;
    filename = QFileDialog::getExistingDirectory(this, "Select or Create Project Folder");

    if (filename.isNull() || filename.isEmpty())
    {
        QMessageBox::information(this, "Empty or Null Path", "The directory path is null or empty, you must select a valid diretory to continue");
    }
    else
    {
        XmlGui.writeXMLdocGUI();
        XmlGui.writeNodeData("ProjectDirectory",filename);
        list = filename.split("/");
        name = list[list.size()-1];
        filenameXml = filename + "/" + name + ".morph";
        XmlGui.setDocumentFilename(filenameXml);
    }
}

void MainWindow::on_actionOpen_Project_triggered()
{
    QString filename;
    filename = QFileDialog::getOpenFileName(this, "Select *.morph file to open");

    if (filename.isNull() || filename.isEmpty())
    {
        QMessageBox::information(this, "Empty or Null Path", "The file path is null or empty, you must select a valid *.morph file to continue");
    }
    else
    {
        XmlGui.loadDocument(filename, 1);
    }
}

void MainWindow::threadFinished()
{
    std::cout<<"Thread finished"<<std::cout<<std::endl;
}

void MainWindow::morphUpdated(const QString &result)
{
    statusLabel->setText(result);
    std::cout<<result.toStdString()<<std::endl;
}

void MainWindow::on_actionExit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_btn_about_clicked()
{
    dialogaboutmorphed about;
    about.setModal(true);
    about.exec();
}
