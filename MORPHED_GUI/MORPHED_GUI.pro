#-------------------------------------------------
#
# Project created by QtCreator 2014-07-21T13:27:38
#
#-------------------------------------------------

QT       += core gui
QT       += xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = MORPHED_GUI
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dialogdelftparams.cpp \
    dialogmorphparams.cpp \
    dialogdescription.cpp \
    dialoginputs.cpp \
    dialoggrainsize.cpp \
    dialogoutputs.cpp \
    windowviewer.cpp \
    qcustomplot.cpp \
    customgraphicsview.cpp \
    piechartwidget.cpp \
    dialoganimate.cpp \
    dialogaboutmorphed.cpp

HEADERS  += mainwindow.h \
    dialogdelftparams.h \
    dialogmorphparams.h \
    dialogdescription.h \
    dialoginputs.h \
    dialoggrainsize.h \
    dialogoutputs.h \
    windowviewer.h \
    qcustomplot.h \
    customgraphicsview.h \
    piechartwidget.h \
    dialoganimate.h \
    dialogaboutmorphed.h

FORMS    += mainwindow.ui \
    dialogdelftparams.ui \
    dialogmorphparams.ui \
    dialogdescription.ui \
    dialoginputs.ui \
    dialoggrainsize.ui \
    dialogoutputs.ui \
    windowviewer.ui \
    dialoganimate.ui \
    dialogaboutmorphed.ui

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../MORPHED_GUTS/release/ -lMORPHED_GUTS
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../MORPHED_GUTS/debug/ -lMORPHED_GUTS
else:unix: LIBS += -L$$OUT_PWD/../MORPHED_GUTS/ -lMORPHED_GUTS

INCLUDEPATH += $$PWD/../MORPHED_GUTS
DEPENDPATH += $$PWD/../MORPHED_GUTS

unix|win32: LIBS += -L$$PWD/../../../../../MinGW/msys/1.0/local/lib/ -llibgdal

INCLUDEPATH += $$PWD/../../../../../MinGW/msys/1.0/local/include
DEPENDPATH += $$PWD/../../../../../MinGW/msys/1.0/local/include
