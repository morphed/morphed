#ifndef DIALOGOUTPUTS_H
#define DIALOGOUTPUTS_H

#include <QDialog>
#include "xmlreadwrite.h"

namespace Ui {
class DialogOutputs;
}

class DialogOutputs : public QDialog
{
    Q_OBJECT

public:
    explicit DialogOutputs(MORPH::XMLReadWrite &XmlObj, QWidget *parent = 0);
    ~DialogOutputs();

    bool getUpdate();
    void setParamsFromDialog();
    void setParamsFromXML();
    void writeParamsToXML();

private slots:
    void on_btn_ok_clicked();

private:
    Ui::DialogOutputs *ui;

    MORPH::XMLReadWrite XmlProj;

    bool update, dem, dodTotal, dodRecent, slope, grain, morpho, veg, readme, depth, shear, xvel, yvel;
    int fsType, ssType, fsTs, ssTs;
};

#endif // DIALOGOUTPUTS_H
