#ifndef DIALOGABOUTMORPHED_H
#define DIALOGABOUTMORPHED_H

#include <QDialog>

namespace Ui {
class dialogaboutmorphed;
}

class dialogaboutmorphed : public QDialog
{
    Q_OBJECT

public:
    explicit dialogaboutmorphed(QWidget *parent = 0);
    ~dialogaboutmorphed();

private:
    Ui::dialogaboutmorphed *ui;
};

#endif // DIALOGABOUTMORPHED_H
