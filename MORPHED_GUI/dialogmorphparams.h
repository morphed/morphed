#ifndef DIALOGMORPHPARAMS_H
#define DIALOGMORPHPARAMS_H

#include <QDialog>
#include "morphedbaseclass.h"

namespace Ui {
class DialogMorphParams;
}

class DialogMorphParams : public QDialog
{
    Q_OBJECT

public:
    explicit DialogMorphParams(MORPH::XMLReadWrite &XmlObj, QWidget *parent = 0);
    ~DialogMorphParams();

    bool getUpdate();
    void setParamsFromDialog();
    void setParamsFromXML();
    void writeParamsToXML();

private slots:
    void on_rbtn_exp_clicked();

    void on_rbtn_gaus_clicked();

    void on_rbtn_custom_clicked();

    void on_btn_update_clicked();

    void on_btn_ok_clicked();

    void on_btn_cancel_clicked();

    void on_dblSpin_length_valueChanged(double arg1);

private:
    Ui::DialogMorphParams *ui;

    MORPH::XMLReadWrite XmlProj;
    bool update;
    int type;
    double siga, mub, length, shear, slope, cellSize;
    QVector<double> x, y;

    void setX();
};

#endif // DIALOGMORPHPARAMS_H
