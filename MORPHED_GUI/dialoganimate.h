#ifndef DIALOGANIMATE_H
#define DIALOGANIMATE_H

#include <QDialog>

namespace Ui {
class DialogAnimate;
}

class DialogAnimate : public QDialog
{
    Q_OBJECT

public:
    explicit DialogAnimate(int advanceSecs, QWidget *parent = 0);
    ~DialogAnimate();

    bool getUpdate();
    int getSeconds();

private slots:
    void on_btn_cancel_clicked();

    void on_btn_ok_clicked();

private:
    Ui::DialogAnimate *ui;

    int seconds;
    bool update;
};

#endif // DIALOGANIMATE_H
