#ifndef DIALOGINPUTS_H
#define DIALOGINPUTS_H

#include <QDialog>
#include "xmlreadwrite.h"
#include "dialoggrainsize.h"
#include "morphedbaseclass.h"

namespace Ui {
class DialogInputs;
}

class DialogInputs : public QDialog
{
    Q_OBJECT

public:
    explicit DialogInputs(MORPH::XMLReadWrite &XmlObj, QWidget *parent = 0);
    ~DialogInputs();

    int getIterations();
    QString getOrigDEMPath();
    bool getUpdate();
    void setParamsFromXML();
    void writeParamsToXML();

private slots:
    void on_btn_dem_clicked();

    void on_btn_hydrosedi_clicked();

    void on_btn_grainSize_clicked();

    void on_btn_ok_clicked();

    void on_btn_cancel_clicked();

    void on_intSpin_iterations_valueChanged(int arg1);

private:
    Ui::DialogInputs *ui;

    bool update;
    MORPH::XMLReadWrite XmlProj;
    QVector<double> q, sedi, date, dswe;
    int iterations, volType;
    QString dem, demOrig, hydroSediOrig, hydroSedi, suffix;
};

#endif // DIALOGINPUTS_H
