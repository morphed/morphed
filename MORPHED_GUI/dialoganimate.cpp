#include "dialoganimate.h"
#include "ui_dialoganimate.h"

DialogAnimate::DialogAnimate(int advanceSecs, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogAnimate)
{
    ui->setupUi(this);
    seconds = advanceSecs;
    ui->intSpin_secs->setValue(seconds);
    update = false;
}

DialogAnimate::~DialogAnimate()
{
    delete ui;
}

bool DialogAnimate::getUpdate()
{
    return update;
}

int DialogAnimate::getSeconds()
{
    return seconds;
}

void DialogAnimate::on_btn_cancel_clicked()
{
    update = false;
    this->close();
}

void DialogAnimate::on_btn_ok_clicked()
{
    update = true;
    this->close();
}
