#include "dialogdescription.h"
#include "ui_dialogdescription.h"

DialogDescription::DialogDescription(MORPH::XMLReadWrite &XmlObj, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogDescription)
{
    ui->setupUi(this);
    update = false;
    XmlProj = XmlObj;
    setParamsFromXML();
}

DialogDescription::~DialogDescription()
{
    delete ui;
}

bool DialogDescription::getUpdate()
{
    return update;
}

void DialogDescription::setParamsFromXML()
{
    QString nodeData;
    nodeData = XmlProj.readNodeData("Description", "Reach");
    if (!nodeData.isNull() || !nodeData.isEmpty())
    {
        reach = nodeData;
        ui->line_reach->setText(reach);
    }
    nodeData = XmlProj.readNodeData("Description", "Date");
    if (!nodeData.isNull() || !nodeData.isEmpty())
    {
        date = nodeData;
        ui->line_date->setText(date);
    }
    nodeData = XmlProj.readNodeData("Description", "DatesModeled");
    if (!nodeData.isNull() || !nodeData.isEmpty())
    {
        dateModeled = nodeData;
        ui->line_datesModeled->setText(dateModeled);
    }
    nodeData = XmlProj.readNodeData("Description", "DescriptionDetail");
    if (!nodeData.isNull() || !nodeData.isEmpty())
    {
        desc = nodeData;
        ui->txt_desc->setText(desc);
    }
}

void DialogDescription::writeParamsToXML()
{
    //Read data from dialog and assign to data members
    reach = ui->line_reach->text();
    date = ui->line_date->text();
    dateModeled = ui->line_datesModeled->text();
    desc = ui->txt_desc->toPlainText();

    //Write data members to XML
    XmlProj.writeNodeData("Description","Reach", reach);
    XmlProj.writeNodeData("Description","Date", date);
    XmlProj.writeNodeData("Description","DatesModeled", dateModeled);
    XmlProj.writeNodeData("Description","DescriptionDetail", desc);
}

void DialogDescription::on_btn_ok_clicked()
{
    update = true;
    writeParamsToXML();
    XmlProj.printXML();
    this->close();
}

void DialogDescription::on_btn_cancel_clicked()
{
    this->close();
}
