#include "dialogdelftparams.h"
#include "ui_dialogdelftparams.h"

DialogDelftParams::DialogDelftParams(MORPH::XMLReadWrite &XmlObj, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogDelftParams)
{
    ui->setupUi(this);

    XmlProj = XmlObj;

    disableDSLineEdits();
    disableUSLineEdits();
    setParamsFromXML();
}

DialogDelftParams::~DialogDelftParams()
{
    delete ui;
}

bool DialogDelftParams::getUpdate()
{
    return update;
}

void DialogDelftParams::disableDSLineEdits()
{
    ui->line_dsX1->setEnabled(false);
    ui->line_dsY1->setEnabled(false);
    ui->line_dsX2->setEnabled(false);
    ui->line_dsY2->setEnabled(false);
}

void DialogDelftParams::disableUSLineEdits()
{
    ui->line_usX1->setEnabled(false);
    ui->line_usY1->setEnabled(false);
    ui->line_usX2->setEnabled(false);
    ui->line_usY2->setEnabled(false);
}

void DialogDelftParams::enableDSLineEdits()
{
    ui->line_dsX1->setEnabled(true);
    ui->line_dsY1->setEnabled(true);
    ui->line_dsX2->setEnabled(true);
    ui->line_dsY2->setEnabled(true);
}

void DialogDelftParams::enableUSLineEdits()
{
    ui->line_usX1->setEnabled(true);
    ui->line_usY1->setEnabled(true);
    ui->line_usX2->setEnabled(true);
    ui->line_usY2->setEnabled(true);
}

void DialogDelftParams::setParamsFromDialog()
{
    delftPath = ui->line_delftPath->text();
    usX1 = ui->line_usX1->text().toDouble();
    usY1 = ui->line_usY1->text().toDouble();
    usX2 = ui->line_usX2->text().toDouble();
    usY2 = ui->line_usY2->text().toDouble();
    dsX1 = ui->line_dsX1->text().toDouble();
    dsY1 = ui->line_dsY1->text().toDouble();
    dsX2 = ui->line_dsX2->text().toDouble();
    dsY2 = ui->line_dsY2->text().toDouble();
    simTime = ui->dblSpin_simTime->value();
    timeStep = ui->dblSpin_timeStep->value();
    roughness = ui->dblSpin_roughness->value();
    HEV = ui->dblSpin_hev->value();
    if (ui->rbtn_usN->isChecked())
    {
        usLoc = 1;
    }
    else if (ui->rbtn_usS->isChecked())
    {
        usLoc = 2;
    }
    else if (ui->rbtn_usE->isChecked())
    {
        usLoc = 3;
    }
    else if (ui->rbtn_usW->isChecked())
    {
        usLoc = 4;
    }
    else
    {

    }

    if (ui->rbtn_dsN->isChecked())
    {
        dsLoc = 1;
    }
    else if (ui->rbtn_dsS->isChecked())
    {
        dsLoc = 2;
    }
    else if (ui->rbtn_dsE->isChecked())
    {
        dsLoc = 3;
    }
    else if (ui->rbtn_dsW->isChecked())
    {
        dsLoc = 4;
    }
    else
    {

    }
}

void DialogDelftParams::setParamsFromXML()
{
    delftPath = XmlProj.readNodeData("Delft3DParameters", "DelftPath");
    ui->line_delftPath->setText(delftPath);
    usX1 = XmlProj.readNodeData("Delft3DParameters", "USX1").toDouble();
    ui->line_usX1->setText(QString::number(usX1, 'f', 5));
    usY1 = XmlProj.readNodeData("Delft3DParameters", "USY1").toDouble();
    ui->line_usY1->setText(QString::number(usY1, 'f', 5));
    usX2 = XmlProj.readNodeData("Delft3DParameters", "USX2").toDouble();
    ui->line_usX2->setText(QString::number(usX2, 'f', 5));
    usY2 = XmlProj.readNodeData("Delft3DParameters", "USY2").toDouble();
    ui->line_usY2->setText(QString::number(usY2, 'f', 5));
    dsX1 = XmlProj.readNodeData("Delft3DParameters", "DSX1").toDouble();
    ui->line_dsX1->setText(QString::number(dsX1, 'f', 5));
    dsY1 = XmlProj.readNodeData("Delft3DParameters", "DSY1").toDouble();
    ui->line_dsY1->setText(QString::number(dsY1, 'f', 5));
    dsX2 = XmlProj.readNodeData("Delft3DParameters", "DSX2").toDouble();
    ui->line_dsX2->setText(QString::number(dsX2, 'f', 5));
    dsY2 = XmlProj.readNodeData("Delft3DParameters", "DSY2").toDouble();
    ui->line_dsY2->setText(QString::number(dsY2, 'f', 5));

    simTime = XmlProj.readNodeData("Delft3DParameters", "SimTime").toDouble();
    if (simTime <= 0)
    {
        simTime = 60.0;
    }
    ui->dblSpin_simTime->setValue(simTime);
    timeStep = XmlProj.readNodeData("Delft3DParameters", "TimeStep").toDouble();
    if (timeStep <= 0)
    {
        timeStep = 0.025;
    }
    ui->dblSpin_timeStep->setValue(timeStep);
    roughness = XmlProj.readNodeData("Delft3DParameters", "Roughness").toDouble();
    if (roughness <= 0)
    {
        roughness = 0.03;
    }
    ui->dblSpin_roughness->setValue(roughness);
    HEV = XmlProj.readNodeData("Delft3DParameters", "HEV").toDouble();
    if (HEV <= 0)
    {
        HEV = 0.01;
    }
    ui->dblSpin_hev->setValue(HEV);
    xtl = XmlProj.readNodeData("DEMProperties", "TopLeftX").toDouble();
    ytl = XmlProj.readNodeData("DEMProperties", "TopLeftY").toDouble();
    cellWidth = XmlProj.readNodeData("DEMProperties", "CellWidth").toDouble();
    rows = XmlProj.readNodeData("DEMProperties", "Rows").toInt();
    cols = XmlProj.readNodeData("DEMProperties", "Cols").toInt();

    usLoc = XmlProj.readNodeData("Delft3DParameters", "USBoundLocation").toInt();
    if (usLoc == 1)
    {
        ui->rbtn_usN->setChecked(true);
        on_rbtn_usN_clicked();
    }
    else if (usLoc == 2)
    {
        ui->rbtn_usS->setChecked(true);
        on_rbtn_usS_clicked();
    }
    else if (usLoc == 3)
    {
        ui->rbtn_usE->setChecked(true);
        on_rbtn_usE_clicked();
    }
    else if (usLoc == 4)
    {
        ui->rbtn_usW->setChecked(true);
        on_rbtn_usW_clicked();
    }
    else
    {

    }

    dsLoc = XmlProj.readNodeData("Delft3DParameters", "DSBoundLocation").toInt();
    if (dsLoc == 1)
    {
        ui->rbtn_dsN->setChecked(true);
        on_rbtn_dsN_clicked();
    }
    else if (dsLoc == 2)
    {
        ui->rbtn_dsS->setChecked(true);
        on_rbtn_dsS_clicked();
    }
    else if (dsLoc == 3)
    {
        ui->rbtn_dsE->setChecked(true);
        on_rbtn_dsE_clicked();
    }
    else if (dsLoc == 4)
    {
        ui->rbtn_dsW->setChecked(true);
        on_rbtn_dsW_clicked();
    }
    else
    {

    }
}

void DialogDelftParams::writeParamsToXML()
{
    XmlProj.writeNodeData("Delft3DParameters", "DelftPath", delftPath);
    XmlProj.writeNodeData("Delft3DParameters", "USBoundLocation", QString::number(usLoc));
    XmlProj.writeNodeData("Delft3DParameters", "USX1", QString::number(usX1,'f',5));
    XmlProj.writeNodeData("Delft3DParameters", "USY1", QString::number(usY1,'f',5));
    XmlProj.writeNodeData("Delft3DParameters", "USX2", QString::number(usX2,'f',5));
    XmlProj.writeNodeData("Delft3DParameters", "USY2", QString::number(usY2,'f',5));
    XmlProj.writeNodeData("Delft3DParameters", "DSBoundLocation", QString::number(dsLoc));
    XmlProj.writeNodeData("Delft3DParameters", "DSX1", QString::number(dsX1,'f',5));
    XmlProj.writeNodeData("Delft3DParameters", "DSY1", QString::number(dsY1,'f',5));
    XmlProj.writeNodeData("Delft3DParameters", "DSX2", QString::number(dsX2,'f',5));
    XmlProj.writeNodeData("Delft3DParameters", "DSY2", QString::number(dsY2,'f',5));
    XmlProj.writeNodeData("Delft3DParameters", "SimTime", QString::number(simTime));
    XmlProj.writeNodeData("Delft3DParameters", "TimeStep", QString::number(timeStep));
    XmlProj.writeNodeData("Delft3DParameters", "Roughness", QString::number(roughness));
    XmlProj.writeNodeData("Delft3DParameters", "HEV", QString::number(HEV));
}

void DialogDelftParams::on_btn_delftPath_clicked()
{
    QString filename = QFileDialog::getExistingDirectory(this, "Select Delft3D path");
    delftPath = filename;
    ui->line_delftPath->setText(delftPath);
}

void DialogDelftParams::on_btn_ok_clicked()
{
    setParamsFromDialog();
    writeParamsToXML();
    XmlProj.printXML();
    this->close();
}

void DialogDelftParams::on_btn_cancel_clicked()
{
    this->close();
}

void DialogDelftParams::on_rbtn_usN_clicked()
{
    enableUSLineEdits();
    ui->line_usY2->setText(ui->line_usY1->text());
    ui->line_usY2->setEnabled(false);
    connect(ui->line_usY1, SIGNAL(textChanged(QString)), ui->line_usY2, SLOT(setText(QString)));
    disconnect(ui->line_usX1, SIGNAL(textChanged(QString)), ui->line_usX2, SLOT(setText(QString)));
}

void DialogDelftParams::on_rbtn_usS_clicked()
{
    enableUSLineEdits();
    ui->line_usY2->setText(ui->line_usY1->text());
    ui->line_usY2->setEnabled(false);
    connect(ui->line_usY1, SIGNAL(textChanged(QString)), ui->line_usY2, SLOT(setText(QString)));
    disconnect(ui->line_usX1, SIGNAL(textChanged(QString)), ui->line_usX2, SLOT(setText(QString)));
}

void DialogDelftParams::on_rbtn_usE_clicked()
{
    enableUSLineEdits();
    ui->line_usX2->setText(ui->line_usX1->text());
    ui->line_usX2->setEnabled(false);
    connect(ui->line_usX1, SIGNAL(textChanged(QString)), ui->line_usX2, SLOT(setText(QString)));
    disconnect(ui->line_usY1, SIGNAL(textChanged(QString)), ui->line_usY2, SLOT(setText(QString)));
}

void DialogDelftParams::on_rbtn_usW_clicked()
{
    enableUSLineEdits();
    ui->line_usX2->setText(ui->line_usX1->text());
    ui->line_usX2->setEnabled(false);
    connect(ui->line_usX1, SIGNAL(textChanged(QString)), ui->line_usX2, SLOT(setText(QString)));
    disconnect(ui->line_usY1, SIGNAL(textChanged(QString)), ui->line_usY2, SLOT(setText(QString)));
}

void DialogDelftParams::on_rbtn_dsN_clicked()
{
    enableDSLineEdits();
    ui->line_dsX1->clear();
    ui->line_dsX2->clear();
    ui->line_dsY1->setText(QString::number(ytl,'f',5));
    ui->line_dsY2->setText(ui->line_dsY1->text());
    ui->line_dsY1->setEnabled(false);
    ui->line_dsY2->setEnabled(false);
    connect(ui->line_dsY1, SIGNAL(textChanged(QString)), ui->line_dsY2, SLOT(setText(QString)));
    disconnect(ui->line_dsX1, SIGNAL(textChanged(QString)), ui->line_dsX2, SLOT(setText(QString)));
}

void DialogDelftParams::on_rbtn_dsS_clicked()
{
    enableDSLineEdits();
    ui->line_dsX1->clear();
    ui->line_dsX2->clear();
    ui->line_dsY1->setText(QString::number(ytl - (rows*cellWidth),'f',5));
    ui->line_dsY2->setText(ui->line_dsY1->text());
    ui->line_dsY1->setEnabled(false);
    ui->line_dsY2->setEnabled(false);
    connect(ui->line_dsY1, SIGNAL(textChanged(QString)), ui->line_dsY2, SLOT(setText(QString)));
    disconnect(ui->line_dsX1, SIGNAL(textChanged(QString)), ui->line_dsX2, SLOT(setText(QString)));
}

void DialogDelftParams::on_rbtn_dsE_clicked()
{
    enableDSLineEdits();
    ui->line_dsY1->clear();
    ui->line_dsY2->clear();
    ui->line_dsX1->setText(QString::number(xtl + (cols*cellWidth),'f',5));
    ui->line_dsX2->setText(ui->line_dsX1->text());
    ui->line_dsX1->setEnabled(false);
    ui->line_dsX2->setEnabled(false);
    connect(ui->line_dsX1, SIGNAL(textChanged(QString)), ui->line_dsX2, SLOT(setText(QString)));
    disconnect(ui->line_dsY1, SIGNAL(textChanged(QString)), ui->line_dsY2, SLOT(setText(QString)));
}

void DialogDelftParams::on_rbtn_dsW_clicked()
{
    enableDSLineEdits();
    ui->line_dsY1->clear();
    ui->line_dsY2->clear();
    ui->line_dsX1->setText(QString::number(xtl,'f',5));
    ui->line_dsX2->setText(ui->line_dsX1->text());
    ui->line_dsX1->setEnabled(false);
    ui->line_dsX2->setEnabled(false);
    connect(ui->line_dsX1, SIGNAL(textChanged(QString)), ui->line_dsX2, SLOT(setText(QString)));
    disconnect(ui->line_dsY1, SIGNAL(textChanged(QString)), ui->line_dsY2, SLOT(setText(QString)));
}
