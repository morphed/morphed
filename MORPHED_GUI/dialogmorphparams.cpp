#include "dialogmorphparams.h"
#include "ui_dialogmorphparams.h"

DialogMorphParams::DialogMorphParams(MORPH::XMLReadWrite &XmlObj, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogMorphParams)
{
    ui->setupUi(this);

    XmlProj = XmlObj;
    setParamsFromXML();

    ui->dblSpin_slope->setValue(slope);
    ui->dblSpin_shear->setValue(shear);

    if (type == 1)
    {
        ui->rbtn_exp->setChecked(true);
        ui->dblSpin_a->setValue(siga);
        ui->dblSpin_b->setValue(mub);
        ui->dblSpin_length->setValue(length);
    }
    else if (type == 2)
    {
        ui->rbtn_gaus->setChecked(true);
        ui->dblSpin_sig->setValue(siga);
        ui->dblSpin_mu->setValue(mub);
        ui->dblSpin_length->setValue(length);
    }

    if (!ui->rbtn_exp->isChecked())
    {
        ui->dblSpin_a->setEnabled(false);
        ui->dblSpin_b->setEnabled(false);
    }
    if (!ui->rbtn_gaus->isChecked())
    {
        ui->dblSpin_sig->setEnabled(false);
        ui->dblSpin_mu->setEnabled(false);
    }
    ui->rbtn_custom->setEnabled(false);
    ui->line_custom->setEnabled(false);
    ui->btn_custom->setEnabled(false);
    if (ui->dblSpin_length->value() <= 0.0 || (!ui->rbtn_exp->isChecked() && !ui->rbtn_gaus->isChecked()))
    {
        ui->btn_update->setEnabled(false);
    }

    ui->plot->xAxis->setLabel("Cells from Origin");
    ui->plot->yAxis->setLabel("Proportion of Sediment Depositied");
    ui->plot->yAxis->setRange(0,1);
    ui->plot->xAxis->setRange(0,10);
    ui->plot->xAxis->setAutoTickStep(false);
    ui->plot->xAxis->setTickStep(1);
    ui->plot->yAxis->setAutoTickStep(false);
    ui->plot->yAxis->setAutoSubTicks(false);
    ui->plot->yAxis->setTickStep(0.1);
    ui->plot->yAxis->setSubTickCount(1);
}

DialogMorphParams::~DialogMorphParams()
{
    delete ui;
}

bool DialogMorphParams::getUpdate()
{
    return update;
}

void DialogMorphParams::setParamsFromDialog()
{
    length = ui->dblSpin_length->value();
    shear = ui->dblSpin_shear->value();
    slope = ui->dblSpin_slope->value();
    if (ui->rbtn_exp->isChecked())
    {
        siga = ui->dblSpin_a->value();
        mub = ui->dblSpin_b->value();
        type = 1;
    }
    else if (ui->rbtn_gaus->isChecked())
    {
        siga = ui->dblSpin_sig->value();
        mub = ui->dblSpin_mu->value();
        type = 2;
    }
}

void DialogMorphParams::setParamsFromXML()
{
    length = XmlProj.readNodeData("MorphedParameters", "DistributionLength").toDouble();
    cellSize = XmlProj.readNodeData("MorphedParameters", "CellSize").toDouble();
    siga = XmlProj.readNodeData("MorphedParameters", "SigA").toDouble();
    mub = XmlProj.readNodeData("MorphedParameters", "MuB").toDouble();
    shear = XmlProj.readNodeData("MorphedParameters", "BankShearThresh").toDouble();
    slope = XmlProj.readNodeData("MorphedParameters", "BankSlopeThresh").toDouble();
    type = XmlProj.readNodeData("MorphedParameters", "DistributionType").toInt();
}

void DialogMorphParams::writeParamsToXML()
{
    XmlProj.writeNodeData("MorphedParameters", "DistributionLength", QString::number(length));
    XmlProj.writeNodeData("MorphedParameters", "CellSize", QString::number(cellSize));
    XmlProj.writeNodeData("MorphedParameters", "DistributionType", QString::number(type));
    XmlProj.writeNodeData("MorphedParameters", "SigA", QString::number(siga));
    XmlProj.writeNodeData("MorphedParameters", "MuB", QString::number(mub));
    XmlProj.writeNodeData("MorphedParameters", "BankShearThresh", QString::number(shear));
    XmlProj.writeNodeData("MorphedParameters", "BankSlopeThresh", QString::number(slope));
}

void DialogMorphParams::on_rbtn_exp_clicked()
{
    ui->dblSpin_a->setEnabled(true);
    ui->dblSpin_b->setEnabled(true);
    ui->dblSpin_mu->setEnabled(false);
    ui->dblSpin_sig->setEnabled(false);
    if (ui->dblSpin_length->value() > 0.0)
    {
        ui->btn_update->setEnabled(true);
    }
}

void DialogMorphParams::on_rbtn_gaus_clicked()
{
    ui->dblSpin_a->setEnabled(false);
    ui->dblSpin_b->setEnabled(false);
    ui->dblSpin_mu->setEnabled(true);
    ui->dblSpin_sig->setEnabled(true);
    if (ui->dblSpin_length->value() > 0.0)
    {
        ui->btn_update->setEnabled(true);
    }
}

void DialogMorphParams::on_rbtn_custom_clicked()
{

}

void DialogMorphParams::on_btn_update_clicked()
{
    setParamsFromDialog();
    x.clear();
    y.clear();

    y = MORPH::MORPHEDBaseClass::getDistribution(type, siga, mub,MORPH::MORPHEDBaseClass::getLength(length, XmlProj.readNodeData("DEMProperties","CellWidth").toDouble()), XmlProj.readNodeData("DEMProperties","CellWidth").toDouble());
    setX();

    QPen pen;
    pen.setColor(Qt::green);
    pen.setWidth(2);
    ui->plot->addGraph();
    ui->plot->graph(0)->setPen(pen);
    ui->plot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDot));
    ui->plot->graph(0)->setLineStyle(QCPGraph::lsLine);
    ui->plot->graph(0)->setData(x,y);
    ui->plot->yAxis->setAutoTickStep(true);
    ui->plot->yAxis->setSubTickCount(1);
    ui->plot->xAxis->setAutoTickStep(false);
    ui->plot->xAxis->setRange(0,MORPH::MORPHEDBaseClass::findMaxVector(x, x.size()));
    ui->plot->yAxis->setRange(0,MORPH::MORPHEDBaseClass::findMaxVector(y, y.size())+MORPH::MORPHEDBaseClass::findMaxVector(y, y.size())*0.05);

    if (x.size() >= 10)
    {
        ui->plot->xAxis->setTickStep(round(x.size()/10));
    }
    else
    {
        ui->plot->xAxis->setTickStep(1);
    }

    ui->plot->xAxis->setRange(0,MORPH::MORPHEDBaseClass::findMaxVector(x, x.size())+1);
    ui->plot->replot();
    ui->plot->update();
}

void DialogMorphParams::on_btn_ok_clicked()
{
    setParamsFromDialog();
    writeParamsToXML();
    XmlProj.printXML();
    this->close();
}

void DialogMorphParams::on_btn_cancel_clicked()
{
    this->close();
}

void DialogMorphParams::on_dblSpin_length_valueChanged(double arg1)
{
    if (arg1 > 0.0 && (ui->rbtn_exp->isChecked() || ui->rbtn_gaus->isChecked()))
    {
        ui->btn_update->setEnabled(true);
    }
    else
    {
        ui->btn_update->setEnabled(false);
    }
}

void DialogMorphParams::setX()
{
    for (int i=0; i<y.size(); i++)
    {
        x.append(i+1);
    }
}
