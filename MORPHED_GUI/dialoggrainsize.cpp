#include "dialoggrainsize.h"
#include "ui_dialoggrainsize.h"

DialogGrainSize::DialogGrainSize(MORPH::XMLReadWrite &XmlObj, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogGrainSize)
{
    ui->setupUi(this);

    XmlProj = XmlObj;

    sub = 0;
    subType = 0;
    subSize = 0;

    ui->dblSpin_uniform->setDecimals(4);
    ui->dblSpin_actUniform->setDecimals(4);
    ui->dblSpin_subUniform->setDecimals(4);
    ui->label_active->setEnabled(false);
    ui->chbx_subsuf->setEnabled(false);
    ui->dblSpin_uniform->setEnabled(false);
    ui->label_uniform->setEnabled(false);
    ui->rbtn_actRaster->setEnabled(false);
    ui->rbtn_actUniform->setEnabled(false);
    ui->line_actRaster->setEnabled(false);
    ui->btn_actRaster->setEnabled(false);
    ui->line_subRaster->setEnabled(false);
    ui->rbtn_subPercent->setEnabled(false);
    ui->rbtn_subRaster->setEnabled(false);
    ui->rbtn_subUniform->setEnabled(false);
    ui->btn_subRaster->setEnabled(false);
    ui->dblSpin_subPercent->setEnabled(false);
    ui->dblSpin_subUniform->setEnabled(false);
    ui->dblSpin_actUniform->setEnabled(false);

    setParamsFromXML();
}

DialogGrainSize::~DialogGrainSize()
{
    delete ui;
}

bool DialogGrainSize::getUpdate()
{
    return update;
}

void DialogGrainSize::setParamsfromDialog()
{
    if (ui->rbtn_uniform->isChecked())
    {
        type = 1;
        actSize = ui->dblSpin_uniform->value();
        thickness = ui->dblSpin_thickness->value();
        actType = 1;
        subType = 0;
    }
    else if (ui->rbtn_variable->isChecked())
    {
        type = 2;

        if (ui->rbtn_actUniform->isChecked())
        {
            actType = 1;
            actSize = ui->dblSpin_actUniform->value();
            actRaster = "";
            thickness = ui->dblSpin_thickness->value();
        }
        else if (ui->rbtn_actRaster->isChecked())
        {
            actType = 3;
            actRaster = XmlProj.readNodeData("ProjectDirectory") + "/Inputs/01_InitialInputs/GS_Active.tif";
            XmlProj.writeNodeData("OriginalGrainSizeActivePath", actRasterOrig);
            actSize = 0;
            thickness = ui->dblSpin_thickness->value();
        }
        else
        {

        }

        if(ui->chbx_subsuf->isChecked())
        {
            sub = 1;
            if (ui->rbtn_subUniform->isChecked())
            {
                subType = 1;
                subSize = ui->dblSpin_subUniform->value();
                subRaster = "";
                thickness = ui->dblSpin_thickness->value();
            }
            else if (ui->rbtn_subPercent->isChecked())
            {
                subType = 2;
                subSize = ui->dblSpin_subPercent->value();
                subRaster = "";
                thickness = ui->dblSpin_thickness->value();
            }
            else if (ui->rbtn_subRaster->isChecked())
            {
                subType = 3;
                subRaster = XmlProj.readNodeData("ProjectDirectory") + "/Inputs/01_InitialInputs/GS_Subsurface.tif";
                XmlProj.writeNodeData("OriginalGrainSizeSubSurfPath", subRasterOrig);
                subSize = 0;
                thickness = ui->dblSpin_thickness->value();
            }
        }
        else
        {
            sub = 0;
        }
    }
    else
    {
        type = 0;
    }
}

void DialogGrainSize::setParamsFromXML()
{
    QString nodeData;
    nodeData = XmlProj.readNodeData("Inputs", "GrainSize", "LayerThickness");
    thickness = nodeData.toDouble();
    ui->dblSpin_thickness->setValue(thickness);
    nodeData = XmlProj.readNodeData("Inputs", "GrainSize", "Type");
    if (!nodeData.isNull() || !nodeData.isEmpty())
    {
        type = nodeData.toInt();
        if (type == 1)
        {
            actSize = XmlProj.readNodeData("Inputs", "GrainSize", "ActiveSize").toDouble();
            ui->rbtn_uniform->setChecked(true);
            ui->dblSpin_uniform->setValue(actSize);
        }
        else if (type == 2)
        {
            ui->rbtn_variable->setChecked(true);
            sub = XmlProj.readNodeData("Inputs", "GrainSize", "SubSurfLayer").toInt();
            actType = XmlProj.readNodeData("Inputs", "GrainSize", "ActiveType").toInt();
            if (actType == 1)
            {
                ui->rbtn_actUniform->setChecked(true);
                actSize = XmlProj.readNodeData("Inputs", "GrainSize", "ActiveSize").toDouble();
                ui->dblSpin_actUniform->setValue(actSize);
            }
            else if (actType == 3)
            {
                ui->rbtn_actRaster->setChecked(true);
                actRaster = XmlProj.readNodeData("Inputs", "GrainSize", "ActiveRasterPath");
                ui->line_actRaster->setText(actRaster);
            }
            else
            {

            }

            if (sub == 1)
            {
                ui->chbx_subsuf->setChecked(true);
                ui->chbx_subsuf->setEnabled(true);
                subType = XmlProj.readNodeData("Inputs", "GrainSize", "SubSurfType").toInt();
                if(subType == 1)
                {
                    ui->rbtn_subUniform->setChecked(true);
                    ui->rbtn_subUniform->setEnabled(true);
                    ui->dblSpin_subUniform->setEnabled(true);
                    subSize = XmlProj.readNodeData("Inputs", "GrainSize", "SubSurfSize").toDouble();
                    ui->dblSpin_subUniform->setValue(subSize);
                }
                else if (subType == 2)
                {
                    ui->rbtn_subPercent->setChecked(true);
                    subSize = XmlProj.readNodeData("Inputs", "GrainSize", "SubSurfSize").toDouble();
                    ui->dblSpin_subPercent->setValue(subSize);
                }
                else if (subType ==3)
                {
                    ui->rbtn_subRaster->setChecked(true);
                    subRaster = XmlProj.readNodeData("Inputs", "GrainSize", "SubSurfRasterPath");
                    ui->line_subRaster->setText(subRaster);
                }
            }

        }
    }
}

void DialogGrainSize::writeParamsToXML()
{
    XmlProj.writeNodeData("Inputs", "GrainSize", "Type", QString::number(type));
    XmlProj.writeNodeData("Inputs", "GrainSize", "ActiveType", QString::number(actType));
    XmlProj.writeNodeData("Inputs", "GrainSize", "ActiveRasterPath", actRasterOrig);
    XmlProj.writeNodeData("Inputs", "GrainSize", "ActiveSize", QString::number(actSize));
    XmlProj.writeNodeData("Inputs", "GrainSize", "SubSurfLayer", QString::number(sub));
    XmlProj.writeNodeData("Inputs", "GrainSize", "SubSurfType", QString::number(subType));
    XmlProj.writeNodeData("Inputs", "GrainSize", "SubSurfRasterPath", subRasterOrig);
    XmlProj.writeNodeData("Inputs", "GrainSize", "SubSurfSize", QString::number(subSize));
    XmlProj.writeNodeData("Inputs", "GrainSize", "LayerThickness", QString::number(thickness));
}

void DialogGrainSize::on_rbtn_uniform_clicked()
{
    ui->label_active->setEnabled(false);
    ui->rbtn_actRaster->setEnabled(false);
    ui->rbtn_actUniform->setEnabled(false);
    ui->line_actRaster->setEnabled(false);
    ui->btn_actRaster->setEnabled(false);
    ui->line_subRaster->setEnabled(false);
    ui->rbtn_subPercent->setEnabled(false);
    ui->rbtn_subRaster->setEnabled(false);
    ui->rbtn_subUniform->setEnabled(false);
    ui->btn_subRaster->setEnabled(false);
    ui->dblSpin_subPercent->setEnabled(false);
    ui->dblSpin_subUniform->setEnabled(false);
    ui->dblSpin_actUniform->setEnabled(false);
    ui->chbx_subsuf->setEnabled(false);
    ui->dblSpin_uniform->setEnabled(true);
    ui->label_uniform->setEnabled(true);
}

void DialogGrainSize::on_rbtn_variable_clicked()
{
    ui->label_active->setEnabled(true);
    ui->rbtn_actRaster->setEnabled(true);
    ui->rbtn_actUniform->setEnabled(true);
    ui->chbx_subsuf->setEnabled(true);
    ui->dblSpin_uniform->setEnabled(false);
    ui->label_uniform->setEnabled(false);
    if (ui->chbx_subsuf->isChecked())
    {
        ui->rbtn_subPercent->setEnabled(true);
        ui->rbtn_subRaster->setEnabled(true);
        ui->rbtn_subUniform->setEnabled(true);
    }
}

void DialogGrainSize::on_chbx_subsuf_stateChanged(int arg1)
{
    if (arg1)
    {
        ui->rbtn_subPercent->setEnabled(true);
        ui->rbtn_subRaster->setEnabled(true);
        ui->rbtn_subUniform->setEnabled(true);
    }
    else
    {
        ui->rbtn_subPercent->setEnabled(false);
        ui->rbtn_subRaster->setEnabled(false);
        ui->rbtn_subUniform->setEnabled(false);
    }
}

void DialogGrainSize::on_btn_actRaster_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, "Select Active Raster");
    if (!filename.isNull() || !filename.isEmpty())
    {
        QFile file(filename);
        if (file.exists())
        {
            QFileInfo fi(file);
            ui->line_actRaster->setText(filename);
            actRasterOrig = filename;
            actSuffix = fi.suffix();
        }
    }
}

void DialogGrainSize::on_btn_subRaster_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, "Select Subsurface Raster");
    if (!filename.isNull() || !filename.isEmpty())
    {
        QFile file(filename);
        if (file.exists())
        {
            QFileInfo fi(file);
            ui->line_subRaster->setText(filename);
            subRasterOrig = filename;
            subSuffix = fi.suffix();
        }
    }
}

void DialogGrainSize::on_btn_ok_clicked()
{
    setParamsfromDialog();
    writeParamsToXML();
    XmlProj.printXML();
    this->close();
}

void DialogGrainSize::on_btn_cancel_clicked()
{
    this->close();
}

void DialogGrainSize::on_rbtn_actUniform_clicked()
{
    ui->dblSpin_actUniform->setEnabled(true);
    ui->line_actRaster->setEnabled(false);
    ui->btn_actRaster->setEnabled(false);
}

void DialogGrainSize::on_rbtn_actRaster_clicked()
{
    ui->line_actRaster->setEnabled(true);
    ui->btn_actRaster->setEnabled(true);
    ui->dblSpin_actUniform->setEnabled(false);
}

void DialogGrainSize::on_rbtn_subUniform_clicked()
{
    ui->dblSpin_subUniform->setEnabled(true);
    ui->line_subRaster->setEnabled(false);
    ui->btn_subRaster->setEnabled(false);
    ui->dblSpin_subPercent->setEnabled(false);
}

void DialogGrainSize::on_rbtn_subPercent_clicked()
{
    ui->dblSpin_subPercent->setEnabled(true);
    ui->dblSpin_subUniform->setEnabled(false);
    ui->line_subRaster->setEnabled(false);
    ui->btn_subRaster->setEnabled(false);
}

void DialogGrainSize::on_rbtn_subRaster_clicked()
{
    ui->line_subRaster->setEnabled(true);
    ui->btn_subRaster->setEnabled(true);
    ui->dblSpin_subPercent->setEnabled(false);
    ui->dblSpin_subUniform->setEnabled(false);
}
