#include "windowviewer.h"
#include "ui_windowviewer.h"

WindowViewer::WindowViewer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::WindowViewer)
{
    ui->setupUi(this);

    scene = new QGraphicsScene;
    //qvMechColor;
    qvImbalColor.resize(2), qvMechColor.resize(5), qvEventMech.resize(5), qvTotalMech.resize(5), qvEventImbal.resize(5), qvTotalImbal.resize(5);
    qvImbalColor[0] = QColor(230,0,0,255), qvImbalColor[1] = QColor(0,76,168,255);
    qvMechColor[0] = QColor(255,51,51,255), qvMechColor[1] = QColor(0,128,255,255), qvMechColor[2] = QColor(255,153,51,255), qvMechColor[3] = QColor(0,153,0,255), qvMechColor[4] = QColor(255,255,51,255);
    ui->gv_spatial->rotate(90);
    nAnimateSecs = 1;
}

WindowViewer::~WindowViewer()
{
    delete ui;
}

void WindowViewer::setupPlots()
{
    ui->plot_3stacked->plotLayout()->clear();

    qvHydroPosX.resize(2);
    qvHydroPosY.resize(2);
    QPen pen, pen1;
    pen.setColor(QColor(0,0,255,255));
    pen1.setWidth(2);
    pen1.setColor(Qt::red);
    pen1.setStyle(Qt::DashLine);

    qvHydroPosX[0] = qvDate[0], qvHydroPosX[1] = qvDate[0];
    qvHydroPosY[0] = 0, qvHydroPosY[1] = MORPH::MORPHEDBaseClass::findMaxVector(qvQ, nIterations) + 20;

    QCPLayoutGrid *sediPlotLayout = new QCPLayoutGrid;
    QCPLayoutGrid *braidPlotLayout = new QCPLayoutGrid;
    QCPAxisRect *sediPlotAxes = new QCPAxisRect(ui->plot_3stacked,false);
    QCPAxisRect *braidPlotAxes = new QCPAxisRect(ui->plot_3stacked,false);
    QCPAxisRect *hydroPlotAxes = new QCPAxisRect(ui->plot_3stacked,false);
    hydroPlotAxes->addAxes(QCPAxis::atBottom | QCPAxis::atLeft);
    ui->plot_3stacked->plotLayout()->addElement(0,0,hydroPlotAxes);
    ui->plot_3stacked->plotLayout()->addElement(1,0,sediPlotLayout);
    ui->plot_3stacked->plotLayout()->addElement(2,0,braidPlotLayout);
    sediPlotLayout->addElement(0,0,sediPlotAxes);
    braidPlotLayout->addElement(0,0,braidPlotAxes);
    sediPlotAxes->addAxes(QCPAxis::atBottom | QCPAxis::atLeft);
    braidPlotAxes->addAxes(QCPAxis::atBottom | QCPAxis::atLeft);
    sediPlotAxes->axis(QCPAxis::atBottom)->setTickLabels(false);
    hydroPlotAxes->axis(QCPAxis::atBottom)->setTickLabels(false);

    QCPMarginGroup *marginGroup = new QCPMarginGroup(ui->plot_3stacked);
    sediPlotAxes->setMarginGroup(QCP::msLeft, marginGroup);
    braidPlotAxes->setMarginGroup(QCP::msLeft, marginGroup);

    ui->plot_3stacked->replot();
    ui->plot_3stacked->repaint();
}

void WindowViewer::updatePlots()
{
}

void WindowViewer::updateTable()
{
    bedEE = XmlDisplay.readNodeData(floodName,"BedErosion","Event").toDouble();
    bedET = XmlDisplay.readNodeData(floodName,"BedErosion","Total").toDouble();
    bedDE = XmlDisplay.readNodeData(floodName,"BedDeposition","Event").toDouble();
    bedDT = XmlDisplay.readNodeData(floodName,"BedDeposition","Total").toDouble();
    sloughEE = XmlDisplay.readNodeData(floodName,"SloughErosion","Event").toDouble();
    sloughET = XmlDisplay.readNodeData(floodName,"SloughErosion","Total").toDouble();
    sloughDE = XmlDisplay.readNodeData(floodName,"SloughDeposition","Event").toDouble();
    sloughDT = XmlDisplay.readNodeData(floodName,"SloughDeposition","Total").toDouble();
    exportE = XmlDisplay.readNodeData(floodName,"ExportedSediment","Event").toDouble();
    exportT = XmlDisplay.readNodeData(floodName,"ExportedSediment","Total").toDouble();
    importE = XmlDisplay.readNodeData(floodName,"ImportedSediment","Event").toDouble();
    importT = XmlDisplay.readNodeData(floodName,"ImportedSediment","Total").toDouble();

    qvEventMech[0] = bedEE, qvEventMech[1] = bedDE, qvEventMech[2] = sloughEE, qvEventMech[3] = sloughDE, qvEventMech[4] = importE;
    qvTotalMech[0] = bedET, qvTotalMech[1] = bedDT, qvTotalMech[2] = sloughET, qvTotalMech[3] = sloughDT, qvTotalMech[4] = importT;
    qvEventImbal[0] = exportE, qvEventImbal[1] = importE;
    qvTotalImbal[0] = exportT, qvTotalImbal[1] = exportT;

    ui->pie_eventImbal->setData(qvEventImbal, qvImbalColor);
    ui->pie_totalImbal->setData(qvTotalImbal, qvImbalColor);
    ui->pie_eventMech->setData(qvEventMech, qvMechColor);
    ui->pie_totalMech->setData(qvTotalMech, qvMechColor);

    double eventSum, totalSum;

    eventSum = bedEE + bedDE + sloughEE + sloughDE + importE;
    totalSum = bedET + bedDT + sloughDT + sloughET + importT;

    ui->lbl_bedDAmt->setText(QString::number(bedDE)), ui->lbl_bedDAmtT->setText(QString::number(bedDT)), ui->lbl_bedEAmt->setText(QString::number(bedEE)), ui->lbl_bedEAmtT->setText(QString::number(bedET));
    ui->lbl_sloughDAmt->setText(QString::number(sloughDE)), ui->lbl_sloughDAmtT->setText(QString::number(sloughDT)), ui->lbl_sloughEAmt->setText(QString::number(sloughEE)), ui->lbl_sloughEAmtT->setText(QString::number(sloughET));
    ui->lbl_bedDPer->setText(QString::number(bedDE/eventSum*100)), ui->lbl_bedDPerT->setText(QString::number(bedDT/totalSum*100)), ui->lbl_bedEPer->setText(QString::number(bedEE/eventSum*100)), ui->lbl_bedEPerT->setText(QString::number(bedET/totalSum*100));
    ui->lbl_sloughDPer->setText(QString::number(sloughDE/eventSum*100)), ui->lbl_sloughDPerT->setText(QString::number(sloughDT/totalSum*100)), ui->lbl_sloughEPer->setText(QString::number(sloughEE/eventSum*100)), ui->lbl_sloughEPerT->setText(QString::number(sloughET/totalSum*100));
    ui->lbl_importAmt->setText(QString::number(importE)), ui->lbl_importAmtT->setText(QString::number(importT)), ui->lbl_exportAmt->setText(QString::number(exportE)), ui->lbl_exportAmtT->setText(QString::number(exportT));
    ui->lbl_importPer->setText(QString::number(importE/eventSum*100)), ui->lbl_importPerT->setText(QString::number(importT/totalSum*100));
}

void WindowViewer::updateView()
{
    scene->clear();
    QImage image;
    image = QImage(qsDirPath + "/" + XmlDisplay.readNodeData(floodName, "HillshadePath"));
    hlsd = new QGraphicsPixmapItem(QPixmap::fromImage(image));
    image = QImage(qsDirPath + "/" + XmlDisplay.readNodeData(floodName, "DEMPath"));
    dem = new QGraphicsPixmapItem(QPixmap::fromImage(image));
    image = QImage(qsDirPath + "/" + XmlDisplay.readNodeData(floodName, "WaterDepthPath"));
    depth = new QGraphicsPixmapItem(QPixmap::fromImage(image));
    image = QImage(qsDirPath + "/" + XmlDisplay.readNodeData(floodName, "DoDCumulativePath"));
    dod = new QGraphicsPixmapItem(QPixmap::fromImage(image));

    if (ui->chbx_hlsd->isChecked())
    {
        scene->addItem(hlsd);
    }
    if (ui->chbx_dem->isChecked())
    {
        scene->addItem(dem);
    }
    if (ui->chbx_depth->isChecked())
    {
        scene->addItem(depth);
    }
    if (ui->chbx_dod->isChecked())
    {
        scene->addItem(dod);
    }

    ui->gv_spatial->setScene(scene);
}

void WindowViewer::on_actionOpen_Project_triggered()
{
    QString filename;
    filename = QFileDialog::getOpenFileName(this,"Select *.display.morph file to open");
    XmlDisplay.loadDocument(filename,2);

    nIterations = XmlDisplay.readNodeData("Floods").toInt();
    ui->lbl_floods->setText(QString::number(nIterations));
    nCurrent = 0;
    ui->intSpin_flood->setMaximum(nIterations);

    QFileInfo fi(filename);
    QDir direc = fi.dir();
    qsDirPath = direc.absolutePath();
    MORPH::MORPHEDBaseClass::loadInputText(qsDirPath+"/"+XmlDisplay.readNodeData("InputHydroSedi"),qvDate,qvQ,qvDswe,qvSedin,nIterations);
    setupPlots();
}

void WindowViewer::on_intSpin_flood_valueChanged(int arg1)
{
    nCurrent = arg1;
    floodName = "Flood" + QString::number(arg1);
    updateView();
    updateTable();
    updatePlots();
}

void WindowViewer::on_tbtn_prev_clicked()
{
    if (nCurrent > 0)
    {
        nCurrent--;
        qvHydroPosX[0] = qvDate[nCurrent-1], qvHydroPosX[1] = qvDate[nCurrent-1];
        ui->intSpin_flood->setValue(nCurrent);
        ui->intSpin_flood->valueChanged(nCurrent);
    }
}

void WindowViewer::on_tbtn_next_clicked()
{
    if (nCurrent < nIterations)
    {
        nCurrent++;
        qvHydroPosX[0] = qvDate[nCurrent-1], qvHydroPosX[1] = qvDate[nCurrent-1];
        ui->intSpin_flood->setValue(nCurrent);
        ui->intSpin_flood->valueChanged(nCurrent);
    }
}

void WindowViewer::on_actionAnimation_triggered()
{
    DialogAnimate dialog(nAnimateSecs, this);
    dialog.setModal(true);
    dialog.exec();

    if(dialog.getUpdate())
    {
        nAnimateSecs = dialog.getSeconds();
    }
}

void WindowViewer::on_actionAnimate_triggered()
{
    //this needs to run on a separate thread
    for (int i=0; i<nIterations; i++)
    {
        thread()->sleep(nAnimateSecs);
        on_tbtn_next_clicked();
    }
}

void WindowViewer::on_actionClose_triggered()
{
    QWidget::close();
}
