#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "xmlreadwrite.h"
#include "morphedmodel.h"
#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include "dialogdescription.h"
#include "dialoginputs.h"
#include "dialogmorphparams.h"
#include "dialogdelftparams.h"
#include "dialogoutputs.h"
#include "windowviewer.h"
#include <iostream>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_btn_Description_clicked();

    void on_btn_Inputs_clicked();

    void on_btn_morphParams_clicked();

    void on_btn_delftParams_clicked();

    void on_btn_Outputs_clicked();

    void on_btn_run_clicked();

    void on_btn_viewer_clicked();

    void on_actionNew_Project_triggered();

    void on_actionOpen_Project_triggered();

    void threadFinished();

    void morphUpdated(const QString &result);

    void on_actionExit_triggered();

    void on_btn_about_clicked();

signals:
    void runMorphed(int iterations);

private:
    Ui::MainWindow *ui;
    QLabel *statusLabel;
    WindowViewer *outputWindow;

    MORPH::XMLReadWrite XmlGui;
    GDALDataset *pDataset;

    QString filenameXml;
    QThread morphThread;

    int iterations;
};

#endif // MAINWINDOW_H
