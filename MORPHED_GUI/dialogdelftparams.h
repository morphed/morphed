#ifndef DIALOGDELFTPARAMS_H
#define DIALOGDELFTPARAMS_H

#include <QDialog>
#include <QFileDialog>
#include "xmlreadwrite.h"

namespace Ui {
class DialogDelftParams;
}

class DialogDelftParams : public QDialog
{
    Q_OBJECT

public:
    explicit DialogDelftParams(MORPH::XMLReadWrite &XmlObj, QWidget *parent = 0);
    ~DialogDelftParams();

    bool getUpdate();
    void disableDSLineEdits();
    void disableUSLineEdits();
    void enableDSLineEdits();
    void enableUSLineEdits();
    void setParamsFromDialog();
    void setParamsFromXML();
    void writeParamsToXML();

private slots:
    void on_btn_delftPath_clicked();

    void on_btn_ok_clicked();

    void on_btn_cancel_clicked();

    void on_rbtn_usN_clicked();

    void on_rbtn_usS_clicked();

    void on_rbtn_usE_clicked();

    void on_rbtn_usW_clicked();

    void on_rbtn_dsN_clicked();

    void on_rbtn_dsS_clicked();

    void on_rbtn_dsE_clicked();

    void on_rbtn_dsW_clicked();

private:
    Ui::DialogDelftParams *ui;

    bool update;

    MORPH::XMLReadWrite XmlProj;

    QString delftPath;
    double usX1, usY1, usX2, usY2, dsX1, dsY1, dsX2, dsY2, simTime, timeStep, roughness, HEV, ytl, xtl, cellWidth;
    int rows, cols, usLoc, dsLoc;
};

#endif // DIALOGDELFTPARAMS_H
