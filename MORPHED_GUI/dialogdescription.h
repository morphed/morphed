#ifndef DIALOGDESCRIPTION_H
#define DIALOGDESCRIPTION_H

#include <QDialog>
#include "xmlreadwrite.h"

namespace Ui {
class DialogDescription;
}

class DialogDescription : public QDialog
{
    Q_OBJECT

public:
    explicit DialogDescription(MORPH::XMLReadWrite &XmlObj, QWidget *parent = 0);
    ~DialogDescription();

    bool getUpdate();
    void setParamsFromXML();
    void writeParamsToXML();

private slots:
    void on_btn_ok_clicked();

    void on_btn_cancel_clicked();

private:
    Ui::DialogDescription *ui;

    bool update;
    MORPH::XMLReadWrite XmlProj;
    QString reach, date, dateModeled, desc;
};

#endif // DIALOGDESCRIPTION_H
