#include "piechartwidget.h"
#include <QPainter>
#include <QDebug>

PieChartWidget::PieChartWidget(QWidget *parent) :
    QWidget(parent)
{
    setMinimumSize(100,100);
}

void PieChartWidget::setData(QVector<double> values, QVector<QColor> colors)
{
    qvValues = values;
    qvColors = colors;
    repaint();
    update();
}

void PieChartWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QRectF size;
    if (this->height() > this->width())
    {
        size = QRectF(5,5,this->width()-10,this->width()-10);
    }
    else if (this->width() > this->height())
    {
        size = QRectF(5,5,this->height()-10,this->height()-10);
    }
    else
    {
        size = QRectF(5,5,this->width()-10,this->height()-10);
    }

    painter.setRenderHint(QPainter::Antialiasing);

    double sum = 0, startAng = 0;
    double angle, endAng, percent;

    for (int i=0; i<qvValues.size(); i++)
    {
        sum += qvValues[i];
    }

    for (int i=0; i<qvValues.size(); i++)
    {
        percent = qvValues[i] / sum;
        angle = percent * 360.0;
        endAng = startAng + angle;
        painter.setBrush(qvColors[i]);
        painter.drawPie(size,startAng*16,angle*16);
        startAng = endAng;
    }
//    painter.setBrush(Qt::red);
//    painter.drawPie(size, 0, 90*16);
//    painter.setBrush(Qt::blue);
//    painter.drawPie(size,90*16, 270*16);

}
