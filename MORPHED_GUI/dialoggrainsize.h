#ifndef DIALOGGRAINSIZE_H
#define DIALOGGRAINSIZE_H

#include <QDialog>
#include <QFileDialog>
#include "xmlreadwrite.h"

namespace Ui {
class DialogGrainSize;
}

class DialogGrainSize : public QDialog
{
    Q_OBJECT

public:
    explicit DialogGrainSize(MORPH::XMLReadWrite &XmlObj, QWidget *parent = 0);
    ~DialogGrainSize();

    bool getUpdate();
    void setParamsfromDialog();
    void setParamsFromXML();
    void writeParamsToXML();

private slots:

    void on_rbtn_uniform_clicked();

    void on_rbtn_variable_clicked();

    void on_chbx_subsuf_stateChanged(int arg1);

    void on_btn_actRaster_clicked();

    void on_btn_subRaster_clicked();

    void on_btn_ok_clicked();

    void on_btn_cancel_clicked();

    void on_rbtn_actUniform_clicked();

    void on_rbtn_actRaster_clicked();

    void on_rbtn_subUniform_clicked();

    void on_rbtn_subPercent_clicked();

    void on_rbtn_subRaster_clicked();

private:
    Ui::DialogGrainSize *ui;

    bool update;
    MORPH::XMLReadWrite XmlProj;
    QString actSuffix, subSuffix, actRaster, subRaster, actRasterOrig, subRasterOrig;
    int type, sub, actType, subType;
    double actSize, subSize, thickness;
};

#endif // DIALOGGRAINSIZE_H
