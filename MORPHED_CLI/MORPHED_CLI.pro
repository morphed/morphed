#-------------------------------------------------
#
# Project created by QtCreator 2014-07-21T13:28:04
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = MORPHED_CLI
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
