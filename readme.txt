This is the repository where our morphodynamic modeling files will be hosted.

The libgdal-1.dll needs to be added to the MORPHED_GUTS/release or debug build folders to run the source code. 
1. Compile the source code in release mode
2. Add the libdgdal-1.dll to the proper folder (release or debug) in the build directory

To use the MORPHED_GUI exe and MORPHED_GUTS dll you must have all the Qt dll files and platforms folder. Contact khafen74@gmail.com for these files.